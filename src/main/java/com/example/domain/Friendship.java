package com.example.domain;


import com.example.utils.Pair;

import java.time.LocalDate;

public class Friendship extends Entity<Pair<Long, Long>> {

    /**
     * Friendship extends entity
     * The id is a Pair<Long, Long>
     */
    User firstUser;
    User secondUser;
    private LocalDate date;

    /**
     * The constructor
     *
     * @param firstUser  id of the first user
     * @param secondUser second user
     * @param date when the friendship started
     */
    public Friendship(User firstUser, User secondUser, LocalDate date) {
        this.firstUser = firstUser;
        this.secondUser = secondUser;
        this.date = date;
    }

    /**
     * The constructor
     *
     * @param firstUser  id of the first user
     * @param secondUser second user
     */
    public Friendship(User firstUser, User secondUser) {
        this.firstUser = firstUser;
        this.secondUser = secondUser;
        this.date = LocalDate.now();
    }

    /**
     * getter
     *
     * @return date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * getter
     *
     * @return the first user
     */
    public User getFirstUser() {
        return firstUser;
    }

    /**
     * getter
     *
     * @return the second user
     */
    public User getSecondUser() {
        return secondUser;
    }
}
