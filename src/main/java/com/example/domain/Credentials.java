package com.example.domain;

public class Credentials extends Entity<String> {

    String username;
    String password;
    Long userId;

    public Credentials(String username, String password, Long user_id) {
        this.username = username;
        this.password = password;
        this.userId = user_id;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Long getUserId() {
        return userId;
    }
}
