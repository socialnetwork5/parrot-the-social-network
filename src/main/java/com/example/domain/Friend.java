package com.example.domain;

import com.example.utils.Sex;

import java.time.LocalDate;

public class Friend extends User{
    /**
     * Class Friend is used as a Decorator for a User.
     * A User has a list of Friends.
     */
    private User user;
    private LocalDate date;

    /**
     * Class constuctor
     * @param user - the user
     * @param friendsSince - the date since the user is a friend
     */
    public Friend(User user, LocalDate friendsSince){
        this.user = user;
        this.date = friendsSince;
    }

    /**
     * getter
     * @return the id of the user
     */
    @Override
    public Long getId() {
        return user.getId();
    }

    /**
     * getter
     * @return the name of the user
     */
    @Override
    public String getFirstName() {
        return user.getFirstName();
    }

    /**
     * getter
     * @return the name of the user
     */
    @Override
    public String getLastName() {
        return user.getLastName();
    }

    /**
     * getter
     * @return the sex of the user
     */
    @Override
    public Sex getSex() {
        return user.getSex();
    }

    /**
     * getter
     * @return the birthdate of the user
     */
    @Override
    public LocalDate getBirthdate() {
        return user.getBirthdate();
    }


    @Override
    public String getProfilePicture() {
        return user.getProfilePicture();
    }

    /**
     * getter
     * @return the email of the user
     */
    @Override
    public String getEmail() {
        return user.getEmail();
    }

    /**
     * getter
     * @return the date since the two users are friends
     */
    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return user +
                " friends since " + date;
    }
}
