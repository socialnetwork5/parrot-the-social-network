package com.example.domain;

import java.time.LocalDateTime;
import java.util.List;

public class Message extends Entity<Long>{
    Long sender;
    Long group;
    String text;
    LocalDateTime time;
    Long replyAt;

    /**
     * Class constructor
     * @param idSendingUser - the id of the sender
     * @param text - the text of the message
     * @param time - the time when the message was send
     */
    public Message(Long idSendingUser, Long group, String text, LocalDateTime time) {
        this.sender = idSendingUser;
        this.group = group;
        this.text = text;
        this.time = time;
        this.replyAt = null;
    }

    /**
     * class constructor, sets the time on now()
     * @param idSendingUser - the id of the sender
     * @param text -the text of the message
     */
    public Message(Long idSendingUser, Long group, String text) {
        this.sender = idSendingUser;
        this.group = group;
        this.text = text;
        this.time = LocalDateTime.now();
        this.replyAt = null;
    }

    public Long getGroup() {
        return group;
    }

    /**
     * getter
     * @return the sender id
     */
    public Long getSender() {
        return sender;
    }

    /**
     * getter
     * @return the text of the message
     */
    public String getText() {
        return text;
    }

    /**
     * getter
     * @return the time
     */
    public LocalDateTime getTime() {
        return time;
    }

    /**
     * setter
     * @param time to be set
     */
    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    /**
     * getter
     * @return the id of the message that this message intends to reply
     */
    public Long getReplyAt() {
        return replyAt;
    }

    /**
     * setter
     * @param replyAt to be set
     */
    public void setReplyAt(Long replyAt) {
        this.replyAt = replyAt;
    }

    @Override
    public String toString() {
        return "id: " + this.getId() +
                ", from: " + sender +
                ", text: '" + text + '\'' +
                ", " + time +
                ", replyAt=" + replyAt;
    }

    public void setText(String toString) {
        text = toString;
    }
}
