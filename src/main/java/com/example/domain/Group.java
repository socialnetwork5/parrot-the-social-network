package com.example.domain;

import java.util.List;

public class Group extends Entity<Long>{
    private String name;
    private List<Long> members;
    private String picture;
    private Long lastMessage;

    public Group(List<Long> members) {
        this.members = members;
        this.lastMessage = null;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getMembers() {
        return members;
    }

    public void setMembers(List<Long> members) {
        this.members = members;
    }

    public void addMember(Long user){
        members.add(user);
    }

    public boolean isAConversation(){
        return members.size()==2;
    }

    public boolean isAMember(Long user){
        return members.contains(user);
    }

    public Long getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(Long lastMessage) {
        this.lastMessage = lastMessage;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", members=" + members +
                ", picture='" + picture + '\'' +
                '}';
    }
}
