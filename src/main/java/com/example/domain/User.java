package com.example.domain;


import com.example.utils.Sex;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class User extends Entity<Long> {
    /**
     * The User classeextends Entity<Long>, therefore all users will have an id
     * Also, a user will also have a first and last name and a list of friends
     */
    private String firstName, lastName, email;
    private Sex sex;
    private LocalDate birthdate;
    private List<Friend> friendsList;
    String profilePicture;

    /**
     * the default constructor
     */
    public User(){}
    /**
     * The constructor
     *
     * @param firstName the firstname of the user
     * @param lastName  the last name of the user
     * @param sex
     * @param email
     */
    public User(String firstName, String lastName, Sex sex, String email, LocalDate birthdate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.email = email;
        this.birthdate = birthdate;
        this.friendsList = new ArrayList<>();
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    /**
     * getter
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * getter
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * getter
     *
     * @return the sex of the user
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * getter
     *
     * @return the birthdate of the user
     */
    public LocalDate getBirthdate() {
        return birthdate;
    }


    /**
     * setter
     *
     * @param firstName the new first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * setter
     *
     * @param lastName the new last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * setter
     *
     * @param sex the new sex
     */
    public void setSex(Sex sex) {
        this.sex = sex;
    }

    /**
     * setter
     *
     * @param birthdate
     */
    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    /**
     * getter
     * @return the friends list of a user
     * user1 ((user1, user2, data), (user3, user1, data))
     *
     */
    public List<Friend> getFriendsList(){
        return friendsList;
    }

    /**
     * setter
     * @param friends to be set as new friends list
     */
    public void setFriendsList(List<Friend> friends){
        this.friendsList = friends;
    }

    /**
     * getter
     *
     * @return the email address of the user
     */
    public String getEmail() {
        return email;
    }

    /**
     * setter
     *
     * @param email address to be set
     */
    public void setEmail(String email) {
        this.email = email;
    }


    /**
     * toString
     *
     * @return a String that represents the user
     */
    @Override
    public String toString() {
        return
                "id: " + getId() +
                        " | firstName: " + firstName +
                        " | lastName: " + lastName +
                        " | sex: " + sex +
                        " | email: " + email +
                        " | birthdate: " + birthdate +
                        " | image path: " + profilePicture;
    }
}
