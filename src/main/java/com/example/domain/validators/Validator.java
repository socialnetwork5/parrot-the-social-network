package com.example.domain.validators;

import com.example.exceptions.ValidationException;

public interface Validator<T> {
    /**
     * Validator Interface(implemented by FriendshipValidator and UserValidator)
     *
     * @param entity to be checked
     * @throws ValidationException
     */
    void validate(T entity) throws ValidationException;
}