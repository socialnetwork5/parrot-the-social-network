package com.example.domain.validators;


import com.example.domain.User;
import com.example.exceptions.ValidationException;

import java.time.LocalDate;
import java.time.Period;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class UserValidator implements Validator<User> {
    /**
     * User Validator
     *
     * @param entity to be checked
     * @throws ValidationException if:
     *                             -first_name is null
     *                             -last_name is null
     *                             -email is null
     *                             -sex is null
     *                             -sex is not a value from the list
     *                             -email is not valid
     *                             -age is below 16
     */
    @Override
    public void validate(User entity) throws ValidationException {
        String message = "";

        message += checkFirstName(entity.getFirstName());
        message += checkLastName(entity.getLastName());
        message += checkEmail(entity.getEmail());
        message += checkBirthdate(entity.getBirthdate());

        if (!message.equals(""))
            throw new ValidationException(message);
    }

    private String checkBirthdate(LocalDate birthdate) {
        int varsta = Period.between(birthdate, LocalDate.now()).getYears();
        if (varsta < 16)
            return "An age above 16 is required!";
        return "";
    }

    private String checkEmail(String email) {
        if (email.length() == 0)
            return "Email cannot be null!\n";
        else {
            String regex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(email);
            if (!matcher.matches())
                return "Email is not valid!\n";
            return "";
        }
    }


    private String checkLastName(String lastName) {
        if (lastName.length() == 0)
            return "Last name cannot be null!\n";
        else{
            String regex = "^[A-Z][a-z]*$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(lastName);
            if (!matcher.matches())
                return "Last name is not valid!\n";
        }
        return "";
    }

    private String checkFirstName(String firstName) {
        if (firstName.length() == 0)
            return "First name cannot be null!\n";
        else{
            String regex = "^[A-Z][a-z]*$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(firstName);
            if (!matcher.matches())
                return "First name is not valid!\n";
        }
        return "";
    }
}
