package com.example.domain.validators;

import com.example.domain.Friendship;
import com.example.exceptions.ValidationException;

public class FriendshipValidator implements Validator<Friendship> {
    /**
     * Friendship validator
     *
     * @param entity to be checked
     * @throws ValidationException
     */
    @Override
    public void validate(Friendship entity) throws ValidationException {
        //to be implemented
    }
}
