package com.example.domain.validators;

import com.example.domain.Event;
import com.example.exceptions.ValidationException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventValidator implements Validator<Event>{

    @Override
    public void validate(Event entity) throws ValidationException {
        String message = "";
        message += checkString(entity.getName(), "Title");
        message += checkString(entity.getDescription(), "Description");
        message += checkString(entity.getLocation(), "Location");
        if (!message.equals(""))
            throw new ValidationException(message);
    }

    private String checkString(String string, String error) {
        if (string.length() == 0)
            return error + " cannot be null!\n";
        return "";
    }
}
