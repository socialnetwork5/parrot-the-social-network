package com.example.domain;

import java.time.LocalDateTime;

public class Event extends Entity<Long> {
    private String name, description, picture, location;
    private Long organizer;
    LocalDateTime time;

    public Event(String name, String location, Long organizer, LocalDateTime time) {
        this.name = name;
        this.location = location;
        this.organizer = organizer;
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Long getOrganizer() {
        return organizer;
    }

    public void setOrganizer(Long organizer) {
        this.organizer = organizer;
    }
}
