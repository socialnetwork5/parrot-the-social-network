package com.example.domain;


import com.example.utils.Pair;
import com.example.utils.Status;

import java.time.LocalDate;

public class FriendRequest extends Entity<Pair<Long, Long>>{
    Long idSendingUser;
    Long idReceivingUser;
    Status status;
    LocalDate date;

    /**
     * Class constructor sets the status on pending
     * @param idSendingUser - the id of the sending user
     * @param idReceivingUser - the id of the receiving user
     */
    public FriendRequest(Long idSendingUser, Long idReceivingUser) {
        this.idSendingUser = idSendingUser;
        this.idReceivingUser = idReceivingUser;
        this.status = Status.pending;
        this.date = LocalDate.now();
    }

    /**
     * Class constructor that receives a status
     * @param idSendingUser - the id of the sending user
     * @param idReceivingUser - the id of the receiving user
     * @param status - the status of the request
     */
    public FriendRequest(Long idSendingUser, Long idReceivingUser, Status status, LocalDate date) {
        this.idSendingUser = idSendingUser;
        this.idReceivingUser = idReceivingUser;
        this.status = status;
        this.date = date;
    }

    public FriendRequest(Long idSendingUser, Long idReceivingUser, Status status) {
        this.idSendingUser = idSendingUser;
        this.idReceivingUser = idReceivingUser;
        this.status = status;
    }

    /**
     * getter
     * @return the sending id
     */
    public Long getIdSendingUser() {
        return idSendingUser;
    }

    /**
     * getter
     * @return the receiver id
     */
    public Long getIdReceivingUser() {
        return idReceivingUser;
    }

    /**
     * getter
     * @return status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * setter
     * @param status to be set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Id sender: " + idSendingUser +
                " Id receiver: " + idReceivingUser +
                " status=" + status;
    }
}
