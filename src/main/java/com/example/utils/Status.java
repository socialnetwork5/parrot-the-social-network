package com.example.utils;

public enum Status {
    pending, approved, rejected
}
