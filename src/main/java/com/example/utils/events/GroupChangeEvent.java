package com.example.utils.events;

import com.example.domain.Group;

public class GroupChangeEvent implements Event{
    private ChangeEventType type;
    private Group data, oldData;

    public GroupChangeEvent(ChangeEventType type, Group data) {
        this.type = type;
        this.data = data;
    }

    public GroupChangeEvent(ChangeEventType type, Group data, Group oldData) {
        this.type = type;
        this.data = data;
        this.oldData = oldData;
    }

    public ChangeEventType getType() {
        return type;
    }

    public Group getData() {
        return data;
    }

    public Group getOldData() {
        return oldData;
    }
}
