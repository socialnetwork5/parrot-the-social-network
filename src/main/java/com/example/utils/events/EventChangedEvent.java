package com.example.utils.events;


public class EventChangedEvent implements Event{
    private ChangeEventType type;
    private com.example.domain.Event data, oldData;

    public EventChangedEvent(ChangeEventType type, com.example.domain.Event data) {
        this.type = type;
        this.data = data;
    }

    public EventChangedEvent(ChangeEventType type, com.example.domain.Event data, com.example.domain.Event oldData) {
        this.type = type;
        this.data = data;
        this.oldData=oldData;
    }

    public ChangeEventType getType() {
        return type;
    }

    public com.example.domain.Event getData() {
        return data;
    }

    public com.example.domain.Event getOldData() {
        return oldData;
    }
}
