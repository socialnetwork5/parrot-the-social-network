package com.example.utils.events;

import com.example.controller.chat.cellViewsMessages.CellViewMessage;
import com.example.domain.Message;

public class MessageChangeEvent implements Event{
    private ChangeEventType type;
    private Message data, oldData;
    private CellViewMessage userData;

    public MessageChangeEvent(ChangeEventType type, Message data, CellViewMessage userData) {
        this.type = type;
        this.data = data;
        this.userData = userData;
    }

    public MessageChangeEvent(ChangeEventType type, Message data, Message oldData, CellViewMessage userData) {
        this.type = type;
        this.data = data;
        this.oldData = oldData;
        this.userData = userData;
    }

    public MessageChangeEvent(ChangeEventType type, Message data) {
        this.type = type;
        this.data = data;
    }

    public MessageChangeEvent(ChangeEventType type, Message data, Message oldData) {
        this.type = type;
        this.data = data;
        this.oldData = oldData;
    }

    public ChangeEventType getType() {
        return type;
    }

    public Message getData() {
        return data;
    }

    public Message getOldData() {
        return oldData;
    }

    public CellViewMessage getUserData(){return userData;}
}
