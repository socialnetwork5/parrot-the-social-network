package com.example.utils;

public enum Sex {
    MALE,
    FEMALE,
    OTHER
}
