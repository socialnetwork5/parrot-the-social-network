package com.example.utils;

import java.time.format.DateTimeFormatter;

public class Dates {
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(Dates.DATE);

    public static final String DATE = "yyyy-MM-dd";
}
