package com.example.utils;

import java.util.Timer;
import java.util.TimerTask;

import static javafx.application.Platform.runLater;

public class TimerLoad {

    public static Timer setInterval(Runnable runnable, int delay){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runLater(new Runnable(){
                    @Override
                    public void run() {
                        runnable.run();
                    }
                });
            }
        }, delay, 2000);
        return timer;
    }
}
