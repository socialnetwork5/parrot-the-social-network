package com.example.socialnetworkdb2;

import com.example.controller.mainComtrollers.LogInController;
import com.example.domain.Credentials;
import com.example.domain.validators.*;
import com.example.repository.Repository;
import com.example.repository.db.*;
import com.example.service.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class LogInInterface extends Application {
    @Override
    public void start(Stage stage) throws IOException {

        UserDbRepository userRepo = new UserDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres",new UserValidator());
        FriendshipDbRepository friendshipRepo = new FriendshipDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres",new FriendshipValidator());
        FriendRequestDbRepository friendRequestRepository= new FriendRequestDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres",new FriendRequestValidator());
        MessageDbRepository messageRepo = new MessageDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres", new MessageValidator());
        Repository<String, Credentials> credentialsRepo = new CredentialsDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres", new CredentialsValidator());
        GroupDbRepository groupRepo = new GroupDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres", new GroupValidator());
        EventDbRepository eventRepo = new EventDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres", new EventValidator());
        EnrollmentsDbRepository enrollmentsDbRepository = new EnrollmentsDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres");


        ServiceUser userServ = new ServiceUser(userRepo);
        ServiceFriendship friendshipServ = new ServiceFriendship(friendshipRepo);
        ServiceFriendRequest friendRequestServ = new ServiceFriendRequest(friendRequestRepository);
        ServiceMessage messageServ = new ServiceMessage(messageRepo);
        ServiceCredentials serviceCredentials = new ServiceCredentials(credentialsRepo);
        ServiceGroup serviceGroup = new ServiceGroup(groupRepo);
        ServiceEnrollments serviceEnrollments= new ServiceEnrollments(enrollmentsDbRepository);
        ServiceEvents serviceEvents = new ServiceEvents(eventRepo);
        EventManager eventManager = new EventManager(serviceEvents, serviceEnrollments);

        SuperServiceAdmin serv = new SuperServiceAdmin(userServ, friendshipServ);
        SuperServiceUser serviceUser = new SuperServiceUser(userServ, friendshipServ, friendRequestServ, messageServ, serviceGroup, eventManager);
        ServiceLogIn serviceLogIn = ServiceLogIn.getInstance(serviceCredentials);
        ServiceSignUp serviceSignUp = ServiceSignUp.getInstance(serviceCredentials, userServ);

        FXMLLoader fxmlLoader = new FXMLLoader(LogInInterface.class.getResource("mainWindows/logInView.fxml"));
        BorderPane root=fxmlLoader.load();

        LogInController logInController = fxmlLoader.getController();
        logInController.setServices(serviceLogIn, serviceSignUp, serviceUser);
        Scene scene = new Scene(root);

        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();


    }

    public static void main(String[] args) {
        launch();
    }
}