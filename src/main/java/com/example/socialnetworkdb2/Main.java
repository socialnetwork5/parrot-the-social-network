package com.example.socialnetworkdb2;

import com.example.domain.Credentials;
import com.example.domain.validators.*;
import com.example.repository.Repository;
import com.example.repository.db.*;
import com.example.service.*;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.pdfbox.pdmodel.font.encoding.WinAnsiEncoding;

import javax.security.auth.login.CredentialException;
import java.io.IOException;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws IOException {
//        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("chatPane.fxml"));
//
//        AnchorPane root=fxmlLoader.load();
//
//        Repository<Long, User> userRepo = new UserDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
//                "postgres","postgres",new UserValidator());
//        Repository<Pair<Long, Long>, Friendship> friendshipRepo = new FriendshipDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
//                "postgres","postgres",new FriendshipValidator());
//        Repository<Pair<Long, Long>, FriendRequest> friendRequestRepository= new FriendRequestDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
//                "postgres","postgres",new FriendRequestValidator());
//        MessageDbRepository messageRepo = new MessageDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
//                "postgres","postgres", new MessageValidator());
//        Repository<String, Credentials> credentialsRepo = new CredentialsDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
//                "postgres","postgres", new CredentialsValidator());
//        GroupDbRepository groupRepo = new GroupDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
//                "postgres","postgres", new GroupValidator());
//        Repository<Long, Event> eventRepo = new EventDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
//                "postgres","postgres", new EventValidator());
//        EnrollmentsDbRepository enrollmentsDbRepository = new EnrollmentsDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
//                "postgres","postgres");
//
//        ServiceUser userServ = new ServiceUser(userRepo);
//        ServiceFriendship friendshipServ = new ServiceFriendship(friendshipRepo);
//        ServiceFriendRequest friendRequestServ = new ServiceFriendRequest(friendRequestRepository);
//        ServiceMessage messageServ = new ServiceMessage(messageRepo);
//        ServiceCredentials serviceCredentials = new ServiceCredentials(credentialsRepo);
//        ServiceGroup serviceGroup = new ServiceGroup(groupRepo);
//        ServiceEnrollments serviceEnrollments= new ServiceEnrollments(enrollmentsDbRepository);
//        ServiceEvents serviceEvents = new ServiceEvents(eventRepo);
//        EventManager eventManager = new EventManager(serviceEvents, serviceEnrollments);
//
//        SuperServiceAdmin serv = new SuperServiceAdmin(userServ, friendshipServ);
//        SuperServiceUser serviceUser = new SuperServiceUser(userServ, friendshipServ, friendRequestServ, messageServ, serviceGroup, eventManager);
//        ServiceLogIn serviceLogIn = new ServiceLogIn(serviceCredentials);
//        ServiceSignUp serviceSignUp = new ServiceSignUp(serviceCredentials, userServ);
//
//        try {
//            serviceUser.setIdUser(2L);
//        } catch (CredentialException e) {
//            e.printStackTrace();
//        }
//
//        Scene scene = new Scene(root);
//        stage.setTitle("Hello!");
//        stage.setScene(scene);
//        stage.show();
    }

    public static String remove(String test) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < test.length(); i++) {
            if (WinAnsiEncoding.INSTANCE.contains(test.charAt(i))) {
                b.append(test.charAt(i));
            }
        }
        return b.toString();
    }

    public static void main(String[] args) throws IOException {
        UserDbRepository userRepo = new UserDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres",new UserValidator());
        FriendshipDbRepository friendshipRepo = new FriendshipDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres",new FriendshipValidator());
        FriendRequestDbRepository friendRequestRepository= new FriendRequestDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres",new FriendRequestValidator());
        MessageDbRepository messageRepo = new MessageDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres", new MessageValidator());
        Repository<String, Credentials> credentialsRepo = new CredentialsDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres", new CredentialsValidator());
        GroupDbRepository groupRepo = new GroupDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres", new GroupValidator());
        EventDbRepository eventRepo = new EventDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres", new EventValidator());
        EnrollmentsDbRepository enrollmentsDbRepository = new EnrollmentsDbRepository("jdbc:postgresql://localhost:5432/socialnetwork",
                "postgres","postgres");

        ServiceUser userServ = new ServiceUser(userRepo);
        ServiceFriendship friendshipServ = new ServiceFriendship(friendshipRepo);
        ServiceFriendRequest friendRequestServ = new ServiceFriendRequest(friendRequestRepository);
        ServiceMessage messageServ = new ServiceMessage(messageRepo);
        ServiceGroup serviceGroup = new ServiceGroup(groupRepo);
        ServiceEnrollments serviceEnrollments= new ServiceEnrollments(enrollmentsDbRepository);
        ServiceEvents serviceEvents = new ServiceEvents(eventRepo);
        EventManager eventManager = new EventManager(serviceEvents, serviceEnrollments);
        SuperServiceUser serviceUser = new SuperServiceUser(userServ, friendshipServ, friendRequestServ, messageServ, serviceGroup, eventManager);

        try {
            serviceUser.setIdUser(2L);
        } catch (CredentialException e) {
            e.printStackTrace();
        }

    }
}