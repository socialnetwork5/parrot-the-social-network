package com.example.service;

import com.example.domain.Event;
import com.example.domain.User;
import com.example.repository.db.EnrollmentsDbRepository;

import java.util.List;

public class ServiceEnrollments {
    private EnrollmentsDbRepository repository;

    public ServiceEnrollments(EnrollmentsDbRepository repository) {
        this.repository = repository;
    }

    public List<Event> getEnrolled(Long id){
        return repository.getEnrolled(id);
    }

    public List<Long> getEnrolledUsers(Long id) {return repository.getEnrolledUsers(id);}

    public boolean isUserEnrolled(Long idUser, Long idEvent){return repository.isUserEnrolled(idUser, idEvent);}

    public void enrollUser(Long idUser, Long idEvent){repository.enrollUser(idUser, idEvent);}

    public void unEnrollUser(Long idUser, Long idEvent){repository.unEnrollUser(idUser, idEvent);}

    public void turnNotificationsOn(Long idUser, Long idEvent){repository.turnNotificationsOn(idUser, idEvent);}

    public void turnNotificationsOff(Long idUser, Long idEvent){repository.turnNotificationsOff(idUser, idEvent);}

    public boolean areNotificationsOn(Long idUser, Long idEvent){return repository.areNotificationsOn(idUser, idEvent);}
}
