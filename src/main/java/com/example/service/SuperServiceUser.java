package com.example.service;


import com.example.domain.*;
import com.example.dto.FriendDTO;
import com.example.dto.MessageDTO;
import com.example.dto.RequestDTO;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.utils.Status;
import com.example.utils.events.ChangeEventType;
import com.example.utils.events.UserChangedEvent;
import com.example.utils.observer.Observable;
import com.example.utils.observer.Observer;

import javax.security.auth.login.CredentialException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class SuperServiceUser implements Observable<UserChangedEvent> {
    private ServiceUser serviceUser;
    private ServiceFriendship serviceFriendship;
    private ServiceFriendRequest serviceFriendRequest;
    private ServiceMessage serviceMessage;
    private ServiceGroup serviceGroup;
    private EventManager eventManager;
    private Long idUser;
    private List<Observer<UserChangedEvent>> observers = new ArrayList<>();


    public SuperServiceUser(ServiceUser serviceUser, ServiceFriendship serviceFriendship, ServiceFriendRequest serviceFriendRequest, ServiceMessage serviceMessage, ServiceGroup serviceGroup, EventManager eventManager){
        this.serviceUser = serviceUser;
        this.serviceFriendship = serviceFriendship;
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceMessage = serviceMessage;
        this.serviceGroup = serviceGroup;
        this.eventManager = eventManager;
    }

    /**
     * Function used for connection
     * @param idUser the id of the user that wants to connect
     * @throws CredentialException
     */
    public void setIdUser(Long idUser) throws CredentialException {
        try {
            serviceUser.findOne(idUser);
        } catch (ServiceException e) {
            throw new CredentialException("User with the given id does not exist");
        }
        this.idUser = idUser;
    }

    public Long getIdUser(){
        return idUser;
    }


    public List<Group> findAllUsersGroups(){
        List<Group> groups =  serviceGroup.findAll().stream().filter(g -> g.isAMember(idUser)).collect(Collectors.toList());
        return groups;
    }

    public List<Group> findAllActiveUsersGroups(){
        List<Group> groups =  serviceGroup
                .findAll()
                .stream()
                .filter(g -> g.isAMember(idUser))
                .filter(g -> !g.isAConversation() || (g.isAConversation() && g.getLastMessage() != 0 ))
                .collect(Collectors.toList());
        return groups;
    }

    public Group createGroup(String name, List<Long> members, String path) throws ValidationException {
        return serviceGroup.createGroup(name, members, path);
    }

    public Group searchConversation(Long id) throws ValidationException {
        return serviceGroup.searchConversation(idUser, id);
    }

    /**
     * function used to accept a certain friendrequest
     * @param idSender - the id of the sender
     * @throws ValidationException
     * @throws ServiceException
     */
    public void acceptFriendRequest(Long idSender) throws ValidationException, ServiceException {
        serviceFriendRequest.updateFriendRequest(idSender, this.idUser, "approved");
        User first = serviceUser.findOne(idSender);
        User second = serviceUser.findOne(idUser);
        serviceFriendship.save(first, second);
        notifyObservers(new UserChangedEvent(ChangeEventType.UPDATE, null));
    }

    /**
     * function used to decline a friend request
     * @param idSender - the id of the sender
     * @throws ValidationException
     * @throws ServiceException
     */
    public void declineFriendRequest(Long idSender) throws ValidationException, ServiceException {
        serviceFriendRequest.updateFriendRequest(idSender, idUser, "rejected");
        notifyObservers(new UserChangedEvent(ChangeEventType.UPDATE, null));
    }

    /**
     * function used to delete a certain sended friendrequest
     * @param idReceiver
     * @throws ServiceException
     */
    public void deleteSentFriendRequest(Long idReceiver) throws ServiceException {
        serviceFriendRequest.deleteFriendRequest(idUser, idReceiver);
        notifyObservers(new UserChangedEvent(ChangeEventType.UPDATE, null));
    }

    /**
     * getFriends
     * @return the list of friends in DTO format
     * @throws ServiceException
     */
    public List<FriendDTO> getFriends() throws ServiceException {
        User user = serviceUser.findOne(idUser);
        return serviceFriendship.getFriendsDTO(user);
    }

    public List<Friend> getListOfFriends() throws ServiceException {
        User user = serviceUser.findOne(idUser);
        return serviceFriendship.getFriends(user);
    }

    /**
     * function used to send a friend request
     * @param idReceiver - the id of the receiver
     * @throws ValidationException
     * @throws ServiceException
     */
    public void sendFriendRequest(Long idReceiver) throws ValidationException, ServiceException {
        serviceFriendRequest.sendFriendRequest(idUser, idReceiver);
        notifyObservers(new UserChangedEvent(ChangeEventType.UPDATE, null));
    }

    /**
     * function used to get all the pending friendrequest for the user
     * @return
     */
    public Iterable<User> getAllPendingForReceiver() {
        Iterable<FriendRequest> requests = serviceFriendRequest.findAll();
        return StreamSupport.stream(requests.spliterator(), false).
                filter(friendRequest -> friendRequest.getIdReceivingUser().equals(idUser)).
                filter(friendRequest -> friendRequest.getStatus().equals(Status.pending)).
                map(friendRequest -> {
                    try {
                        return serviceUser.findOne(friendRequest.getIdSendingUser());
                    } catch (ServiceException ignored) {}
                    return null;
                }).
                collect(Collectors.toList());
    }

    public Iterable<RequestDTO> getAllPendingForReceiverDTO() {
        Iterable<FriendRequest> requests = serviceFriendRequest.findAll();
        return StreamSupport.stream(requests.spliterator(), false).
                filter(friendRequest -> friendRequest.getIdReceivingUser().equals(idUser)).
                filter(friendRequest -> friendRequest.getStatus().equals(Status.pending)).
                map(friendRequest -> {
                    try {
                        User user = serviceUser.findOne(friendRequest.getIdSendingUser());
                        RequestDTO requestDTO = new RequestDTO(user, friendRequest.getStatus(), friendRequest.getDate());
                        return requestDTO;
                    } catch (ServiceException ignored) {}
                    return null;
                }).
                collect(Collectors.toList());
    }

    public Iterable<RequestDTO> getAllRejectedAndApprovedForReceiverDTO() {
        Iterable<FriendRequest> requests = serviceFriendRequest.findAll();
        return StreamSupport.stream(requests.spliterator(), false).
                filter(friendRequest -> friendRequest.getIdReceivingUser().equals(idUser)).
                filter(friendRequest -> {
                    if(friendRequest.getStatus().equals(Status.approved) || friendRequest.getStatus().equals(Status.rejected))
                        return true;
                    return false;
                }).
                map(friendRequest -> {
                    try {
                        User user = serviceUser.findOne(friendRequest.getIdSendingUser());
                        return new RequestDTO(user, friendRequest.getStatus(), friendRequest.getDate());
                    } catch (ServiceException ignored) {}
                    return null;
                }).
                collect(Collectors.toList());
    }

    public boolean wantsToBeFriend(User user){
        List<User> friends  = StreamSupport.stream(getAllPendingForReceiver().spliterator(), false).collect(Collectors.toList());
        if(friends.contains(user))
            return true;
        return false;
    }

    /**
     * funcion used to get all friend request for the sender
     * @return the list of searched friend requests
     */
    public Iterable<User> getAllPendingForSender(){
        Iterable<FriendRequest> requests = serviceFriendRequest.findAll();
        return StreamSupport.stream(requests.spliterator(), false).
                filter(friendRequest -> friendRequest.getIdSendingUser().equals(idUser)).
                filter(friendRequest -> friendRequest.getStatus().equals(Status.pending)).
                map(friendRequest -> {
                    try {
                        return serviceUser.findOne(friendRequest.getIdReceivingUser());
                    } catch (ServiceException ignored) {}
                    return null;
                }).
                collect(Collectors.toList());
    }

    /**
     * fucntion used to delete a certain friendship
     * @param idFriend - the id of the friend we want to delete
     * @throws ServiceException
     */
    public void deleteFriendship(Long idFriend) throws ServiceException {
        serviceFriendship.delete(idUser, idFriend);
        try {
            serviceFriendRequest.deleteFriendRequest(idUser, idFriend);
        }catch (ServiceException ignored){}
        try {
            serviceFriendRequest.deleteFriendRequest(idFriend, idUser);
        }catch (ServiceException ignored){}
        notifyObservers(new UserChangedEvent(ChangeEventType.UPDATE, null));
    }

    /**
     * function used to update user infos
     * @param firstName - to be set
     * @param lastName - to be set
     * @param sex - to be set
     * @param email - to be set
     * @param birthdate - to be set
     * @throws ValidationException
     * @throws ServiceException
     */
    public void updateInfos(String firstName, String lastName, String sex, String email, LocalDate birthdate) throws ValidationException, ServiceException {
        serviceUser.update(idUser, firstName, lastName, sex, email, birthdate);
    }

    /**
     * function used to get user info
     * @return the user
     * @throws ServiceException
     */
    public User getUser() throws ServiceException {
        return serviceUser.findOne(idUser);
    }

    public User findOne(Long id) throws ServiceException {
        return serviceUser.findOne(id);
    }

    /**
     * function used to return all the users from the network
     * @return list of users
     */
    public Iterable<User> getUsers(){
        return serviceUser.findAll();
    }

    /**
     * sendMessage
     * @param idGroup- the id of the group
     * @param text - the text of the message
     * @throws ValidationException
     * @throws ServiceException
     */
    public void sendMessage(Long idGroup, String text) throws ValidationException, ServiceException {
        Long idMesaj = serviceMessage.send(idUser, idGroup, text);
        serviceGroup.updateLastMessage(idGroup, idMesaj);
    }

    /**
     * replyMessage
     * @param idMessage - id of the message
     * @param text - text of the message
     * @throws ValidationException
     * @throws ServiceException
     */
    public void replyMessage(Long idMessage, String text) throws ValidationException, ServiceException {
        Long idReply = serviceMessage.reply(idMessage, idUser, text);
        Long idGroup = serviceMessage.findOne(idMessage).getGroup();
        serviceGroup.updateLastMessage(idGroup, idReply);
    }

    public void setProfilePicture(String path) throws ServiceException, ValidationException {
        User user = serviceUser.findOne(idUser);
        serviceUser.updateProfilePicture(user, path);
    }

    public void createEvent(String title, String description, String location, LocalDateTime time, String picture) throws ValidationException {
        eventManager.CreateEvent(title, description, location, time, idUser, picture);
    }

    public void enrollUser(Long idEvent){
        eventManager.enrollUser(idUser, idEvent);
    }

    public void unEnrollUser(Long idEvent){eventManager.unEnrollUser(idUser, idEvent);}

    public void turnNotificationsOff(Long idEvent) {eventManager.turnNotificationsOff(idUser, idEvent);}

    public void turnNotificationsOn(Long idEvent) {eventManager.turnNotificationsOn(idUser, idEvent);}

    public List<Event> getNearbyEvents(){return eventManager.getNearbyEvents();}

    public List<Event> getNotificationsForUpcomingEvents(){return eventManager.getNotificationsForUpcomingEvents(idUser);}

    public ServiceFriendship getServiceFriendship(){
        return serviceFriendship;
    }

    public ServiceFriendRequest getServiceFriendRequest(){
        return serviceFriendRequest;
    }

    public ServiceUser getServiceUser(){
        return serviceUser;
    }

    public ServiceGroup getServiceGroup() {return serviceGroup;}


    public ServiceMessage getServiceMessage(){return serviceMessage;}

    public EventManager getEventManager() {return eventManager;}

    public List<User> searchByName(String name){
        name = name.toLowerCase();
        Iterable<User> users = getUsers();
        String finalName = name;
        List<User> usersWithName = StreamSupport.stream(users.spliterator(), false).
                filter(user -> user.getFirstName().toLowerCase().startsWith(finalName) ||
                        user.getLastName().toLowerCase().startsWith(finalName) ||
                        (user.getLastName().toLowerCase() + " " + user.getFirstName().toLowerCase()).startsWith(finalName) ||
                        (user.getFirstName().toLowerCase() + " " + user.getLastName().toLowerCase()).startsWith(finalName)).
                collect(Collectors.toList());
        return usersWithName;
    }

    public boolean areFriends(Long idFriend){
        return serviceFriendship.findOne(idUser, idFriend) != null;
    }

    public boolean requestExists(Long idReceiver){
        FriendRequest friendRequest = serviceFriendRequest.findOne(idUser, idReceiver);
        return friendRequest != null && friendRequest.getStatus() == Status.pending;
    }

    public boolean reverseRequestExists(Long idSender){
        FriendRequest friendRequest = serviceFriendRequest.findOne(idSender, idUser);
        return friendRequest != null && friendRequest.getStatus() == Status.pending;
    }

    public List<Message> getGroupChat(Long idGroup){
        return serviceMessage.getGroupChat(idGroup);
    }

    public Message getMessage(Long idMessage){
        return serviceMessage.getMessage(idMessage);
    }

    public List<MessageDTO> getRaportMessages(LocalDateTime begin, LocalDateTime end, Long idGroup){
        Iterable<Message> messages = serviceMessage.findAll();
        return StreamSupport.stream(messages.spliterator(), false).filter(message -> message.getTime().isAfter(begin) && message.getTime().isBefore(end) && message.getGroup().equals(idGroup))
                .map(message -> {
                    try {
                        Long id = message.getId();
                        User user = serviceUser.findOne(message.getSender());
                        String name = user.getFirstName() + " " + user.getLastName();
                        LocalDateTime time = message.getTime();
                        String text = message.getText();
                        Long replyId = message.getReplyAt();
                        String replyAt = null;
                        if(replyId != null)
                            replyAt = serviceMessage.findOne(message.getReplyAt()).getText();

                        return new MessageDTO(id, name, time, text, replyAt);
                    } catch (ServiceException e) {
                        e.printStackTrace();
                    }
                    return null;
                }).collect(Collectors.toList());

    }

    public boolean isAGroupMember(Long id){
        Group group = serviceGroup.findOne(id);
        return group.isAMember(idUser);
    }

    public List<MessageDTO> getRaportAllMessages(LocalDateTime begin, LocalDateTime end){
        Iterable<Message> messages = serviceMessage.findAll();
        return StreamSupport.stream(messages.spliterator(), false).filter(message -> message.getTime().isAfter(begin) && message.getTime().isBefore(end) && isAGroupMember(message.getGroup()) && message.getSender()!=idUser)
                .map(message -> {
                    try {
                        Long id = message.getId();
                        User user = serviceUser.findOne(message.getSender());
                        String name = user.getFirstName() + " " + user.getLastName();
                        LocalDateTime time = message.getTime();
                        String text = message.getText();
                        Long replyId = message.getReplyAt();
                        String replyAt = null;
                        if(replyId != null)
                            replyAt = serviceMessage.findOne(message.getReplyAt()).getText();

                        return new MessageDTO(id, name, time, text, replyAt);
                    } catch (ServiceException e) {
                        e.printStackTrace();
                    }
                    return null;
                }).collect(Collectors.toList());

    }

    public List<FriendDTO> getRaportAllNewFriends(LocalDateTime begin, LocalDateTime end) throws ServiceException {
        //return serviceFriendship.findAll().stream().filter(friendship -> friendship.getDate().isAfter(begin.toLocalDate()) && friendship.getDate().isBefore(end.toLocalDate())).collect(Collectors.toList());
        return getFriends().stream().filter(friendship -> friendship.getDate().isAfter(begin.toLocalDate()) && friendship.getDate().isBefore(end.toLocalDate())).collect(Collectors.toList());
    }


    public List<Message> getMessagesOnPage(int page){
        return serviceMessage.getMessagesOnPage(page);
    }

    public List<Message> getMessagesOnPage(int page, Long idGroup){
        return serviceMessage.getMessagesOnPage(page, idGroup);
    }

    public void setPageSizeGeneralMessages(int size){
        serviceMessage.setPageSizeGeneral(size);
    }

    public void setPageSizeGroupMessages(int size){
        serviceMessage.setPageSizeGroup(size);
    }

    public List<Message> getPrevMessages(){
        return serviceMessage.getPrevMessages();
    }

    public List<Message> getPrevMessages(Long idGroup){
        return serviceMessage.getPrevMessages(idGroup);
    }


    @Override
    public void addObserver(Observer<UserChangedEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<UserChangedEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(UserChangedEvent t) {
        observers.forEach(observer -> observer.update(t));
    }

}
