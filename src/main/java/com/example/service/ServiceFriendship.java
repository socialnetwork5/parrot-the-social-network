package com.example.service;


import com.example.domain.Friend;
import com.example.domain.Friendship;
import com.example.domain.User;
import com.example.dto.FriendDTO;
import com.example.dto.FriendshipDTO;
import com.example.exceptions.RepositoryException;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.repository.db.FriendshipDbRepository;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.PageableImplementation;
import com.example.utils.Pair;
import com.example.utils.events.ChangeEventType;
import com.example.utils.events.FriendshipChangedEvent;
import com.example.utils.observer.Observable;
import com.example.utils.observer.Observer;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class ServiceFriendship implements Observable<FriendshipChangedEvent> {
    private FriendshipDbRepository relations;
    private List<Observer<FriendshipChangedEvent>> observers = new ArrayList<>();
    private int pageGeneral = 0, pageSizeGeneral = 1;
    private int pageUser = 0, pageSizeUser = 1;
    private Pageable pageable;


    public ServiceFriendship(FriendshipDbRepository relations) {
        this.relations = relations;
    }

    /**
     * The save method
     * @param user1 - the first user
     * @param user2 - the second user
     */
    public void save(User user1, User user2) throws ValidationException, IllegalArgumentException, ServiceException, RepositoryException {
        Friendship relation = new Friendship(user1, user2);
        relation.setId(new Pair(user1.getId(), user2.getId()));
        relations.save(relation);
        notifyObservers(new FriendshipChangedEvent(ChangeEventType.ADD, relation));
    }

    /**
     * delete function
     * @param idUser1 of the first user
     * @param idUser2 of the second user
     * @throws IllegalArgumentException if the id is null
     * @throws ServiceException if the friendship does not exist
     */
    public void delete(Long idUser1, Long idUser2) throws IllegalArgumentException, ServiceException{
        Pair id = new Pair(idUser1, idUser2);
        Friendship relation = relations.delete(id);
        if(relation == null)
            throw new ServiceException("No relation was found between the tho given users.");
        notifyObservers(new FriendshipChangedEvent(ChangeEventType.DELETE, relation));
    }

    /**
     * findAll method
     * @return the Iterable form of the map
     */
    public ArrayList<FriendshipDTO> findAll(){
        ArrayList<FriendshipDTO> friends = new ArrayList<>();
        relations.findAll().forEach(r -> {
            User user1 = r.getFirstUser();
            User user2 = r.getSecondUser();
            LocalDate date = r.getDate();
            FriendshipDTO relationDTO = new FriendshipDTO(user1, user2, date);
            friends.add(relationDTO);
        });
        return friends;
    }

    /**
     * getFriends is used in order to get the friend list of a user
     * @param user - the user
     * @return the friend list
     */
    public List<Friend> getFriends(User user){
        return user.getFriendsList();
    }

    /**
     * getFriends is used in order to get the friend list of a user
     * @param user - the user
     * @return the friend list of a user but in DTO form
     */
    public List<FriendDTO> getFriendsDTO(User user){
        return user.getFriendsList().stream()
                .map(dto -> new FriendDTO(dto.getId(), dto.getFirstName(), dto.getLastName(), dto.getDate()))
                .collect(Collectors.toList());
    }

    /**
     * getUserFriendsDTOByDate is used in order to get the friends for which the relation started in a certain month
     * @param user - the user
     * @param monthStr - the specified month
     * @return - the searched list of friends
     */
    public List<FriendDTO> getUserFriendsDTOByDate(User user, String monthStr){
        Month month = Month.valueOf(monthStr.toUpperCase());
        return user.getFriendsList().stream()
                .filter(f -> f.getDate().getMonth().equals(month))
                .map(dto -> new FriendDTO(dto.getId(), dto.getFirstName(), dto.getLastName(), dto.getDate()))
                .collect(Collectors.toList());
    }

    public Friendship findOne(Long idUser1, long idUser2){
        return relations.findOne(new Pair<>(idUser1, idUser2));
    }

    @Override
    public void addObserver(Observer<FriendshipChangedEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendshipChangedEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendshipChangedEvent t) {
        observers.stream().forEach(obs->obs.update(t));
    }

    public void setPageSizeGeneral(int pageSizeGeneral) {
        this.pageSizeGeneral = pageSizeGeneral;
    }

    public void setPageSizeUser(int pageSizeUser) {
        this.pageSizeUser = pageSizeUser;
    }

    public List<Friendship> getFriendshipsOnPage(int page){
        this.pageGeneral = page;
        pageable = new PageableImplementation(page, pageSizeGeneral);
        Page<Friendship> friendshipPage = relations.findAll(pageable);
        return friendshipPage.getContent().collect(Collectors.toList());
    }

    public List<Friendship> getFriendshipsOnPageForUser(int page, Long idUser){
        this.pageUser = page;
        pageable = new PageableImplementation(page, pageSizeUser);
        Page<Friendship> friendshipPage = relations.findAll(pageable, idUser);
        return friendshipPage.getContent().collect(Collectors.toList());
    }

    public List<Friendship> getNextFriendships(){
        List<Friendship> friendships = getFriendshipsOnPage(pageGeneral + 1);
        if(friendships.size() <= 0){
            this.pageGeneral --;
            return new ArrayList<>();
        }
        return friendships;
    }

    public List<Friendship> getNextFriendshipsForUser(Long idUser){
        List<Friendship> friendships = getFriendshipsOnPageForUser(pageUser + 1, idUser);
        if(friendships.size() <= 0){
            this.pageUser --;
            return new ArrayList<>();
        }
        return friendships;
    }


}
