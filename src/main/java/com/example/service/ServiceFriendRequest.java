package com.example.service;

import com.example.domain.FriendRequest;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.repository.db.FriendRequestDbRepository;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.PageableImplementation;
import com.example.utils.Pair;
import com.example.utils.Status;
import com.example.utils.events.ChangeEventType;
import com.example.utils.events.RequestChangeEvent;
import com.example.utils.observer.Observable;
import com.example.utils.observer.Observer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceFriendRequest implements Observable<RequestChangeEvent> {
    private FriendRequestDbRepository friendRequests;
    private List<Observer<RequestChangeEvent>> observers=new ArrayList<>();
    private int pageGeneral = 0, pageSizeGeneral = 1;
    private  int pageReceived = 0, pageSizeReceived = 1;
    private  int pageSent = 0, pageSizeSent = 1;
    private Pageable pageable;

    public ServiceFriendRequest(FriendRequestDbRepository friendRequests) {
        this.friendRequests = friendRequests;
    }

    /**
     * sendFriendRequest is used in order to send a friend request
     * @param id_sender - the id of the sender
     * @param id_receiver - the id of the receiver
     * @throws ValidationException
     * @throws ServiceException
     */
    public void sendFriendRequest(Long id_sender, Long id_receiver) throws ValidationException, ServiceException {
        FriendRequest friendRequest = friendRequests.findOne(new Pair<>(id_receiver, id_sender));

        if(friendRequest != null) {
            Status status = friendRequest.getStatus();
            switch (status) {
                case pending -> throw new ServiceException("There is already a friend request from " + id_receiver + " to " + id_sender);
                case approved -> throw new ServiceException("The users are already friends");
                case rejected -> friendRequests.delete(new Pair<>(id_receiver, id_sender));
            }
        }

        friendRequest = friendRequests.save(new FriendRequest(id_sender, id_receiver));

        if(friendRequest != null) {
            friendRequest = friendRequests.findOne(new Pair<>(id_sender, id_receiver));
            if(friendRequest.getStatus() == Status.rejected) {
                FriendRequest newFriendRequest = new FriendRequest(id_sender, id_receiver, Status.pending, LocalDate.now());
                friendRequests.update(newFriendRequest);
                notifyObservers(new RequestChangeEvent(ChangeEventType.UPDATE, newFriendRequest, friendRequest));
            }
            else
                throw new ServiceException("The request already exists");
        }
        notifyObservers(new RequestChangeEvent(ChangeEventType.ADD, null));

    }

    /**
     * deleteFriendRequest is used in order to delete a certain firnedRequest
     * @param id_sender - the id of the sender
     * @param id_receiver - the id of the receiver
     * @throws ServiceException
     */
    public void deleteFriendRequest(Long id_sender, Long id_receiver) throws ServiceException {
        FriendRequest friendRequest = friendRequests.delete(new Pair<>(id_sender, id_receiver));
        if(friendRequest == null)
            throw new ServiceException("The friend request does not exist");
        else
            notifyObservers(new RequestChangeEvent(ChangeEventType.DELETE, friendRequest));
    }

    /**
     * updateFriendRequest is used in order to modify the satus of a certain FriendRequest
     * when a friendRequest is accepted, this function is called
     * @param id_sender
     * @param id_receiver
     * @param newStatus
     * @throws ValidationException
     * @throws ServiceException
     */
    public void updateFriendRequest(Long id_sender, Long id_receiver, String newStatus) throws ValidationException, ServiceException {
        Status status;
        try {
            status = Status.valueOf(newStatus);
        } catch (IllegalArgumentException e) {
            throw new ValidationException("Status is not valid");
        }

        FriendRequest friendRequest = friendRequests.update(new FriendRequest(id_sender, id_receiver, status));
        if (friendRequest != null)
            throw new ServiceException("The request does not exist");
        else
            notifyObservers(new RequestChangeEvent(ChangeEventType.UPDATE, friendRequest, friendRequest));
    }

    public Iterable<FriendRequest> findAll() {
        return friendRequests.findAll();
    }

    public FriendRequest findOne(Long idSender, Long idReceiver){
        return friendRequests.findOne(new Pair<>(idSender, idReceiver));
    }

    @Override
    public void addObserver(Observer<RequestChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<RequestChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(RequestChangeEvent t) {
        observers.stream().forEach(x->x.update(t));
    }

    public void setPageSizeGeneral(int pageSizeGeneral) {
        this.pageSizeGeneral = pageSizeGeneral;
    }

    public void setPageSizeReceived(int pageSizeReceived) {
        this.pageSizeReceived = pageSizeReceived;
    }

    public void setPageSizeSent(int pageSizeSent) {
        this.pageSizeSent = pageSizeSent;
    }

    public List<FriendRequest> getFriendRequestsOnPage(int page){
        this.pageGeneral = page;
        pageable = new PageableImplementation(page, pageSizeGeneral);
        Page<FriendRequest> friendRequestPage = friendRequests.findAll(pageable);
        return friendRequestPage.getContent().collect(Collectors.toList());
    }

    public List<FriendRequest> getFriendRequestsReceivedOnPage(int page, Long idUser){
        this.pageReceived = page;
        pageable = new PageableImplementation(page, pageSizeReceived);
        Page<FriendRequest> friendRequestPage = friendRequests.findAllReceived(pageable, idUser);
        return friendRequestPage.getContent().collect(Collectors.toList());
    }

    public List<FriendRequest> getFriendRequestsSentOnPage(int page, Long idUser){
        this.pageSent = page;
        pageable = new PageableImplementation(page, pageSizeSent);
        Page<FriendRequest> friendRequestPage = friendRequests.findAllSent(pageable, idUser);
        return friendRequestPage.getContent().collect(Collectors.toList());
    }

    public List<FriendRequest> getNextFriendRequests(){
        List<FriendRequest> friendRequests = getFriendRequestsOnPage(pageGeneral + 1);
        if(friendRequests.size() <= 0) {
            this.pageGeneral--;
            return new ArrayList<>();
        }
        return  friendRequests;
    }


    public List<FriendRequest> getNextFriendRequestsReceived(Long idUser){
        List<FriendRequest> friendRequests = getFriendRequestsReceivedOnPage(pageReceived + 1, idUser);
        if(friendRequests.size() <= 0) {
            this.pageReceived--;
            return new ArrayList<>();
        }
        return  friendRequests;
    }

    public List<FriendRequest> getNextFriendRequestsSent(Long idUser){
        List<FriendRequest> friendRequests = getFriendRequestsSentOnPage(pageSent + 1, idUser);
        if(friendRequests.size() <= 0) {
            this.pageSent--;
            return new ArrayList<>();
        }
        return  friendRequests;
    }
}
