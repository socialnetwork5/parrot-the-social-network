package com.example.service;

import com.example.domain.User;
import com.example.exceptions.RepositoryException;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.repository.db.UserDbRepository;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.PageableImplementation;
import com.example.utils.Sex;
import com.example.utils.events.ChangeEventType;
import com.example.utils.events.UserChangedEvent;
import com.example.utils.observer.Observable;
import com.example.utils.observer.Observer;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceUser implements Observable<UserChangedEvent> {
    private UserDbRepository users;
    private List<Observer<UserChangedEvent>> observers = new ArrayList<>();
    private int page = 0, pageSize = 1;
    private Pageable pageable;

    /**
     * Constructor class granted access to both repos
     * The maximum id is selected from the map and at every save the id is incremented
     * @param users first repo
     *
     */
    public ServiceUser(UserDbRepository users) {
        this.users = users;
    }

    /**
     * Save a user
     * @param first_name of the user
     * @param last_name of the user
     * @param sex of the user(male/female)
     * @param email of the user(example@mail.com)
     * @param birthdate must be above 16 years old
     * @throws ValidationException if not valid
     * @throws IllegalArgumentException if id is null
     * @throws ServiceException if id already exist or mail already exist
     * @throws ParseException if bithdate not in correct format
     * @throws DateTimeParseException if bithdate not in correct format
     */
    public void save(String first_name, String last_name, String sex, String email, LocalDate birthdate) throws ValidationException, IllegalArgumentException, RepositoryException, ServiceException, DateTimeParseException {
        Sex newSex;
        try {
            newSex = Sex.valueOf(sex.toUpperCase());
        }catch (IllegalArgumentException e){
            throw new ValidationException("sex is invalid");
        }
        User user = new User(first_name, last_name, newSex, email, birthdate);
        User save = users.save(user);
        if(save != null)
            throw new ServiceException("Id already in use!");

        notifyObservers(new UserChangedEvent(ChangeEventType.ADD, user));
    }

    /**
     * deletes a user from the map and from all friends list
     * @param id
     * @throws IllegalArgumentException if id is null
     * @throws ServiceException if user does not exist
     */
    public void delete(Long id) throws IllegalArgumentException, ServiceException{
        User user = users.delete(id);
        if(user == null)
            throw new ServiceException("No user with the given id.");
        notifyObservers(new UserChangedEvent(ChangeEventType.DELETE, user));
    }

    public void update(long id, String first_name, String last_name, String sex, String email, LocalDate birthdate)throws ValidationException, IllegalArgumentException, RepositoryException, ServiceException, DateTimeParseException{
        Sex newSex;
        try {
            newSex = Sex.valueOf(sex.toUpperCase());
        }catch (IllegalArgumentException e){
            throw new ValidationException("sex is invalid");
        }
        User user = new User(first_name, last_name, newSex, email, birthdate);
        user.setId(id);
        User update = users.update(user);
        if(update != null)
            throw new ServiceException("No user with the given id.");
        notifyObservers(new UserChangedEvent(ChangeEventType.UPDATE, user, update));
    }

    /**
     * findAll method
     * @return iterable form of the map
     */
    public Iterable<User> findAll(){
        return users.findAll();
    }

    public User findOne(long id) throws ServiceException {
        User user = users.findOne(id);
        if(user == null)
            throw new ServiceException("No user with the given id.");
        return user;
    }

    public Long getIdByMail(String mail){
         Iterable<User> all = users.findAll();
         User[] user = new User[1];
         user[0] = (User)StreamSupport.stream(all.spliterator(), false).
                 filter(user2 -> user2.getEmail().equals(mail)).findFirst().orElse(null);
        return user[0].getId();
    }

    public void updateProfilePicture(User user, String path) throws ValidationException {
        user.setProfilePicture(path);
        users.update(user);
    }

    @Override
    public void addObserver(Observer<UserChangedEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<UserChangedEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(UserChangedEvent t) {
        observers.stream().forEach(obs->obs.update(t));
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<User> getUsersOnPage(int page){
        this.page = page;
        pageable = new PageableImplementation(page, pageSize);
        Page<User> userPage = users.findAll(pageable);
        return userPage.getContent().collect(Collectors.toList());
    }

    public List<User> getNextUsers(){
        List<User> userList = getUsersOnPage(page + 1);
        if(userList.size() <= 0){
            this.page--;
            return new ArrayList<>();
        }
        return userList;
    }
}
