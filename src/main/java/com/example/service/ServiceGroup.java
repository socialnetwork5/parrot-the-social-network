package com.example.service;

import com.example.domain.Group;
import com.example.exceptions.ValidationException;
import com.example.repository.Repository;
import com.example.repository.db.GroupDbRepository;
import com.example.utils.events.GroupChangeEvent;
import com.example.utils.events.UserChangedEvent;
import com.example.utils.observer.Observable;
import com.example.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceGroup implements Observable<GroupChangeEvent> {

    GroupDbRepository groups;
    private List<Observer<GroupChangeEvent>> observers = new ArrayList<>();

    public ServiceGroup(GroupDbRepository groups) {
        this.groups = groups;
    }

    public Group findOne(Long id){
        return groups.findOne(id);
    }

    public List<Group> findAll(){
        return StreamSupport.stream(groups.findAll().spliterator(), false).collect(Collectors.toList());
    }

    public Group searchConversation(Long user1, Long user2) throws ValidationException {
        Optional<Group> searched = findAll().stream()
                .filter(g -> g.isAConversation())
                .filter(g -> g.getMembers().contains(user1))
                .filter(g -> g.getMembers().contains(user2))
                .findFirst();

        if(searched.isPresent())
            return searched.get();
        return null;
    }

    public Group createGroup(String name, List<Long> members, String picture) throws ValidationException {
        Group group = new Group(members);
        group.setName(name);
        if(picture!=null)
            group.setPicture(picture);
        return saveGroup(group);
    }

    public Group saveGroup(Group group) throws ValidationException {
        Group saved = groups.save(group);
        notifyObservers(new GroupChangeEvent(null, null));
        return saved;
    }

    public void updateLastMessage(long id, long lastMessage) throws ValidationException {
        Group group = groups.findOne(id);
        group.setLastMessage(lastMessage);
        groups.update(group);
    }

    @Override
    public void addObserver(Observer<GroupChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<GroupChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(GroupChangeEvent t) {
        observers.stream().forEach(o -> o.update(t));
    }
}
