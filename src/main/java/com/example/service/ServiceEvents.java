package com.example.service;

import com.example.domain.Event;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.repository.db.EventDbRepository;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.PageableImplementation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceEvents {
    private EventDbRepository repository;

    public ServiceEvents(EventDbRepository repository) {
        this.repository = repository;
    }
    private int page = 0, pageSize = 1;
    private Pageable pageable;


    public void saveEvent(long id_user, String name, String description, LocalDateTime time, String location, String picture) throws ValidationException {
        Event event = new Event(name, location, id_user, time);
        if(description != null)
            event.setDescription(description);
        if(picture != null)
            event.setPicture(picture);
        repository.save(event);
    }

    public void deleteEvent(Long id_event) throws ServiceException {
        Event event = repository.delete(id_event);
        if(event == null)
            throw new ServiceException("No event with the given id");
    }

    public List<Event> findAll(){
        return StreamSupport.stream(repository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    public List<Event> findAllAfterNow(){
        return repository.findAllAfterNow();
    }

    public Event findOne(Long id){
        return repository.findOne(id);
    }

    public void setPageSize(int size){
        this.pageSize = size;
    }

    public List<Event> getEventsOnPage(int page){
        this.page = page;
        pageable = new PageableImplementation(page, pageSize);
        Page<Event> eventPage = repository.findAllAfter(pageable);
        return eventPage.getContent().collect(Collectors.toList());
    }

    public List<Event> getNextEvents(){
        List<Event> events = getEventsOnPage(page + 1);
        if(events.size() <= 0) {
            this.page--;
            return new ArrayList<>();
        }
        return getEventsOnPage(page);
    }
}
