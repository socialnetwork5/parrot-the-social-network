package com.example.service;

import com.example.domain.*;
import com.example.dto.FriendDTO;
import com.example.dto.FriendshipDTO;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SuperServiceAdmin {
    private ServiceUser serviceUser;
    private ServiceFriendship serviceFriendship;

    public SuperServiceAdmin(ServiceUser serviceUser, ServiceFriendship serviceFriendship) {
        this.serviceUser = serviceUser;
        this.serviceFriendship = serviceFriendship;
    }

    /**
     * USER FUNCTIONS SECTION STARTS
     */

    /**
     * saveUser saves a  user using the savemethod from ServiceUser
     * @param first_name of the user
     * @param last_name of the user
     * @param sex of the user
     * @param email of the user
     * @param birthdate of the user
     * @throws ValidationException
     * @throws ServiceException
     */
    public void saveUser(String first_name, String last_name, String sex, String email, LocalDate birthdate) throws ValidationException, ServiceException {
        serviceUser.save(first_name, last_name, sex, email, birthdate);
    }

    /**
     * deleets a user
     * @param id of the user to be deleted
     * @throws ServiceException
     */
    public void deleteUser(long id) throws ServiceException {
        serviceUser.delete(id);
    }

    /**
     * updates user info
     * @param id of the user
     * @param first_name of the user
     * @param last_name of the user
     * @param sex of the user
     * @param email of the user
     * @param birthdate of the user
     * @throws ValidationException
     * @throws ServiceException
     */
    public void updateUser(long id, String first_name, String last_name, String sex, String email, LocalDate birthdate) throws ValidationException, ServiceException {
        serviceUser.update(id, first_name, last_name, sex, email, birthdate);
    }

    /**
     * finds a user with a given id
     * @param id
     * @return the searched user
     * @throws ServiceException
     */
    public User findOneUser(Long id) throws ServiceException {
        return serviceUser.findOne(id);
    }

    /**
     * returns the list of users
     * @return
     */
    public Iterable<User> findAllUser(){
        return serviceUser.findAll();
    }

    /**
     * USER FUNCTIONS SECTION ENDS
     */

    /**
     * FRIENDSHIP FUNCTIONS SECTION STARTS
     */

    /**
     * findAllFriendships
     * @return the list of friendships
     */
    public ArrayList<FriendshipDTO> findAllFriendship(){
        return serviceFriendship.findAll();
    }

    /**
     * getFriends
     * @param id of the user which friends are searched
     * @return the list of friends
     * @throws ServiceException
     */
    public List<Friend> getFriends(long id) throws ServiceException {
        User user = serviceUser.findOne(id);
        return serviceFriendship.getFriends(user);
    }

    /**
     * getFriends
     * @param id of the user which friends are searched
     * @return the list of friends in DTO form
     * @throws ServiceException
     */
    public List<FriendDTO> getFriendsDTO(long id) throws ServiceException {
        User user = serviceUser.findOne(id);
        return serviceFriendship.getFriendsDTO(user);
    }

    /**
     * getFriends
     * @param id of the user which friends are searched
     * @return the list of friends in DTO fotm
     * @throws ServiceException
     */
    public List<FriendDTO> getFriendsDTOByDate(long id, String month) throws ServiceException {
        User user = serviceUser.findOne(id);
        return serviceFriendship.getUserFriendsDTOByDate(user, month);
    }

    public Integer noCommunities(){
        ArrayList<ArrayList<User>> communities = new ArrayList<>();
        findConnectedComponents(communities);
        return communities.size();
    }

    public ArrayList<User> mostSociableCommunity(){
        ArrayList<ArrayList<User>> comms = new ArrayList<>();
        findConnectedComponents(comms);
        ArrayList<User> maxCom = null;
        int max = 0;
        for (ArrayList<User> comm : comms)
            if (comm.size() > max) {
                max = comm.size();
                maxCom = comm;
            }
        return maxCom;
    }

    private void dfs(User user, ArrayList<Long> visited, ArrayList<User> component) throws ServiceException {
        visited.add(user.getId());
        component.add(user);
        getFriends(user.getId()).forEach(friend -> {
            if (visited.indexOf(friend.getId())<0) {
                try {
                    dfs(friend, visited, component);
                } catch (ServiceException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void findConnectedComponents(ArrayList<ArrayList<User>> components) {
        ArrayList<Long> visited = new ArrayList<>();
        serviceUser.findAll().forEach(user -> {
            ArrayList<User> component = new ArrayList<>();
            if (visited.indexOf(user.getId())<0) {
                try {
                    dfs(user, visited, component);
                } catch (ServiceException e) {
                    e.printStackTrace();
                }
                components.add(component);
            }
        });
    }

    public Long getIdByMail(String mail){
        return serviceUser.getIdByMail(mail);
    }

}
