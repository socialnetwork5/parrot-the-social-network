package com.example.service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.security.auth.login.CredentialException;
import java.security.InvalidKeyException;

public class ServiceLogIn {
    private ServiceCredentials serviceCredentials;
    private static ServiceLogIn serviceLogIn = null;

    private ServiceLogIn(ServiceCredentials serviceCredentials) {
        this.serviceCredentials = serviceCredentials;
    }

    public static ServiceLogIn getInstance(ServiceCredentials serviceCredentials){
        if(serviceLogIn == null){
            serviceLogIn = new ServiceLogIn(serviceCredentials);
        }
        return serviceLogIn;
    }

    public static ServiceLogIn getInstance(){
        return serviceLogIn;
    }

    public Long authenticate(String username, String password) throws CredentialException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        return serviceCredentials.authenticate(username, password);
    }
}
