package com.example.service;

import com.example.domain.Credentials;
import com.example.exceptions.ValidationException;
import com.example.repository.Repository;
import com.example.utils.Security;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.security.auth.login.CredentialException;
import java.security.InvalidKeyException;

public class ServiceCredentials {
    Repository<String, Credentials> repository;
    Security security;

    public ServiceCredentials(Repository<String, Credentials> repository) {
        this.repository = repository;
        this.security = new Security();
    }

    public Long authenticate(String username, String password) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, CredentialException {
        Credentials credentials = repository.findOne(username);
        if (credentials == null)
            throw new CredentialException("The username or password is incorrect");
        if (security.checkPassword(password, credentials.getPassword()))
            return credentials.getUserId();
        throw new CredentialException("The username or password is incorrect");
    }

    public void save(String username, String password, Long userId) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, ValidationException {
        String pass = security.encryptPassword(password);
        Credentials credentials = new Credentials(username, pass, userId);
        repository.save(credentials);
    }

    public void update(String username, String password, Long userId) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, ValidationException {
        String pass = security.encryptPassword(password);
        Credentials credentials = new Credentials(username, pass, userId);
        repository.update(credentials);
    }
}
