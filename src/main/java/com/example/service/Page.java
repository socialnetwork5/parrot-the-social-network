package com.example.service;

import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.utils.Sex;

import java.time.LocalDate;

public class Page {
    private SuperServiceUser superServiceUser;
    private String firstName, lastName, email, profilePicture;
    private Sex sex;
    private LocalDate birthDate;

    public Page(SuperServiceUser superServiceUser) {
        this.superServiceUser = superServiceUser;
        User user = null;
        try {
            user = superServiceUser.getUser();
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        firstName = user.getFirstName();
        lastName = user.getLastName();
        email = user.getEmail();
        profilePicture = user.getProfilePicture();
        sex = user.getSex();
        birthDate = user.getBirthdate();
    }

    public SuperServiceUser getSuperServiceUser() {
        return superServiceUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public Sex getSex() {
        return sex;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public void setSuperServiceUser(SuperServiceUser superServiceUser) {
        this.superServiceUser = superServiceUser;
    }

}
