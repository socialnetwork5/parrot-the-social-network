package com.example.service;

import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import java.time.LocalDate;

public class ServiceSignUp {

    private ServiceCredentials serviceCredentials;
    private ServiceUser serviceUser;
    private static ServiceSignUp serviceSignUp = null;

    private ServiceSignUp(ServiceCredentials serviceCredentials, ServiceUser serviceUser) {
        this.serviceCredentials = serviceCredentials;
        this.serviceUser = serviceUser;
    }

    public static ServiceSignUp getInstance(ServiceCredentials serviceCredentials, ServiceUser serviceUser){
        if(serviceSignUp == null){
            serviceSignUp = new ServiceSignUp(serviceCredentials, serviceUser);
        }
        return serviceSignUp;
    }

    public static ServiceSignUp getInstance(){
        return serviceSignUp;
    }

    public void signUp(String username, String pass, String first_name, String last_name, String sex, String email, LocalDate birthdate) throws ValidationException, ServiceException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        serviceUser.save(first_name, last_name, sex, email, birthdate);
        Long id = serviceUser.getIdByMail(email);
        serviceCredentials.save(username, pass, id);
    }
}
