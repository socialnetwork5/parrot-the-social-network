package com.example.service;


import com.example.domain.Message;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.repository.db.MessageDbRepository;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.PageableImplementation;
import com.example.utils.events.ChangeEventType;
import com.example.utils.events.MessageChangeEvent;
import com.example.utils.observer.Observable;
import com.example.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceMessage implements Observable<MessageChangeEvent> {

    private MessageDbRepository messages;
    private List<Observer<MessageChangeEvent>> observers = new ArrayList<>();
    private int pageGeneral = 0, pageSizeGeneral = 1;
    private int pageGroup = 0, pageSizeGroup = 1;
    private Pageable pageable;

    public ServiceMessage(MessageDbRepository messages) {
        this.messages = messages;
    }

    /**
     * Send method
     * @param idSender - id of the sender
     * @param idGroup - id of the group
     * @param text - the text of the message
     * @throws ValidationException
     */
    public long send(long idSender, long idGroup, String text) throws ValidationException {
        Message message = new Message(idSender, idGroup, text);
        Long id = messages.saveWithReturn(message);
        message.setId(id);
        notifyObservers(new MessageChangeEvent(ChangeEventType.ADD, message));
        return id;
    }

    /**
     * Reply method
     * @param replyAt - id of the message that we want to reply
     * @param idSender - id of the sender
     * @param text - the text of the message
     * @throws ValidationException
     * @throws ServiceException
     */
    public long reply(long replyAt, long idSender, String text) throws ValidationException, ServiceException {
        Message message = messages.findOne(replyAt);
        if(message == null)
            throw new ServiceException("No message with the given id.");
        Message reply = new Message(idSender, message.getGroup(), text);
        reply.setReplyAt(replyAt);
        long idReply = messages.saveWithReturn(reply);
        reply.setId(idReply);
        notifyObservers(new MessageChangeEvent(ChangeEventType.ADD, reply));
        return idReply;
    }

    /**
     * findAll method returns the entire list of messages
     * @return
     */
    public Iterable<Message> findAll(){
        return messages.findAll();
    }


    /**
     * getGroupChat
     * @param idGroup - the id f the group
     * @return - all the messages from the group
     */
    public List<Message> getGroupChat(Long idGroup){
        return  messages.getGroupChat(idGroup);
    }

    /**
     * finds a specific message
     * @param id - of the searched messageamos
     * @return the seached message
     * @throws ServiceException
     */
    public Message findOne(long id) throws ServiceException {
        Message message= messages.findOne(id);
        if(message == null)
            throw new ServiceException("No message with the given id.");
        return message;
    }

    /**
     * deletes a certain message
     * @param id - of the message we want to delete
     * @throws IllegalArgumentException
     * @throws ServiceException
     */
    public void delete(Long id) throws IllegalArgumentException, ServiceException{
        Message message = messages.delete(id);
        if(message == null)
            throw new ServiceException("No message with the given id.");
        notifyObservers(new MessageChangeEvent(ChangeEventType.DELETE, message));
    }

    public Message getMessage(Long idMessage) {
        return messages.findOne(idMessage);
    }

    @Override
    public void addObserver(Observer<MessageChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<MessageChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(MessageChangeEvent t) {
        observers.forEach(observer -> observer.update(t));
    }

    public void setPageSizeGeneral(int size){
        this.pageSizeGeneral = size;
    }
    public void setPageSizeGroup(int size){
        this.pageSizeGroup = size;
    }

    public List<Message> getMessagesOnPage(int page){
        this.pageGeneral = page;
        pageable = new PageableImplementation(page, pageSizeGeneral);
        Page<Message> messagePage = messages.findAll(pageable);
        return messagePage.getContent().collect(Collectors.toList());
    }

    public List<Message> getMessagesOnPage(int page, Long idGroup){
        this.pageGroup = page;
        pageable = new PageableImplementation(page, pageSizeGroup);
        Page<Message> messagePage = messages.findAll(pageable, idGroup);
        return messagePage.getContent().collect(Collectors.toList());
    }


    public List<Message> getPrevMessages(){
        this.pageGeneral--;
        return getMessagesOnPage(pageGeneral);
    }

    public List<Message> getPrevMessages(Long idGroup){
        this.pageGroup--;
        if(pageGroup >= 0)
            return getMessagesOnPage(pageGroup, idGroup);
        return new ArrayList<>();
    }

    public int getPageGroup(){
        return pageGroup;
    }

}
