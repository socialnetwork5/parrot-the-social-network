package com.example.service;

import com.example.domain.Event;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.utils.events.ChangeEventType;
import com.example.utils.events.EventChangedEvent;
import com.example.utils.observer.Observable;
import com.example.utils.observer.Observer;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EventManager implements Observable<EventChangedEvent> {

    private ServiceEvents serviceEvents;
    private ServiceEnrollments serviceEnrollments;
    private List<Observer<EventChangedEvent>> observers = new ArrayList<>();

    public EventManager(ServiceEvents serviceEvents, ServiceEnrollments serviceEnrollments) {
        this.serviceEvents = serviceEvents;
        this.serviceEnrollments = serviceEnrollments;
    }

    public boolean IsUserEnrolled(long id_user, long id_event){
        return serviceEnrollments.isUserEnrolled(id_user, id_event);
    }

    public boolean areNotificationsOn(long id_user, long id_event){
        return serviceEnrollments.areNotificationsOn(id_user, id_event);
    }

    public void turnNotificationsOn(long id_user, long id_event){
        serviceEnrollments.turnNotificationsOn(id_user, id_event);
        notifyObservers(new EventChangedEvent(null, null));
    }

    public void turnNotificationsOff(long id_user, long id_event){
        serviceEnrollments.turnNotificationsOff(id_user, id_event);
        notifyObservers(new EventChangedEvent(null, null));
    }

    public List<Event> getNearbyEvents(){
        return serviceEvents.findAllAfterNow();
    }

    public void setPageSizeEvents(int pageSizeEvents){
        serviceEvents.setPageSize(pageSizeEvents);
    }

    public List<Event> getEventsOnPage(int page){
        return serviceEvents.getEventsOnPage(page);
    }

    public List<Event> getNextPage(){
        return serviceEvents.getNextEvents();
    }

    public void CreateEvent(String title, String description, String location, LocalDateTime time, Long organizer, String picture) throws ValidationException {
        serviceEvents.saveEvent(organizer, title, description, time, location, picture);
        notifyObservers(new EventChangedEvent(ChangeEventType.ADD, null));
    }

    public void deleteEvent(Long id_event) throws ServiceException {
        serviceEvents.deleteEvent(id_event);
        notifyObservers(new EventChangedEvent(ChangeEventType.DELETE, null));
    }

    public void enrollUser(Long id_user, Long id_event){
        serviceEnrollments.enrollUser(id_user, id_event);
        notifyObservers(new EventChangedEvent(null, null));
    }

    public List<Event> getUpcomingEvents(Long id_user){
        return serviceEnrollments.getEnrolled(id_user).stream().filter(e -> Duration.between(LocalDateTime.now(), e.getTime()).toDays()<7 && e.getTime().isAfter(LocalDateTime.now())).collect(Collectors.toList());
    }

    public List<Event> getNotificationsForUpcomingEvents(Long id_user){
        return getUpcomingEvents(id_user).stream().filter(e -> areNotificationsOn(id_user, e.getId())).collect(Collectors.toList());
    }

    public void unEnrollUser(Long id_user, Long id_event){
        serviceEnrollments.unEnrollUser(id_user, id_event);
        notifyObservers(new EventChangedEvent(null, null));
    }

    @Override
    public void addObserver(Observer<EventChangedEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<EventChangedEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(EventChangedEvent t) {
        observers.forEach(o -> o.update(t));
    }

    public ServiceEvents getServiceEvents(){
        return serviceEvents;
    }


    public List<Long> getEnrolledUsers(Long id){return serviceEnrollments.getEnrolledUsers(id);}
}
