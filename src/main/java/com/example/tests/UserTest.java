/*package ro.ubbcluj.map.tests;

import org.junit.jupiter.api.Test;
import ro.ubbcluj.map.domain.User;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    LocalDate birthdate = LocalDate.parse("2001-08-20");
    User user = new User("Amos", "Andreica", "Male", "amos.andreica@gmail.com", birthdate);

    @Test
    void getFirstName() {
        assertEquals(user.getFirstName(), "Amos");
    }

    @Test
    void getLastName() {
        assertEquals(user.getLastName(), "Andreica");
    }

    @Test
    void getSex() {
        assertEquals(user.getSex(), "Male");
    }

    @Test
    void getBirthdate() {
        assertEquals(user.getBirthdate(), birthdate);
    }

    @Test
    void setFirstName() {
        user.setFirstName("Marcel");
        assertEquals(user.getFirstName(), "Marcel");
    }

    @Test
    void setLastName() {
        user.setLastName("Marcel");
        assertEquals(user.getLastName(), "Marcel");
    }

    @Test
    void setSex() {
        user.setSex("Female");
        assertEquals(user.getSex(), "Female");
    }

    @Test
    void setBirthdate() {
        LocalDate birthdate2 = LocalDate.parse("2001-08-21");
        user.setBirthdate(birthdate2);
        assertEquals(user.getBirthdate(), birthdate2);
    }

    @Test
    void getEmail() {
        assertEquals(user.getEmail(), "amos.andreica@gmail.com");
    }

    @Test
    void setEmail() {
        user.setEmail("marcel.iures@mail.com");
        assertEquals(user.getEmail(), "marcel.iures@mail.com");
    }

}*/