package com.example.repository.db;

import com.example.domain.Message;
import com.example.domain.validators.Validator;
import com.example.exceptions.ValidationException;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.Paginator;
import com.example.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MessageDbRepository implements PagingRepository<Long, Message>{
    private String url;
    private String username;
    private String password;
    private Validator<Message> validator;

    public MessageDbRepository(String url, String username, String password, Validator<Message> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    /**
     * FindOne
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the searched Messages
     */
    public Message findOne(Long id) {
        String sql = "SELECT * from messages where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.isBeforeFirst() ) {
                return null;
            }
            resultSet.next();
            long idSender = resultSet.getLong("id_sender");
            long idGroup = resultSet.getLong("id_group");
            String text = resultSet.getString("text");
            LocalDateTime time = resultSet.getTimestamp("timestamp").toLocalDateTime();
            long replyAt = resultSet.getLong("reply_at");

            Message message= new Message(idSender, idGroup, text, time);
            if(replyAt != 0)
                message.setReplyAt(replyAt);
            message.setId(id);

            return message;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * FindAll
     * @return the entire list of Messages
     */
    public Iterable<Message> findAll() {
        Set<Message> messages = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from messages ORDER BY id");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                long idSender = resultSet.getLong("id_sender");
                long idGroup = resultSet.getLong("id_group");
                String text = resultSet.getString("text");
                LocalDateTime time = resultSet.getTimestamp("timestamp").toLocalDateTime();
                long replyAt = resultSet.getLong("reply_at");

                Message message= new Message(idSender, idGroup, text, time);
                if(replyAt != 0)
                    message.setReplyAt(replyAt);

                message.setId(id);
                messages.add(message);
            }
            return messages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Save
     * @param entity entity must be not null
     * @return null if the Message is successfully saved or the message if not
     * @throws ValidationException
     * @throws IllegalArgumentException
     */
    public Message save(Message entity) throws ValidationException, IllegalArgumentException {
        validator.validate(entity);
        String sql = "insert into messages (id_sender, text, timestamp, reply_at, id_group) values ( ?, ?, ?, ?, ?) returning id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getSender());
            ps.setString(2, entity.getText());
            ps.setTimestamp(3, Timestamp.valueOf(entity.getTime()));
            if(entity.getReplyAt() != null)
                ps.setLong(4, entity.getReplyAt());
            else
                ps.setNull(4, java.sql.Types.INTEGER);
            ps.setLong(5, entity.getGroup());
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();

            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    public Long saveWithReturn(Message entity) throws ValidationException {
        validator.validate(entity);
        String sql = "insert into messages (id_sender, text, timestamp, reply_at, id_group) values ( ?, ?, ?, ?, ?) returning id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getSender());
            ps.setString(2, entity.getText());
            ps.setTimestamp(3, Timestamp.valueOf(entity.getTime()));
            if(entity.getReplyAt() != null)
                ps.setLong(4, entity.getReplyAt());
            else
                ps.setNull(4, java.sql.Types.INTEGER);
            ps.setLong(5, entity.getGroup());
            ResultSet resultSet = ps.executeQuery();
            resultSet.next();

            Long id = resultSet.getLong("id");

            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Delete
     * @param id id must be not null
     * @return the Message if it is successfully deleted or null if not
     */
    public Message delete(Long id) {
        Message entity = findOne(id);
        if(entity == null)
            return null;
        String sql = "delete from messages where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    /**
     * Update
     * @param entity entity must not be null
     * @return null if it is successfully updated or the Message if not.
     * @throws ValidationException
     */
    public Message update(Message entity) throws ValidationException {
        validator.validate(entity);
        String sql = "update users set text = ?, reply_at = ? where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getText());
            ps.setLong(2, entity.getReplyAt());
            ps.setInt(3, entity.getId().intValue());
            int lines = ps.executeUpdate();
            if(lines == 1)
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    public List<Message> getGroupChat(long idGroup){
        String sql = "SELECT * FROM messages where id_group = " + idGroup + " ORDER BY id";
        List<Message> messages = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                long idSender = resultSet.getLong("id_sender");
                String text = resultSet.getString("text");
                LocalDateTime time = resultSet.getTimestamp("timestamp").toLocalDateTime();
                long replyAt = resultSet.getLong("reply_at");

                Message message= new Message(idSender, idGroup, text, time);
                if(replyAt != 0)
                    message.setReplyAt(replyAt);

                message.setId(id);
                messages.add(message);
            }
            return messages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Page<Message> findAll(Pageable pageable) {
        Paginator<Message> paginator = new Paginator<Message>(pageable, this.findAll());
        return paginator.paginate();
    }

    public Page<Message> findAll(Pageable pageable, Long idGroup){
        Paginator<Message> paginator = new Paginator<Message>(pageable, this.getGroupChat(idGroup));
        return paginator.paginate();
    }
}

