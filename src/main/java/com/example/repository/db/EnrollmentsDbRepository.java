package com.example.repository.db;

import com.example.domain.Credentials;
import com.example.domain.Event;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EnrollmentsDbRepository {
    private String url;
    private String username;
    private String password;

    public EnrollmentsDbRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public List<Event> getEnrolled(Long id_user){
        List<Event> events = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT events.* FROM events inner join attendings on id = id_event where id_user = ?")){
             statement.setLong(1, id_user);
             ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                Long organizer = resultSet.getLong("organizer");
                String picture = resultSet.getString("picture");
                String location = resultSet.getString("location");
                LocalDateTime time = resultSet.getTimestamp("time").toLocalDateTime();

                Event event = new Event(name, location, organizer, time);
                event.setId(id);
                if(description != null)
                    event.setDescription(description);
                if(picture != null)
                    event.setPicture(picture);

                events.add(event);
            }
            return events;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Long> getEnrolledUsers(Long id_event){
        List<Long> participants = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT id_user FROM attendings where id_event = ?")){
            statement.setLong(1, id_event);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                long id_user = resultSet.getLong("id_user");
                participants.add(id_user);
            }
            return participants;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isUserEnrolled(Long id_user, Long id_event){
        String sql = "SELECT * from attendings where id_user = ? and id_event = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id_user);
            ps.setLong(2, id_event);

            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.isBeforeFirst() ) {
                return false;
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean unEnrollUser(Long id_user, Long id_event){
        String sql = "DELETE from attendings where id_user = ? and id_event = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id_user);
            ps.setLong(2, id_event);

            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean enrollUser(Long id_user, Long id_event){
        String sql = "insert into attendings (id_user, id_event) values (?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id_user);
            ps.setLong(2, id_event);
            int lines = ps.executeUpdate();
            if(lines == 1)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean turnNotificationsOn(Long id_user, Long id_event){
        String sql = "update attendings set notifications = true where id_user=? and id_event=?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id_user);
            ps.setLong(2, id_event);
            int lines = ps.executeUpdate();
            if(lines == 1)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean turnNotificationsOff(Long id_user, Long id_event){
        String sql = "update attendings set notifications = false where id_user=? and id_event=?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id_user);
            ps.setLong(2, id_event);
            int lines = ps.executeUpdate();
            if(lines == 1)
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean areNotificationsOn(Long id_user, Long id_event){
        String sql = "SELECT * from attendings where id_user = ? and id_event = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id_user);
            ps.setLong(2, id_event);

            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.isBeforeFirst() ) {
                return false;
            }
            resultSet.next();
            boolean notification = resultSet.getBoolean("notifications");
                return notification;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
