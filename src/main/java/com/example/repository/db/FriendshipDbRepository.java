package com.example.repository.db;

import com.example.domain.Friendship;
import com.example.domain.User;
import com.example.domain.validators.Validator;
import com.example.exceptions.RepositoryException;
import com.example.exceptions.ValidationException;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.Paginator;
import com.example.repository.paging.PagingRepository;
import com.example.utils.Pair;
import com.example.utils.Sex;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FriendshipDbRepository implements PagingRepository<Pair<Long, Long>, Friendship> {
    private String url;
    private String username;
    private String password;
    private Validator<Friendship> validator;

    String existError = "ERROR: duplicate key value violates unique constraint";


    public FriendshipDbRepository(String url, String username, String password, Validator<Friendship> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    /**
     * FindOne
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the searched Friendship
     */
    @Override
    public Friendship findOne(Pair<Long, Long> id) {
        String sql = "SELECT u1.id as id_1,u1.first_name as first_name_1,u1.last_name as last_name_1,u1.sex as sex_1,u1.email as email_1,u1.birthdate as birthdate_1,u1.profile_image as image_1, u2.id as id_2,u2.first_name as first_name_2,u2.last_name as last_name_2,u2.sex as sex_2,u2.email as email_2,u2.birthdate as birthdate_2, u2.profile_image as image_2,f.date from users as u1 inner join friendships as f on u1.id = f.user1_id inner join users as u2 on u2.id = f.user2_id where user1_id = ? AND user2_id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, Long.min(id.getFirst(), id.getSecond()));
            ps.setLong(2, Long.max(id.getFirst(), id.getSecond()));
            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.isBeforeFirst() ) {
                return null;
            }
            resultSet.next();

            Long user1_id = resultSet.getLong("id_1");
            String first_name_1 = resultSet.getString("first_name_1");
            String last_name_1 = resultSet.getString("last_name_1");
            String sex_1 = resultSet.getString("sex_1");
            String email_1 = resultSet.getString("email_1");
            LocalDate birthdate_1 = resultSet.getDate("birthdate_1").toLocalDate();
            String image1 = resultSet.getString("image_1");

            Long user2_id = resultSet.getLong("id_2");
            String first_name_2 = resultSet.getString("first_name_2");
            String last_name_2 = resultSet.getString("last_name_2");
            String sex_2 = resultSet.getString("sex_2");
            String email_2 = resultSet.getString("email_2");
            LocalDate birthdate_2 = resultSet.getDate("birthdate_2").toLocalDate();
            String image2 = resultSet.getString("image_2");

            LocalDate date = resultSet.getDate("date").toLocalDate();

            User user1 = new User(first_name_1, last_name_1, Sex.valueOf(sex_1.toUpperCase()), email_1, birthdate_1);
            if(image1!=null)
                user1.setProfilePicture(image1);
            user1.setId(user1_id);
            User user2 = new User(first_name_2, last_name_2, Sex.valueOf(sex_2.toUpperCase()), email_2, birthdate_2);
            user2.setId(user2_id);
            if(image2!=null)
                user2.setProfilePicture(image2);

            Friendship relation = new Friendship(user1, user2, date);

            relation.setId(new Pair<>(user1_id, user2_id));
            return relation;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * FindAll
     * @return the entire list of Friendships
     */
    @Override
    public Iterable<Friendship> findAll() {
        Set<Friendship> relations = new HashSet<>();
        String sql = "SELECT u1.id as id_1,u1.first_name as first_name_1,u1.last_name as last_name_1,u1.sex as sex_1,u1.email as email_1,u1.birthdate as birthdate_1, u1.profile_image as image_1, u2.id as id_2,u2.first_name as first_name_2,u2.last_name as last_name_2,u2.sex as sex_2,u2.email as email_2,u2.birthdate as birthdate_2, u2.profile_image as image_2, f.date from users as u1 inner join friendships as f on u1.id = f.user1_id inner join users as u2 on u2.id = f.user2_id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long user1_id = resultSet.getLong("id_1");
                String first_name_1 = resultSet.getString("first_name_1");
                String last_name_1 = resultSet.getString("last_name_1");
                String sex_1 = resultSet.getString("sex_1");
                String email_1 = resultSet.getString("email_1");
                LocalDate birthdate_1 = resultSet.getDate("birthdate_1").toLocalDate();
                String image1 = resultSet.getString("image_1");

                Long user2_id = resultSet.getLong("id_2");
                String first_name_2 = resultSet.getString("first_name_2");
                String last_name_2 = resultSet.getString("last_name_2");
                String sex_2 = resultSet.getString("sex_2");
                String email_2 = resultSet.getString("email_2");
                LocalDate birthdate_2 = resultSet.getDate("birthdate_2").toLocalDate();
                String image2 = resultSet.getString("image_2");

                LocalDate date = resultSet.getDate("date").toLocalDate();

                User user1 = new User(first_name_1, last_name_1, Sex.valueOf(sex_1.toUpperCase()), email_1, birthdate_1);
                if(image1!=null)
                    user1.setProfilePicture(image1);
                user1.setId(user1_id);
                User user2 = new User(first_name_2, last_name_2, Sex.valueOf(sex_2.toUpperCase()), email_2, birthdate_2);
                if(image2!=null)
                    user2.setProfilePicture(image2);
                user2.setId(user2_id);

                Friendship relation = new Friendship(user1, user2, date);
                relation.setId(new Pair<>(user1_id, user2_id));
                relations.add(relation);
            }
            return relations;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Save
     * @param entity entity must be not null
     * @return null if the Friendship is successfully saved or the Friendship if not
     * @throws ValidationException
     * @throws IllegalArgumentException
     */
    @Override
    public Friendship save(Friendship entity) throws ValidationException, IllegalArgumentException, RepositoryException {
        validator.validate(entity);
        String sql = "insert into friendships (user1_id, user2_id, date) values (?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, Long.min(entity.getFirstUser().getId(), entity.getSecondUser().getId()));
            ps.setLong(2, Long.max(entity.getFirstUser().getId(), entity.getSecondUser().getId()));
            ps.setDate(3, Date.valueOf(entity.getDate()));
            int lines = ps.executeUpdate();
            if(lines == 1)
                return null;
        } catch (SQLException e) {
            if(e.getMessage().contains(existError))
                throw new RepositoryException("Friendship already exists");
            else
                e.printStackTrace();
        }
        return entity;
    }

    /**
     * Delete
     * @param id id must be not null
     * @return the Friendship if it is successfully deleted or null if not
     */
    @Override
    public Friendship delete(Pair<Long, Long> id) {
        Friendship entity = findOne(id);
        String sql = "delete from friendships where user1_id = ? and user2_id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, Long.min(id.getFirst(), id.getSecond()));
            ps.setLong(2, Long.max(id.getFirst(), id.getSecond()));
            ps.executeUpdate();
            return entity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship update(Friendship entity) {
        return null;
    }


    public List<Friendship> findAllForUser(Long idUser){
        List<Friendship> relations = new ArrayList<>();
        String sql = "SELECT u1.id as id_1,u1.first_name as first_name_1,\n" +
                "                u1.last_name as last_name_1,u1.sex as sex_1, \n" +
                "                u1.email as email_1,u1.birthdate as birthdate_1, \n" +
                "                 u1.profile_image as image_1, \n" +
                "                 u2.id as id_2,u2.first_name as first_name_2, \n" +
                "                u2.last_name as last_name_2,u2.sex as sex_2, \n" +
                "                u2.email as email_2,u2.birthdate as birthdate_2, \n" +
                "                 u2.profile_image as image_2, f.date  \n" +
                "                from users as u1  \n" +
                "                inner join friendships as f on u1.id = f.user1_id  \n" +
                "                inner join users as u2 on u2.id = f.user2_id \n" +
                "                where u1.id = " + idUser + " or u2.id =" + idUser;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long user1_id = resultSet.getLong("id_1");
                String first_name_1 = resultSet.getString("first_name_1");
                String last_name_1 = resultSet.getString("last_name_1");
                String sex_1 = resultSet.getString("sex_1");
                String email_1 = resultSet.getString("email_1");
                LocalDate birthdate_1 = resultSet.getDate("birthdate_1").toLocalDate();
                String image1 = resultSet.getString("image_1");

                Long user2_id = resultSet.getLong("id_2");
                String first_name_2 = resultSet.getString("first_name_2");
                String last_name_2 = resultSet.getString("last_name_2");
                String sex_2 = resultSet.getString("sex_2");
                String email_2 = resultSet.getString("email_2");
                LocalDate birthdate_2 = resultSet.getDate("birthdate_2").toLocalDate();
                String image2 = resultSet.getString("image_2");

                LocalDate date = resultSet.getDate("date").toLocalDate();

                User user1 = new User(first_name_1, last_name_1, Sex.valueOf(sex_1.toUpperCase()), email_1, birthdate_1);
                if(image1!=null)
                    user1.setProfilePicture(image1);
                user1.setId(user1_id);
                User user2 = new User(first_name_2, last_name_2, Sex.valueOf(sex_2.toUpperCase()), email_2, birthdate_2);
                if(image2!=null)
                    user2.setProfilePicture(image2);
                user2.setId(user2_id);

                Friendship relation = new Friendship(user1, user2, date);
                relation.setId(new Pair<>(user1_id, user2_id));
                relations.add(relation);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return relations;
    }

    @Override
    public Page<Friendship> findAll(Pageable pageable) {
        Paginator<Friendship> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }

    public Page<Friendship> findAll(Pageable pageable, Long idUser){
        Paginator<Friendship> paginator= new Paginator<>(pageable, this.findAllForUser(idUser));
        return paginator.paginate();
    }

}
