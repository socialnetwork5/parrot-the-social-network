package com.example.repository.db;

import com.example.domain.Event;
import com.example.domain.validators.Validator;
import com.example.exceptions.ValidationException;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.Paginator;
import com.example.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventDbRepository implements PagingRepository<Long, Event> {
    private String url;
    private String username;
    private String password;
    private Validator<Event> validator;

    public EventDbRepository(String url, String username, String password, Validator<Event> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }


    @Override
    public Event findOne(Long id) {
        String sql = "SELECT * from events where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.isBeforeFirst() ) {
                return null;
            }
            resultSet.next();
            String name = resultSet.getString("name");
            String description = resultSet.getString("description");
            Long organizer = resultSet.getLong("organizer");
            String picture = resultSet.getString("picture");
            String location = resultSet.getString("location");
            LocalDateTime time = resultSet.getTimestamp("time").toLocalDateTime();


            Event event = new Event(name, location, organizer, time);
            event.setId(id);
            if(description != null)
                event.setDescription(description);
            if(picture != null)
                event.setPicture(picture);

            return event;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Event> findAll() {
        Set<Event> events = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from events");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                Long organizer = resultSet.getLong("organizer");
                String picture = resultSet.getString("picture");
                String location = resultSet.getString("location");
                LocalDateTime time = resultSet.getTimestamp("time").toLocalDateTime();

                Event event = new Event(name, location, organizer, time);
                event.setId(id);
                if(description != null)
                    event.setDescription(description);
                if(picture != null)
                    event.setPicture(picture);

                events.add(event);
            }
            return events;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Event save(Event entity) throws ValidationException, IllegalArgumentException {
        validator.validate(entity);
        String sql = "insert into events (name, description, organizer, picture, time, location ) values ( ?, ?, ?, ?, ?, ?) returning id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getName());
            if(entity.getDescription() != null)
                ps.setString(2, entity.getDescription());
            else
                ps.setNull(2, Types.VARCHAR);
            ps.setLong(3, entity.getOrganizer());
            if(entity.getPicture() != null)
                ps.setString(4, entity.getPicture());
            else
                ps.setNull(4, Types.VARCHAR);
            ps.setTimestamp(5, Timestamp.valueOf(entity.getTime()));
            ps.setString(6, entity.getLocation());

            ResultSet resultSet = ps.executeQuery();
            resultSet.next();

            Long id = resultSet.getLong("id");

            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Event delete(Long id_event) {
        Event entity = findOne(id_event);
        if(entity == null){
            return null;
        }
        String sql = "DELETE from events where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, id_event);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Event update(Event entity) throws ValidationException {
        return null;
    }

    public List<Event> findAllAfterNow(){
        List<Event> events = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from events where time >= NOW()::timestamp order by id desc");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                Long organizer = resultSet.getLong("organizer");
                String picture = resultSet.getString("picture");
                String location = resultSet.getString("location");
                LocalDateTime time = resultSet.getTimestamp("time").toLocalDateTime();

                Event event = new Event(name, location, organizer, time);
                event.setId(id);
                if(description != null)
                    event.setDescription(description);
                if(picture != null)
                    event.setPicture(picture);

                events.add(event);
            }
            return events;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Page<Event> findAll(Pageable pageable) {
        Paginator<Event> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }
    public Page<Event> findAllAfter(Pageable pageable){
        Paginator<Event> paginator = new Paginator<>(pageable, this.findAllAfterNow());
        return paginator.paginate();
    }
}
