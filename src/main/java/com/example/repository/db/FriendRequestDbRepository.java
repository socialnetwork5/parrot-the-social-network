package com.example.repository.db;

import com.example.domain.FriendRequest;
import com.example.domain.validators.Validator;
import com.example.exceptions.ValidationException;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.Paginator;
import com.example.repository.paging.PagingRepository;
import com.example.utils.Pair;
import com.example.utils.Status;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FriendRequestDbRepository implements PagingRepository<Pair<Long, Long>, FriendRequest> {
    private String url;
    private String username;
    private String password;
    private Validator<FriendRequest> validator;

    public FriendRequestDbRepository(String url, String username, String password, Validator<FriendRequest> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    /**
     * FindOne
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the searched FriendRequest
     */
    @Override
    public FriendRequest findOne(Pair<Long, Long> id) {
        if (id == null)
            throw new IllegalArgumentException("id can't be null");

        String sql = "SELECT * FROM friend_requests WHERE id_sender = ? AND id_receiver = ?";

        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement ps = connection.prepareStatement(sql);)
        {
            ps.setLong(1, id.getFirst());
            ps.setLong(2, id.getSecond());
            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.next())
                return null;

            FriendRequest friendship = new FriendRequest(
                    resultSet.getLong("id_sender"),
                    resultSet.getLong("id_receiver"),
                    Status.valueOf(resultSet.getString("status")),
                    resultSet.getDate("date").toLocalDate()
            );
            friendship.setId(id);
            return friendship;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * FindAll
     * @return the entire list of FriendRequests
     */
    @Override
    public Iterable<FriendRequest> findAll() {
        Set<FriendRequest> friendRequests = new HashSet<>();
        String sql = "SELECT * FROM friend_requests";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()){

            while (resultSet.next()){
                Long id1 = resultSet.getLong("id_sender");
                Long id2 = resultSet.getLong("id_receiver");
                Status status = Status.valueOf(resultSet.getString("status"));
                LocalDate date = resultSet.getDate("date").toLocalDate();

                FriendRequest friendRequest = new FriendRequest(id1, id2, status, date);
                friendRequest.setId(new Pair<>(id1, id2));
                friendRequests.add(friendRequest);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return friendRequests;
    }

    /**
     * Save
     * @param entity entity must be not null
     * @return null if the entity is successfully saved or the entity if not
     * @throws ValidationException
     * @throws IllegalArgumentException
     */
    @Override
    public FriendRequest save(FriendRequest entity) throws ValidationException, IllegalArgumentException {
        if (entity == null)
            throw new IllegalArgumentException("entity can not be null");

        validator.validate(entity);

        String sql = "insert into friend_requests (id_sender, id_receiver, status, date)" +
                " values (?, ?, ?, ?)";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, entity.getIdSendingUser());
            ps.setLong(2, entity.getIdReceivingUser());
            ps.setString(3, entity.getStatus().toString());
            ps.setDate(4, Date.valueOf(entity.getDate()));
            try {
                ps.executeUpdate();
            }catch (SQLException e){
                return entity;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Delete
     * @param id id must be not null
     * @return the entity if it is successfully deleted or null if not
     */
    @Override
    public FriendRequest delete(Pair<Long, Long> id) {
        if (id == null)
            throw new IllegalArgumentException("id can't be null");

        String sqlDelete = "Delete from friend_requests WHERE id_sender = ? AND id_receiver = ?";
        String sqlSearch =  "SELECT * FROM friend_requests WHERE id_sender = ? and id_receiver = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement psDelete = connection.prepareStatement(sqlDelete);
            PreparedStatement psSearch = connection.prepareStatement(sqlSearch)) {

            psSearch.setLong(1, id.getFirst());
            psSearch.setLong(2, id.getSecond());

            ResultSet resultSet = psSearch.executeQuery();
            if (!resultSet.next())
                return null;

            FriendRequest friendRequest = new FriendRequest(
                    resultSet.getLong("id_sender"),
                    resultSet.getLong("id_receiver"),
                    Status.valueOf(resultSet.getString("status")),
                    resultSet.getDate("date").toLocalDate());

            friendRequest.setId(new Pair<>(resultSet.getLong("id_sender"),
                    resultSet.getLong("id_receiver")));

            psDelete.setLong(1, id.getFirst());
            psDelete.setLong(2, id.getSecond());
            psDelete.executeUpdate();
            return friendRequest;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Update
     * @param entity entity must not be null
     * @return null if it is successfully updated or the entity if not.
     * @throws ValidationException
     */
    @Override
    public FriendRequest update(FriendRequest entity) throws ValidationException {
        validator.validate(entity);
        String sql = "update friend_requests\n" +
                "set status = ?" +
                "where id_sender = ? and id_receiver = ?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getStatus().toString());
            ps.setLong(2, entity.getIdSendingUser());
            ps.setLong(3, entity.getIdReceivingUser());

            int lines = ps.executeUpdate();
            if(lines == 1)
                return null;
        } catch (SQLException e) {
                e.printStackTrace();
        }
        return entity;

    }

    public List<FriendRequest> findAllReceivedForUser(long idUser){
        List<FriendRequest> friendRequests = new ArrayList<>();
        String sql = "SELECT * FROM friend_requests where id_receiver = " + idUser;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()){

            while (resultSet.next()){
                Long id1 = resultSet.getLong("id_sender");
                Status status = Status.valueOf(resultSet.getString("status"));
                LocalDate date = resultSet.getDate("date").toLocalDate();

                FriendRequest friendRequest = new FriendRequest(id1, idUser, status, date);
                friendRequest.setId(new Pair<>(id1, idUser));
                friendRequests.add(friendRequest);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return friendRequests;
    }

    public List<FriendRequest> findAllSentForUser(long idUser){
        List<FriendRequest> friendRequests = new ArrayList<>();
        String sql = "SELECT * FROM friend_requests where id_sender = " + idUser;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql);
             ResultSet resultSet = statement.executeQuery()){

            while (resultSet.next()){
                Long id1 = resultSet.getLong("id_sender");
                Status status = Status.valueOf(resultSet.getString("status"));
                LocalDate date = resultSet.getDate("date").toLocalDate();

                FriendRequest friendRequest = new FriendRequest(id1, idUser, status, date);
                friendRequest.setId(new Pair<>(id1, idUser));
                friendRequests.add(friendRequest);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return friendRequests;
    }

    @Override
    public Page<FriendRequest> findAll(Pageable pageable) {
        Paginator<FriendRequest> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }

    public Page<FriendRequest> findAllReceived(Pageable pageable, Long idUser){
        Paginator<FriendRequest> paginator = new Paginator<>(pageable, this.findAllReceivedForUser(idUser));
        return paginator.paginate();
    }

    public Page<FriendRequest> findAllSent(Pageable pageable, Long idUser){
        Paginator<FriendRequest> paginator = new Paginator<>(pageable, this.findAllSentForUser(idUser));
        return paginator.paginate();
    }
}
