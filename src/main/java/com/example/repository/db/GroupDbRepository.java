package com.example.repository.db;

import com.example.domain.Credentials;
import com.example.domain.Group;
import com.example.domain.Message;
import com.example.domain.User;
import com.example.domain.validators.Validator;
import com.example.exceptions.ValidationException;
import com.example.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GroupDbRepository {
    private String url;
    private String username;
    private String password;
    private Validator<Group> validator;

    public GroupDbRepository(String url, String username, String password, Validator<Group> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public Group findOne(Long id) {
        String sql = "SELECT * from groups where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.isBeforeFirst() ) {
                return null;
            }
            resultSet.next();
            String name = resultSet.getString("name");
            String picture = resultSet.getString("picture");
            Long message = resultSet.getLong("last_message");
            Group group = new Group(getMembers(id));
            if(name != null)
                group.setName(name);
            if(picture != null)
                group.setPicture(picture);
            if(message != null)
                group.setLastMessage(message);
            group.setId(id);
            return group;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Long> getMembers(Long idGroup) {
        String sql = "SELECT * from members where id_group = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, idGroup);
            ResultSet resultSet = ps.executeQuery();
            List<Long> receivers = new ArrayList<>();
            while (resultSet.next()) {
                long id_user = resultSet.getLong("id_user");
                receivers.add(id_user);
            }
            return receivers;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Iterable<Group> findAll() {
        Set<Group> groups = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from groups");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String picture = resultSet.getString("picture");
                Long message = resultSet.getLong("last_message");
                List<Long> members = getMembers(id);

                Group group = new Group(members);
                if(name != null)
                    group.setName(name);
                if(picture != null)
                    group.setPicture(picture);
                if(message != null)
                    group.setLastMessage(message);
                group.setId(id);
                groups.add(group);
            }
            return groups;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Group save(Group entity) throws ValidationException, IllegalArgumentException {
        validator.validate(entity);
        String sql = "insert into groups (name, picture, last_message) values (?, ?, ?) returning id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getName());
            ps.setString(2, entity.getPicture());
            if(entity.getLastMessage()!=null)
                ps.setLong(3, entity.getLastMessage());
            else
                ps.setNull(3, java.sql.Types.INTEGER);

            ResultSet resultSet = ps.executeQuery();
            resultSet.next();
            Long id = resultSet.getLong("id");

            saveMembers(id, entity.getMembers());

            return findOne(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Group delete(Long id) {
        Group entity = findOne(id);
        if(entity == null)
            return null;
        String sql = "delete from groups where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    public Group update(Group entity) throws ValidationException {
        validator.validate(entity);
        String sql = "update groups set name = ?, picture = ?, last_message = ? where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getName());
            ps.setString(2, entity.getPicture());
            ps.setLong(3, entity.getLastMessage());
            ps.setLong(4, entity.getId());
            int lines = ps.executeUpdate();
            deleteMembers(entity.getId());
            saveMembers(entity.getId(), entity.getMembers());
            if(lines == 1)
                return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    private void deleteMembers(long id){
        String sql = "delete from members where id_group = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void saveMembers(long id, List<Long> members){
        String sql = "insert into members (id_group, id_user) values (?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            members.forEach(member ->
                    {
                        try {
                            ps.setLong(1, id);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        try {
                            ps.setLong(2, member);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        try {
                            ps.executeUpdate();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
