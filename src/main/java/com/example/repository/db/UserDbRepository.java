package com.example.repository.db;


import com.example.domain.Friend;
import com.example.domain.User;
import com.example.domain.validators.Validator;
import com.example.exceptions.RepositoryException;
import com.example.exceptions.ValidationException;
import com.example.repository.paging.Page;
import com.example.repository.paging.Pageable;
import com.example.repository.paging.Paginator;
import com.example.repository.paging.PagingRepository;
import com.example.utils.Sex;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class UserDbRepository implements PagingRepository<Long, User> {
    private String url;
    private String username;
    private String password;
    private Validator<User> validator;
    String emailError = "duplicate key value violates unique constraint \"users_email_uindex\"";

    public UserDbRepository(String url, String username, String password, Validator<User> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    /**
     * FindOne
     * @param id -the id of the entity to be returned
     *           id must not be null
     * @return the searched USer
     */
    @Override
    public User findOne(Long id) {
        String sql = "SELECT * from users where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.isBeforeFirst() ) {
                return null;
            }
            resultSet.next();
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            Sex sex = Sex.valueOf(resultSet.getString("sex").toUpperCase());
            String email = resultSet.getString("email");
            LocalDate birthdate = resultSet.getDate("birthdate").toLocalDate();
            String profilePicture = resultSet.getString("profile_image");
            User user = new User(firstName, lastName, sex, email, birthdate);
            if(profilePicture!=null)
                user.setProfilePicture(profilePicture);
            user.setId(id);
            List<Friend>friends = getFriendsList(user);
            user.setFriendsList(friends);
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * FindAll
     * @return the entire list of Users
     */
    @Override
    public Iterable<User> findAll() {
        Set<User> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from users");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                Sex sex = Sex.valueOf(resultSet.getString("sex").toUpperCase());
                String email = resultSet.getString("email");
                LocalDate birthdate = resultSet.getDate("birthdate").toLocalDate();
                String profilePicture = resultSet.getString("profile_image");
                User user = new User(firstName, lastName, sex, email, birthdate);
                if(profilePicture!=null)
                    user.setProfilePicture(profilePicture);
                user.setId(id);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * getFriendsList is used in order to get the friend list of a certain user
     * @param user - which friend list is searched
     * @return - the searched friend list
     */
    private List<Friend> getFriendsList(User user){
        List<Friend> friends = new ArrayList<>();
        String sql = "SELECT u.id, u.first_name, u.last_name, u.sex, u.email, u.birthdate, u.profile_image, f.date from users as u inner join friendships as f on u.id = f.user2_id where f.user1_id = ? union SELECT u.id, u.first_name, u.last_name, u.sex, u.email, u.birthdate, u.profile_image, f.date from users as u inner join friendships as f on u.id = f.user1_id where f.user2_id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)){
             statement.setLong(1, user.getId());
            statement.setLong(2, user.getId());
             ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String sex = resultSet.getString("sex");
                String email = resultSet.getString("email");
                LocalDate birthdate = resultSet.getDate("birthdate").toLocalDate();
                String image = resultSet.getString("profile_image");

                LocalDate date = resultSet.getDate("date").toLocalDate();

                User friendUser = new User(first_name, last_name, Sex.valueOf(sex.toUpperCase()), email, birthdate);
                if(image!=null)
                    friendUser.setProfilePicture(image);
                friendUser.setId(id);
                Friend friend = new Friend(friendUser, date);
                friends.add(friend);
            }
            return friends;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Save
     * @param entity entity must be not null
     * @return null if the entity is successfully saved or the entity if not
     * @throws ValidationException
     * @throws IllegalArgumentException
     */
    @Override
    public User save(User entity) throws ValidationException, RepositoryException {
        validator.validate(entity);
        String sql = "insert into users (first_name, last_name, sex, email, birthdate) values (?, ?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.setString(3, entity.getSex().toString());
            ps.setString(4, entity.getEmail());
            ps.setDate(5, Date.valueOf(entity.getBirthdate()));
            int lines = ps.executeUpdate();
            if(lines == 1)
                return null;
        } catch (SQLException e) {
            if(e.getMessage().contains(emailError))
                throw new RepositoryException("Email already in use");
            else
                e.printStackTrace();
        }
        return entity;
    }

    /**
     * Delete
     * @param id id must be not null
     * @return the entity if it is successfully deleted or null if not
     */
    @Override
    public User delete(Long id) {
        User entity = findOne(id);
        if(entity == null)
            return null;
        String sql = "delete from users where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    /**
     * Update
     * @param entity entity must not be null
     * @return null if it is successfully updated or the entity if not.
     * @throws ValidationException
     */
    @Override
    public User update(User entity) throws ValidationException, RepositoryException {
        validator.validate(entity);
        String sql = "update users set first_name = ?, last_name= ?, sex = ?, email = ?, birthdate = ?, profile_image = ? where id = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.setString(3, entity.getSex().toString());
            ps.setString(4, entity.getEmail());
            ps.setDate(5, Date.valueOf(entity.getBirthdate()));
            if(entity.getProfilePicture()!=null)
                ps.setString(6, entity.getProfilePicture());
            else
                ps.setNull(6, Types.VARCHAR);
            ps.setInt(7, entity.getId().intValue());
            int lines = ps.executeUpdate();
            if(lines == 1)
                return null;
        } catch (SQLException e) {
            if(e.getMessage().contains(emailError))
                throw new RepositoryException("Email already in use");
            else
                e.printStackTrace();
        }
        return entity;

    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        Paginator<User> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }
}