package com.example.repository.db;

import com.example.domain.Credentials;
import com.example.domain.validators.Validator;
import com.example.exceptions.RepositoryException;
import com.example.exceptions.ValidationException;
import com.example.repository.Repository;

import java.sql.*;

public class CredentialsDbRepository implements Repository<String, Credentials> {
    private String url;
    private String username;
    private String password;
    private Validator<Credentials> validator;

    public CredentialsDbRepository(String url, String username, String password, Validator<Credentials> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Credentials findOne(String user) {
        String sql = "SELECT * from credentials where username = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, user);
            ResultSet resultSet = ps.executeQuery();
            if (!resultSet.isBeforeFirst() ) {
                return null;
            }
            resultSet.next();
            String userName = resultSet.getString("username");
            String passWord = resultSet.getString("password");
            Long userId = resultSet.getLong("user_id");
            Credentials credentials = new Credentials(userName, passWord, userId);
            credentials.setId(userName);
            return credentials;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Credentials> findAll() {
        return null;
    }

    @Override
    public Credentials save(Credentials entity) throws ValidationException, IllegalArgumentException {
        validator.validate(entity);
        String sql = "insert into credentials (username, password, user_id) values (?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getUsername());
            ps.setString(2, entity.getPassword());
            ps.setLong(3, entity.getUserId());
            int lines = ps.executeUpdate();
            if(lines == 1)
                return null;
        } catch (SQLException e) {
                e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Credentials delete(String s) {
        return null;
    }

    @Override
    public Credentials update(Credentials entity) throws ValidationException {
        validator.validate(entity);
        String sql = "update credentials set password = ? where username = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getPassword());
            ps.setString(2, entity.getUsername());
            int lines = ps.executeUpdate();
            if(lines == 1)
                return null;
        } catch (SQLException e) {
                e.printStackTrace();
        }
        return entity;
    }
}
