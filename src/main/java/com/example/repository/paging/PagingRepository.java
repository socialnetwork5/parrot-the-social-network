package com.example.repository.paging;

import com.example.domain.Entity;
import com.example.repository.Repository;

public interface PagingRepository<ID, E extends Entity<ID>> extends Repository<ID, E>{
    Page<E> findAll(Pageable pageable);
}
