package com.example.repository.paging;

public interface Pageable {
    int getPageNumber();
    int getPageSize();
}
