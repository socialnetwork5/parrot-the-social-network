package com.example.dto;

import java.time.LocalDate;

public class FriendDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate date;

    /**
     * Class constructor
     * @param id - the id of the user
     * @param firstName - first name of the user
     * @param lastName - last name of the user
     * @param date - date since the two users are friends
     */
    public FriendDTO(Long id, String firstName, String lastName, LocalDate date) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "You are friend with " + firstName + " " + lastName + " since " + date;
    }
}
