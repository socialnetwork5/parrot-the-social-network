package com.example.dto;

import com.example.domain.User;
import com.example.utils.Dates;
import com.example.utils.Status;

import java.time.LocalDate;

public class RequestDTO {
    private User user;
    private Status status;
    private LocalDate date;

    public RequestDTO(User user, Status status, LocalDate date) {
        this.user = user;
        this.status = status;
        this.date = date;
    }

    public String getProfilePicture(){return user.getProfilePicture();}

    public Long getIdUser() {
        return user.getId();
    }

    public String getFirstName() {
        return user.getFirstName();
    }

    public String getLastName() {
        return user.getLastName();
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getStatusString(){
        if(status.equals(Status.approved))
            return "accepted";
        if(status.equals(Status.rejected))
            return "declined";
        else
            return "pending";
    }

    @Override
    public String toString() {
        return user.getFirstName() + ' ' +user.getLastName() + ' ' +
                "sent you a friend request on " + date.format(Dates.DATE_FORMATTER) + " you " + getStatusString() + " it";
    }
}
