package com.example.dto;


import com.example.domain.User;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendshipDTO {
    User user1, user2;
    LocalDate date;

    /**
     * FriendshipDTO Class is used for presenting the data in the user interface
     * Friendship class has just two id s and a date
     * When a user wants to see the friendship list, DTO are created in order to
     * present the users informations that creates the friendship
     * @param user1
     * @param user2
     * @param date
     */
    public FriendshipDTO(User user1, User user2, LocalDate date) {
        this.user1 = user1;
        this.user2 = user2;
        this.date = date;
    }

    /**
     * getter
     * @return user 1
     */
    public User getUser1() {
        return user1;
    }

    /**
     * setter
     * @param user1 to be set
     */
    public void setUser1(User user1) {
        this.user1 = user1;
    }

    /**
     * getter
     * @return user 2
     */
    public User getUser2() {
        return user2;
    }

    /**
     * setter
     * @param user2 to be set
     */
    public void setUser2(User user2) {
        this.user2 = user2;
    }

    /**
     * getter
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * setter
     * @param date to be set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return
                "First user: " + user1 +
                ", Second user: " + user2 +
                ", date: " + date;
    }
}
