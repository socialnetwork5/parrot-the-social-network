package com.example.dto;


import com.example.utils.Dates;

import java.time.LocalDateTime;

public class MessageDTO {
    private Long idMessage;
    private String sender;
    private LocalDateTime dateTime;
    private String text;
    private String repliedTo;

    /**
     * Class constructor
     * @param idMessage - message id
     * @param sender - the id of the sender
     * @param dateTime - the time when the message was sent
     * @param text - the message text
     * @param repliedTo -
     */
    public MessageDTO(Long idMessage, String sender, LocalDateTime dateTime, String text, String repliedTo) {
        this.idMessage = idMessage;
        this.sender = sender;
        this.dateTime = dateTime;
        this.text = text;
        this.repliedTo = repliedTo;
    }

    @Override
    public String toString() {
        String ret =
                "Id: " + idMessage +
                "\nTime: " + dateTime.format(Dates.DATE_TIME_FORMATTER) +
                "\nFrom: " + sender +
                "\nMessage: " + text;

        if (repliedTo != null)
            ret += "\nReplied to: " + repliedTo + "\n-------------------------";
        else
            ret +=  "\n-------------------------";
        return ret;
    }
}
