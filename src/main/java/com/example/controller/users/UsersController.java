package com.example.controller.users;

import com.example.controller.users.ListViewCellUsers;
import com.example.controller.mainComtrollers.UserMainController;
import com.example.domain.User;
import com.example.service.Page;
import com.example.service.ServiceUser;
import com.example.service.SuperServiceUser;
import com.example.utils.events.UserChangedEvent;
import com.example.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class UsersController implements Observer<UserChangedEvent> {

    @FXML
    private ListView listUsers;

    @FXML
    private TextField txtName;
    @FXML
    private Button btnLoadMore;
    private ObservableList<User> model = FXCollections.observableArrayList();
    private SuperServiceUser superServiceUser;

    private UserMainController userMainController;
    private Page page;

    @FXML
    public void initialize(){
        listUsers.setItems(model);

        listUsers.setCellFactory(new Callback<ListView, ListCell>() {
            @Override
            public ListCell call(ListView param) {
                return new ListViewCellUsers(superServiceUser);
            }
        });
        txtName.textProperty().addListener(observable -> populate());

        listUsers.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                User user = (User)listUsers.getSelectionModel().getSelectedItem();
                userMainController.showProfile(user);
            }
        });

        EventHandler<MouseEvent> handlerLoadMore = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadNextPage();
            }
        };

        btnLoadMore.setOnMouseClicked(handlerLoadMore);

    }

    public void populate(){
        String txt = txtName.getText();
        List<User> users = null;
        if (txt.equals(""))
            loadFirstPageUsers();
        else {
            users = superServiceUser.searchByName(txt);
            model.setAll(users);
        }
    }

    public void loadFirstPageUsers(){
        ServiceUser serviceUser = superServiceUser.getServiceUser();
        serviceUser.setPageSize(10);
        List<User> users = serviceUser.getUsersOnPage(0);
        model.setAll(users);
    }

    public void loadNextPage(){
        List<User> users = superServiceUser.getServiceUser().getNextUsers();
        model.addAll(users);
    }

    public void setService(Page page){
        this.page = page;
        this.superServiceUser = page.getSuperServiceUser();
        superServiceUser.addObserver(this);
        superServiceUser.getServiceUser().addObserver(this);
        loadFirstPageUsers();
    }

    @Override
    public void update(UserChangedEvent userChangedEvent) {
        List<User> users = new ArrayList<>(model.stream().collect(Collectors.toList()));
        listUsers.getItems().clear();
        model.setAll(users);
    }

    public void injectController(UserMainController userMainController) {
        this.userMainController = userMainController;
    }
}
