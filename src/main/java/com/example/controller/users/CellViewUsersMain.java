package com.example.controller.users;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;

import java.io.File;
import java.io.IOException;

/**
 * FXML Controller class
 *
 * @author blj0011
 */
public class CellViewUsersMain extends HBox
{
    @FXML
    private HBox hboxRoot;
    @FXML
    private Label lblName, buttonText;
    @FXML
    private Button btnAdd, btnRemove;
    @FXML
    private ImageView imagePreview;
    @FXML
    private ImageView imageButton;

    public CellViewUsersMain()  {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/users/cellViewUsersMain.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    public HBox getHBoxRoot()
    {
        return hboxRoot;
    }

    public void setlblNameText(String text)
    {
        lblName.setText(text);
    }

    public void setButtonFriends() {
        buttonText.setText("Delete friend");
        imageButton.setImage(new Image("file:src/main/resources/images/deleteFriend.png"));
    }
    public void setButtonRequestExist() {
        buttonText.setText("Withdraw");
        imageButton.setImage(new Image("file:src/main/resources/images/deleteRequest.png"));
    }
    public void setButtonReverseRequestExist() {
        buttonText.setText("Accept");
        imageButton.setImage(new Image("file:src/main/resources/images/check2.png"));
    }
    public void setButtonNotRelated() {
        buttonText.setText("Add friend");
        imageButton.setImage(new Image("file:src/main/resources/images/addFriend.png"));
    }

    public void setButtonImage(String path) {
        imageButton.setImage(new Image(path));
    }

    public void setImagePreview(String path) {
        imagePreview.setImage(new Image(new File(path).toURI().toString()));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }
    public void setImageDefault(){
        imagePreview.setImage(new Image("file:src/main/resources/profile/default.jpg"));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }

    public void setBtnAddAction(EventHandler actionEvent)
    {
        btnAdd.setOnAction(actionEvent);
    }

    public void setBtnRemoveAction(EventHandler actionEvent)
    {
        btnRemove.setOnAction(actionEvent);
    }

    public void hideBtnAdd() {
        btnAdd.setVisible(false);
    }

    public void hideButtons(){
        btnAdd.setVisible(false);
        btnRemove.setVisible(false);
    }

    public void swapButtonsColors(){
        btnRemove.setStyle(btnAdd.getStyle());
    }
}
