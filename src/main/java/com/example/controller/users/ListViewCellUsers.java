package com.example.controller.users;

import com.example.controller.friendRequests.CellViewFriendRequest;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.service.SuperServiceUser;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
;import java.util.Objects;

public class ListViewCellUsers extends ListCell<User> {

    private SuperServiceUser serviceUser;

    public ListViewCellUsers(SuperServiceUser superServiceUser){
        this.serviceUser = superServiceUser;
    }

    @Override
    protected void updateItem(User user, boolean empty) {
        super.updateItem(user, empty);
        if(empty)
        {
            setText(null);
            setGraphic(null);
        }

        String textBtn = "";

        if(user != null){

            if(serviceUser.areFriends(user.getId())) {
                setCellFriends(user);
                return;
            }
            if(serviceUser.requestExists(user.getId())) {
                setCellRequestExists(user);
                return;
            }
            if(serviceUser.reverseRequestExists(user.getId())) {
                setCellReverseRequestExists(user);
                return;
            }
            if(Objects.equals(serviceUser.getIdUser(), user.getId())) {
                setCellSameUser(user);
                return;
            }

            setCellNotRelated(user);

        }
    }

    private void setLblNameText(CellViewUsersMain customCellView, User user){
        customCellView.setlblNameText(user.getFirstName() + " " + user.getLastName());
    }

    private void setImagePreview(CellViewUsersMain customCellView, User user){
        if(user.getProfilePicture()!=null)
            customCellView.setImagePreview(user.getProfilePicture());
        else
            customCellView.setImageDefault();
    }

    private void setCellFriends(User user){
        CellViewUsersMain customCellView = new CellViewUsersMain();
        setLblNameText(customCellView, user);
        setImagePreview(customCellView, user);

        EventHandler btnHandler = (EventHandler) (Event event) ->{
            try {
                serviceUser.deleteFriendship(user.getId());
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        };

        customCellView.hideBtnAdd();
        customCellView.setBtnRemoveAction(btnHandler);
        customCellView.setButtonFriends();
        setGraphic(customCellView.getHBoxRoot());
    }

    private void setCellRequestExists(User user){
        CellViewUsersMain customCellView = new CellViewUsersMain();
        setLblNameText(customCellView, user);
        setImagePreview(customCellView, user);

        EventHandler btnHandler = (EventHandler) (Event event) ->{
            try {
                serviceUser.deleteSentFriendRequest(user.getId());
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        };
        customCellView.hideBtnAdd();
        customCellView.setBtnRemoveAction(btnHandler);
        customCellView.setButtonRequestExist();
        setGraphic(customCellView.getHBoxRoot());
    }

    private void setCellReverseRequestExists(User user){
        CellViewUsersMain customCellView = new CellViewUsersMain();
        setLblNameText(customCellView, user);
        setImagePreview(customCellView, user);

        EventHandler addHandler = (EventHandler) (Event event) -> {
            try {
                serviceUser.acceptFriendRequest(user.getId());
                //super.updateItem(user, true);
            } catch (ValidationException e) {
                e.printStackTrace();
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        };

        EventHandler removeHandler = (EventHandler) (Event event) -> {
            try {
                serviceUser.declineFriendRequest(user.getId());
                //super.updateItem(user, true);
            } catch (ValidationException e) {
                e.printStackTrace();
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        };
        customCellView.swapButtonsColors();
        customCellView.hideBtnAdd();
        customCellView.setBtnRemoveAction(addHandler);
        customCellView.setButtonReverseRequestExist();
        setGraphic(customCellView.getHBoxRoot());
    }

    private void setCellNotRelated(User user){
        CellViewUsersMain customCellView = new CellViewUsersMain();
        setLblNameText(customCellView, user);
        setImagePreview(customCellView, user);

        EventHandler btnHandler = (EventHandler) (Event event) -> {
            try {
                serviceUser.sendFriendRequest(user.getId());
            } catch (ValidationException e) {
                e.printStackTrace();
            } catch (ServiceException e) {
            }
        };

        customCellView.swapButtonsColors();
        customCellView.hideBtnAdd();
        customCellView.setBtnRemoveAction(btnHandler);
        customCellView.setButtonNotRelated();
        setGraphic(customCellView.getHBoxRoot());
    }

    private void setCellSameUser(User user){
        CellViewUsersMain customCellView = new CellViewUsersMain();
        setLblNameText(customCellView, user);
        setImagePreview(customCellView, user);
        customCellView.hideButtons();
        setGraphic(customCellView.getHBoxRoot());
    }
    
}
