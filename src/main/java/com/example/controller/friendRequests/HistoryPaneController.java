package com.example.controller.friendRequests;

import com.example.dto.RequestDTO;
import com.example.service.Page;
import com.example.service.SuperServiceUser;
import com.example.utils.events.RequestChangeEvent;
import com.example.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class HistoryPaneController implements Observer<RequestChangeEvent> {
    @FXML
    private ListView listViewHistory;
    @FXML
    private Button btnBack;
    @FXML
    private AnchorPane principalAnchorPane;
    private SuperServiceUser superServiceUser;
    private ObservableList<RequestDTO> modelHistory = FXCollections.observableArrayList();
    private Page page;

    @FXML
    public void initialize() {
        listViewHistory.setItems(modelHistory);
    }

    public void initModelHistory(){
        Iterable<RequestDTO> requests = superServiceUser.getAllRejectedAndApprovedForReceiverDTO();
        List<RequestDTO> requestsList = StreamSupport.stream(requests.spliterator(), false)
                .collect(Collectors.toList());
        modelHistory.setAll(requestsList);
    }

    public void setService(Page page){
        this.page = page;
        this.superServiceUser = page.getSuperServiceUser();
        this.superServiceUser.getServiceFriendRequest().addObserver(this);
        initModelHistory();
        setBtnBackHandler();
    }

    private void setBtnBackHandler(){
        EventHandler backHandler = (EventHandler) (Event event) ->{
            principalAnchorPane.getParent().toBack();
            principalAnchorPane.getParent().setVisible(false);
        };
        btnBack.setOnAction(backHandler);
    }

    @Override
    public void update(RequestChangeEvent requestChangeEvent) {
        initModelHistory();
    }
}
