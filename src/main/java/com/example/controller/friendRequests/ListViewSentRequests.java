package com.example.controller.friendRequests;

import javafx.event.Event;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.SuperServiceUser;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;

public class ListViewSentRequests extends ListCell<User> {
    private SuperServiceUser superServiceUser;

    public ListViewSentRequests(SuperServiceUser superServiceUser){
        this.superServiceUser = superServiceUser;
    }

    @Override
    protected void updateItem(User user, boolean empty) {
        super.updateItem(user, empty);
        if(empty){
            setText(null);
            setGraphic(null);
            return;
        }
        if(user != null){
            CellViewFriendRequest customCellView = new CellViewFriendRequest();
            setLblNameText(customCellView, user);
            setImagePreview(customCellView, user);
            customCellView.hideBtnAdd();
            customCellView.setButtonsText("", "cancel");
            EventHandler handler = (EventHandler) (Event event) -> {
                    try{
                        superServiceUser.deleteSentFriendRequest(user.getId());
                    } catch (ServiceException e) {
                        e.printStackTrace();
                    }
            };
            customCellView.setBtnRemoveAction(handler);
            setGraphic(customCellView.getHBoxRoot());

        }


    }

    private void setImagePreview(CellViewFriendRequest customCellView, User user) {
        customCellView.setlblNameText(user.getFirstName() + " " + user.getLastName());
    }

    private void setLblNameText(CellViewFriendRequest customCellView, User user) {
        if(user.getProfilePicture()!=null)
            customCellView.setImagePreview(user.getProfilePicture());
        else
            customCellView.setImageDefault();

    }
}
