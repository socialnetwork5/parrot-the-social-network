package com.example.controller.friendRequests;

import com.example.controller.friendRequests.CellViewFriendRequest;
import com.example.dto.RequestDTO;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.service.SuperServiceUser;
import com.example.utils.Dates;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;

import java.io.File;

public class ListViewCellFriendRequest extends ListCell<RequestDTO>
{
    private SuperServiceUser superServiceUser;

    public ListViewCellFriendRequest(SuperServiceUser superServiceUser) {
        this.superServiceUser = superServiceUser;
    }

    @Override
    public void updateItem(RequestDTO request, boolean empty)
    {

        super.updateItem(request, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        }
        if (request != null) {
            CellViewFriendRequest customCellView = new CellViewFriendRequest();
            customCellView.setlblNameText(request.getFirstName() + " " + request.getLastName() + " sent you a friend request on " + request.getDate().format(Dates.DATE_FORMATTER));
            setImagePreview(customCellView, request);

            EventHandler addHandler = (EventHandler) (Event event) -> {
                try {
                    superServiceUser.acceptFriendRequest(request.getIdUser());
                    super.updateItem(request, true);
                } catch (ValidationException e) {
                    e.printStackTrace();
                } catch (ServiceException e) {
                    e.printStackTrace();
                }
            };
            customCellView.setBtnAddAction(addHandler);

            EventHandler removeHandler = (EventHandler) (Event event) -> {
                try {
                    superServiceUser.declineFriendRequest(request.getIdUser());
                    super.updateItem(request, true);
                } catch (ValidationException e) {
                    e.printStackTrace();
                } catch (ServiceException e) {
                    e.printStackTrace();
                }
            };
            customCellView.setBtnRemoveAction(removeHandler);

            setGraphic(customCellView.getHBoxRoot());
        }
    }

    private void setImagePreview(CellViewFriendRequest customCellView, RequestDTO requestDTO){
        if(requestDTO.getProfilePicture()!=null)
            customCellView.setImagePreview(requestDTO.getProfilePicture());
        else
            customCellView.setImageDefault();
    }
}