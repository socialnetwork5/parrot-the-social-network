package com.example.controller.friendRequests;

import com.example.domain.User;
import com.example.service.Page;
import com.example.service.SuperServiceUser;
import com.example.utils.events.RequestChangeEvent;
import com.example.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class SentRequestsPaneController implements Observer<RequestChangeEvent> {
    @FXML
    private ListView listViewSent;
    @FXML
    private Button btnBack;
    @FXML
    private AnchorPane principalAnchorPane;
    private SuperServiceUser superServiceUser;
    private ObservableList<User> modelSent = FXCollections.observableArrayList();
    private Page page;

    @FXML
    public void initialize(){
        listViewSent.setItems(modelSent);
        listViewSent.setCellFactory(new Callback<ListView, ListCell>() {
            @Override
            public ListCell call(ListView param) {
                ListViewSentRequests cell = new ListViewSentRequests(superServiceUser);
                return cell;
            }
        });
    }

    public void setService(Page page){
        this.page = page;
        this.superServiceUser = page.getSuperServiceUser();
        this.superServiceUser.getServiceFriendRequest().addObserver(this);
        initModel();
        setBtnBackHaandler();
    }

    private void initModel() {
        Iterable<User> sentRequest = superServiceUser.getAllPendingForSender();
        List<User> userList = StreamSupport.stream(sentRequest.spliterator(), false)
                .collect(Collectors.toList());
        modelSent.setAll(userList);
    }

    private void setBtnBackHaandler(){
        EventHandler<MouseEvent> backHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                principalAnchorPane.getParent().toBack();
                principalAnchorPane.getParent().setVisible(false);
            };

        };
        btnBack.setOnMouseClicked(backHandler);
    }

    @Override
    public void update(RequestChangeEvent requestChangeEvent) {
        listViewSent.getItems().clear();
        initModel();
    }
}

