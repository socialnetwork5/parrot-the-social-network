package com.example.controller.friendRequests;

import com.example.dto.RequestDTO;
import com.example.service.Page;
import com.example.service.SuperServiceUser;
import com.example.utils.events.RequestChangeEvent;
import com.example.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class RequestsPaneController implements Observer<RequestChangeEvent> {

    @FXML
    public ListView listViewFriendRequests;
    @FXML
    private Button btnBack, btnSentRequests;
    @FXML
    private AnchorPane principalAnchorPane;
    @FXML
    private BorderPane sentBorderPane;
    @FXML
    private SentRequestsPaneController sentRequestsPaneController;
    private SuperServiceUser superServiceUser;
    private ObservableList<RequestDTO> model = FXCollections.observableArrayList();
    private Page page;

    @FXML
    public void initialize() {
        listViewFriendRequests.setItems(model);
        listViewFriendRequests.setCellFactory(new Callback<ListView<String>, ListCell<String>>()
        {
            @Override
            public ListCell call(ListView param)
            {
                ListViewCellFriendRequest cell = new ListViewCellFriendRequest(superServiceUser);
                return cell;
            }

        });

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/friendRequests/sentPane.fxml"));
            sentBorderPane.setCenter(fxmlLoader.load());
            sentRequestsPaneController = fxmlLoader.getController();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void initModel(){
        Iterable<RequestDTO> requests = superServiceUser.getAllPendingForReceiverDTO();
        List<RequestDTO> requestsList = StreamSupport.stream(requests.spliterator(), false)
                .collect(Collectors.toList());
        model.setAll(requestsList);
    }

    public void setService(Page page){
        this.page = page;
        this.superServiceUser = page.getSuperServiceUser();
        superServiceUser.getServiceFriendRequest().addObserver(this);
        sentRequestsPaneController.setService(page);
        initModel();
        setBtnBackHandler();
        setBtnSentRequestsHandler();
        sentBorderPane.toBack();
        sentBorderPane.setVisible(false);
    }

    private void setBtnSentRequestsHandler() {
        EventHandler<MouseEvent> sentHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sentBorderPane.toFront();
                sentBorderPane.setVisible(true);
            }
        };
        btnSentRequests.setOnMouseClicked(sentHandler);
    }

    private void setBtnBackHandler(){
        EventHandler backHandler = (EventHandler) (Event event) ->{
            principalAnchorPane.getParent().toBack();
            principalAnchorPane.getParent().setVisible(false);
        };
        btnBack.setOnAction(backHandler);
    }

    @Override
    public void update(RequestChangeEvent requestChangeEvent) {
        listViewFriendRequests.getItems().clear();
        initModel();
    }

    public void PanesToBack(){
        sentBorderPane.toBack();
        sentBorderPane.setVisible(false);
    }

}
