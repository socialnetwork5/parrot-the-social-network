package com.example.controller.raports;

import com.example.domain.Group;
import com.example.domain.User;
import com.example.dto.FriendDTO;
import com.example.dto.MessageDTO;
import com.example.exceptions.ServiceException;
import com.example.service.Page;
import com.example.service.SuperServiceUser;
import com.example.utils.PDFWriter;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DatePickersStageController {

    private Page page;
    private SuperServiceUser superServiceUser;
    private Group group;
    private int raportNumber;
    @FXML
    private DatePicker firstDatePicker, secondDatePicker;
    @FXML
    private Button btnExport;
    @FXML
    private AnchorPane backgroundAnchorPane;
    @FXML
    private Label lblError;

    @FXML
    public void initialize(){
        lblError.setVisible(false);

        EventHandler<MouseEvent> handlerExport = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {

                LocalDate firstDate = firstDatePicker.getValue();
                LocalDate secondDate = secondDatePicker.getValue();

                if(firstDate == null || secondDate == null)
                {
                    lblError.setText("You must choose both dates for export");
                    lblError.setVisible(true);
                    return;
                }
                if(firstDate.isAfter(LocalDate.now())){
                    lblError.setText("First day must be in the present or past");
                    lblError.setVisible(true);
                    return;
                }

                lblError.setVisible(false);
                String file = getFile();
                StringBuffer raportText = getNeededRaport(raportNumber, firstDate, secondDate);

                PDFWriter pdfWriter = new PDFWriter(file);
                pdfWriter.createPdfFile();
                String heading = getHeader(raportNumber) ;
                pdfWriter.addPage(heading, raportText);
                if(raportNumber == 1){
                    raportText = getSecondPartFirstRaport(firstDate, secondDate);
                    pdfWriter.addPage("Friendships raport", raportText);
                }
                pdfWriter.saveAndClose();
                ((Stage)(btnExport.getScene().getWindow())).close();
            }
        };

        btnExport.setOnMouseClicked(handlerExport);
    }

    public String getHeader(int raportNumber){
        if(raportNumber == 2){
            return getMessagesHeader();
        }
        if(raportNumber == 1){
            return getUserHeader();
        }
        return null;

    }

    private String getUserHeader(){
        return "Raport messages";
    }

    private String getMessagesHeader() {
        Long friendId = group.getMembers().stream()
                .filter(member -> !Objects.equals(member, superServiceUser.getIdUser()))
                .collect(Collectors.toList()).get(0);

        User friend = null;
        try {
            friend = superServiceUser.getServiceUser().findOne(friendId);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        String name = friend.getFirstName() + " " + friend.getLastName();
        return "Conversation with " + name;
    }

    public StringBuffer getNeededRaport(int raportNumber, LocalDate firsDate, LocalDate secondDate){
        if(raportNumber == 2)
        {
            return getRaportMessages(firsDate, secondDate);
        }
        if(raportNumber == 1)
        {
            return getRaportUser(firsDate, secondDate);
        }
        return null;
    }

    public StringBuffer getRaportMessages(LocalDate firstDate, LocalDate secondDate){
        List<MessageDTO> raportList = superServiceUser.getRaportMessages(firstDate.atStartOfDay(), secondDate.atStartOfDay(), group.getId());

        StringBuffer raportText = new StringBuffer();

        raportList.forEach(message -> {
            String text =  message.toString();
            raportText.append("\n" + text);
        });

        return raportText;
    }

    public StringBuffer getRaportUser(LocalDate firstDate, LocalDate secondDate) {
        List<MessageDTO> raportList = superServiceUser.getRaportAllMessages(firstDate.atStartOfDay(), secondDate.atStartOfDay());

        StringBuffer raportText = new StringBuffer();

        raportList.forEach(message -> {
            String text =  message.toString();
            raportText.append("\n" + text);
        });

        return raportText;
    }

    public StringBuffer getSecondPartFirstRaport(LocalDate firstDate, LocalDate secondDate){
        StringBuffer raportText = new StringBuffer();
        List<FriendDTO> raportList2 = null;

        try {
            raportList2 = superServiceUser.getRaportAllNewFriends(firstDate.atStartOfDay(), secondDate.atStartOfDay());
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        raportList2.forEach(message -> {
            String text =  message.toString();
            raportText.append("\n" + text);
        });

        return raportText;
    }

    public String getFile(){
        Stage stage = (Stage) btnExport.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(stage);
        return file.getAbsolutePath();
    }

    public void setServices(Page page, Group group, int raportNumber){
        this.page = page;
        this.superServiceUser = page.getSuperServiceUser();
        this.group = group;
        this.raportNumber = raportNumber;

    }
}
