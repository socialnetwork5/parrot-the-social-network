package com.example.controller.friendships;

import com.example.controller.friendRequests.HistoryPaneController;
import com.example.controller.friendRequests.RequestsPaneController;
import com.example.domain.Friend;
import com.example.domain.Friendship;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.Page;
import com.example.service.ServiceFriendship;
import com.example.service.SuperServiceUser;
import com.example.utils.events.FriendshipChangedEvent;
import com.example.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class FriendsController implements Observer<FriendshipChangedEvent> {

    @FXML
    public ListView listFriends;
    @FXML
    private Button btnFriendRequests;
    @FXML
    public Button btnSeeHistory, btnLoadMore;
    @FXML
    private BorderPane friendRequestPane;
    @FXML
    public BorderPane historyPane;
    @FXML
    private RequestsPaneController requestsPaneController;
    @FXML
    private HistoryPaneController historyPaneController;

    private Page page;
    private SuperServiceUser superServiceUser;
    private ObservableList<User> model = FXCollections.observableArrayList();

    @FXML
    public void initialize(){
        listFriends.setItems(model);
        listFriends.setCellFactory(new Callback<ListView, ListCell>() {
            @Override
            public ListCell call(ListView param) {
                return  new ListViewCellFriendships(superServiceUser);
            }
        });

        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/friendRequests/requestsPane.fxml"));
            friendRequestPane.setCenter(fxmlLoader.load());
            requestsPaneController = fxmlLoader.getController();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/friendRequests/historyPane.fxml"));
            historyPane.setCenter(fxmlLoader.load());
            historyPaneController = fxmlLoader.getController();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        EventHandler<MouseEvent> handler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadNextPage();
            }
        };

        btnLoadMore.setOnMouseClicked(handler);

    }

    public void loadFirstPage(){
        ServiceFriendship serviceFriendship = superServiceUser.getServiceFriendship();
        serviceFriendship.setPageSizeUser(6);
        List<Friendship> friendships = serviceFriendship.getFriendshipsOnPageForUser(0, superServiceUser.getIdUser());
        List<User> friends = convertFriendshipToUser(friendships);
        model.setAll(friends);
    }

    public void loadNextPage(){
        List<Friendship> friendships = superServiceUser.getServiceFriendship().getNextFriendshipsForUser(superServiceUser.getIdUser());
        List<User> friends = convertFriendshipToUser(friendships);
        model.addAll(friends);
    }

    public List<User> convertFriendshipToUser(List<Friendship> friendships){
        List<User> friends = friendships.stream().map(friendship ->{
                    if(friendship.getFirstUser().getId().equals(superServiceUser.getIdUser()))
                        return friendship.getSecondUser();
                    else
                        return friendship.getFirstUser();
                }
        ).collect(Collectors.toList());
        return friends;
    }

    public void setService(Page page)
    {
        this.page = page;
        this.superServiceUser = page.getSuperServiceUser();
        requestsPaneController.setService(page);
        historyPaneController.setService(page);
        superServiceUser.getServiceFriendship().addObserver(this);
        loadFirstPage();
        setBtnFriendRequestAction();
        setBtnHistoryAction();
        friendRequestPane.toBack();
        friendRequestPane.setVisible(false);
        historyPane.toBack();
        historyPane.setVisible(false);
    }

    private void setBtnFriendRequestAction(){
        EventHandler friendRequestHandler = (EventHandler) (javafx.event.Event event)->{
            friendRequestPane.toFront();
            friendRequestPane.setVisible(true);
        };
        btnFriendRequests.setOnAction(friendRequestHandler);
    }

    private void setBtnHistoryAction(){
        EventHandler historyHandler = (EventHandler) (javafx.event.Event event)->{
            historyPane.toFront();
            historyPane.setVisible(true);
        };
        btnSeeHistory.setOnAction(historyHandler);
    }

    @Override
    public void update(FriendshipChangedEvent friendshipChangedEvent) {
        listFriends.getItems().clear();
        loadFirstPage();
    }

    public void panesToBack(){
        requestsPaneController.PanesToBack();
        friendRequestPane.toBack();
        friendRequestPane.setVisible(false);
        historyPane.toBack();
        historyPane.setVisible(false);
    }
}

