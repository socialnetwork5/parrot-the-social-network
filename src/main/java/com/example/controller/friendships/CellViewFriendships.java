package com.example.controller.friendships;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;

import java.io.File;
import java.io.IOException;

public class CellViewFriendships extends HBox {
    @FXML
    HBox hBoxRoot;
    @FXML
    Label lblName;
    @FXML
    Button btnRemove;
    @FXML
    ImageView imagePreview;

    public CellViewFriendships(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/friendships/cellViewFriends.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public HBox gethBoxRoot() {
        return hBoxRoot;
    }

    public void setLblName(String text) {
        lblName.setText(text);
    }

    public void setImageDefault(){
        imagePreview.setImage(new Image("file:src/main/resources/profile/default.jpg"));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }

    public void setImagePreview(String path) {
        imagePreview.setImage(new Image(new File(path).toURI().toString()));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }

    public void setBtnRemoveAction(EventHandler actionEvent) {
        btnRemove.setOnAction((actionEvent));
    }
}
