package com.example.controller.friendships;

import com.example.controller.friendships.CellViewFriendships;
import com.example.domain.Friend;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.SuperServiceUser;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;


public class ListViewCellFriendships extends ListCell<User> {
    private SuperServiceUser serviceUser;

    public ListViewCellFriendships(SuperServiceUser serviceUser){
        this.serviceUser = serviceUser;
    }

    @Override
    protected void updateItem(User item, boolean empty) {
        super.updateItem(item, empty);

        if(empty){
            setText(null);
            setGraphic(null);
        }
        if(item != null){

            CellViewFriendships customCellView = new CellViewFriendships();

            customCellView.setLblName(item.getFirstName() + " " + item.getLastName());
            setImagePreview(customCellView, item);
            EventHandler removeHandler = (EventHandler) (Event event) -> {
                try{
                    serviceUser.deleteFriendship(item.getId());
                    super.updateItem(item, true);
                } catch (ServiceException e){
                    e.printStackTrace();
                }
            };
            customCellView.setBtnRemoveAction(removeHandler);
            setGraphic(customCellView.gethBoxRoot());

        }
    }
    private void setImagePreview(CellViewFriendships customCellView, User friend){
        if(friend.getProfilePicture()!=null)
            customCellView.setImagePreview(friend.getProfilePicture());
        else
            customCellView.setImageDefault();
    }
}
