package com.example.controller.chat;

import com.example.controller.chat.CellViewGroups;
import com.example.domain.Group;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.SuperServiceUser;
import javafx.scene.control.ListCell;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class ListViewCellGroups extends ListCell<Group>
{
    private SuperServiceUser superServiceUser;

    public ListViewCellGroups(SuperServiceUser superServiceUser) {
        this.superServiceUser = superServiceUser;
    }

    @Override
    public void updateItem(Group group, boolean empty)
    {

        super.updateItem(group, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        }
        if (group != null) {
            setStyle("-fx-control-inner-background: #1a4645" + ";");
            CellViewGroups customCellView = new CellViewGroups();
            try {
                setlblTextAndImage(customCellView, group);
            } catch (ServiceException | URISyntaxException e) {
                e.printStackTrace();
            }
            setGraphic(customCellView.getHBoxRoot());
        }
    }

    private void setlblTextAndImage(CellViewGroups customCellView, Group group) throws ServiceException, URISyntaxException {
        if(group.getLastMessage()!=0){
            String lastMessage = superServiceUser.getServiceMessage().findOne(group.getLastMessage()).getText();
            customCellView.setlblLastMessage(lastMessage);
        }
        else
            customCellView.setlblLastMessage("no messages yet");
        if(!group.isAConversation()){
            customCellView.setlblNameText(group.getName());
            if(group.getPicture()!=null)
                customCellView.setImagePreview(group.getPicture(), true);
            else
                customCellView.setImageDefault(true);
        }
        else{
            List<Long> members = new ArrayList<>(group.getMembers());
            members.remove(superServiceUser.getIdUser());
            User user = superServiceUser.findOne(members.get(0));
            customCellView.setlblNameText(user.getFirstName() + " " + user.getLastName());
            if(user.getProfilePicture()!=null)
                customCellView.setImagePreview(user.getProfilePicture(), false);
            else
                customCellView.setImageDefault(false);
        }
    }
}