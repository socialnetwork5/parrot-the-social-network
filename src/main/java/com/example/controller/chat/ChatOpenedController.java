package com.example.controller.chat;

import com.example.controller.chat.cellViewsMessages.*;
import com.example.controller.raports.DatePickersStageController;
import com.example.domain.Group;
import com.example.domain.Message;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.service.Page;
import com.example.service.SuperServiceUser;
import com.example.socialnetworkdb2.LogInInterface;
import com.example.utils.TimerLoad;
import com.example.utils.events.ChangeEventType;
import com.example.utils.events.MessageChangeEvent;
import com.example.utils.observer.Observer;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

public class ChatOpenedController implements Observer<MessageChangeEvent> {
    private Page page;
    private SuperServiceUser superServiceUser;
    private static Group group = null;
    @FXML
    private ImageView imageGroup;
    @FXML
    private Label lblName, labelAnswer;
    @FXML
    private ScrollPane scrollPaneChat;
    @FXML
    private VBox scrollPaneVBox;
    @FXML
    private TextArea textAreaMessage;
    @FXML
    private Button btnSend, btnCloseReply, btnExportPdf;
    @FXML
    private AnchorPane anchorPaneAnswer, anchorPaneChat;
    private VBox groupsVBox;
    private Timer timer = null;
    private Tooltip tooltipMembers = null;

    final private KeyCombination SHIFT_ENTER = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.SHIFT_DOWN);

    private CellViewMessage selectedCell;
    private final InvalidationListener scrollListener = this::onScrollChange;

    public static void setGroup(Group group) {
        ChatOpenedController.group = group;
    }

    private void onScrollChange(Observable observable) {
        scrollPaneChat.setVvalue(1.);
    }

    @FXML
    public void initialize(){

        btnSend.setDisable(true);
        textAreaMessage.textProperty().addListener(listener -> {
            btnSend.setDisable(textAreaMessage.getText().isBlank());
        });

        EventHandler<MouseEvent> handlerSend = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sendMessage();
            }
        };

        btnSend.setOnMouseClicked(handlerSend);

        EventHandler<KeyEvent> handlerEnter = new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER && !event.isShiftDown()) {
                   sendMessage();
                }
                else if(SHIFT_ENTER.match(event)){
                    textAreaMessage.deleteText(textAreaMessage.getSelection());
                    textAreaMessage.insertText(textAreaMessage.getCaretPosition(), "\n");
                    event.consume();
                }

            }
        };

        EventHandler<MouseEvent> handlerCloseReply = event -> {
            labelAnswer.setUserData(null);
            textAreaMessage.requestFocus();
            anchorPaneAnswer.setVisible(false);
            anchorPaneAnswer.setManaged(false);
            selectedCell.setStyle("-fx-background-color: #d7e0eb");
            selectedCell = null;
        };

        btnCloseReply.setOnMouseClicked(handlerCloseReply);
        textAreaMessage.setOnKeyPressed(handlerEnter);

        anchorPaneAnswer.setVisible(false);
        anchorPaneAnswer.setManaged(false);
        scrollPaneChat.vvalueProperty().addListener((observable, oldV, newV) ->{
            if(newV.equals(0.)) {
                if(timer != null){
                    timer.cancel();
                    timer.purge();
                    timer = null;
                }
                List<Message> messages = superServiceUser.getPrevMessages(group.getId());
                if (group.isAConversation()) {
                    Collections.reverse(messages);
                    messages.forEach(message -> {
                        CellViewMessage cellViewMessage;
                        if (Objects.equals(message.getSender(), superServiceUser.getIdUser()))
                            cellViewMessage = setSenderConversation(message);
                        else
                            cellViewMessage = setReceiverConversation(message);

                        cellViewMessage.addObserver(this);
                        scrollPaneVBox.getChildren().add(0, cellViewMessage);
                    });
                } else {
                    Collections.reverse(messages);
                    messages.forEach(message -> {
                        CellViewMessage cellViewMessage;
                        if (Objects.equals(message.getSender(), superServiceUser.getIdUser()))
                            cellViewMessage = setSenderConversation(message);
                        else
                            cellViewMessage = setReceiverGroup(message);
                        cellViewMessage.addObserver(this);
                        scrollPaneVBox.getChildren().add(0, cellViewMessage);
                    });
                }
            }
            if(newV.equals(1.)){
                timer = TimerLoad.setInterval(this::refresh, 1500);
            }
            else if(timer != null){
                timer.cancel();
                timer.purge();
                timer = null;

            }
        });

        EventHandler<MouseEvent> handlerExport = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                FXMLLoader fxmlLoader = new FXMLLoader(LogInInterface.class.getResource("/com/example/socialnetworkdb2/raports/datePickersStage.fxml"));
                AnchorPane root = null;
                try {
                    root = fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                DatePickersStageController datePickersStageController = fxmlLoader.getController();
                datePickersStageController.setServices(page, group, 2);
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Parrot");
                stage.setScene(scene);
                stage.show();
            }
        };
        btnExportPdf.setOnMouseClicked(handlerExport);

    }

    private void refresh(){
        if(group == null && timer != null){
            timer.cancel();
            timer.purge();
            return;
        }
        try {
            Group aux = superServiceUser.getServiceGroup().findOne(group.getId());
            Message last = superServiceUser.getServiceMessage().findOne(aux.getLastMessage());
            if(last != null && Duration.between(last.getTime(), LocalDateTime.now()).toMillis()<3000) {
                populateConversation();
            }
        } catch (ServiceException ignored) {}
    }

    private void sendMessage(){
        if(!textAreaMessage.getText().isBlank()) {
            String text = textAreaMessage.getText();
            text = text.trim();
            try {
                if (labelAnswer.getUserData() != null) {
                    Message replyAt = (Message) labelAnswer.getUserData();
                    superServiceUser.replyMessage(replyAt.getId(), text);
                    labelAnswer.setUserData(null);
                    anchorPaneAnswer.setVisible(false);
                    anchorPaneAnswer.setManaged(false);
                } else
                    superServiceUser.sendMessage(group.getId(), text);
            } catch (ValidationException | ServiceException e) {
                e.printStackTrace();
            }
        }
        textAreaMessage.setText("");

        if(selectedCell != null) {
            selectedCell.setStyle("-fx-background-color: #d7e0eb");
            selectedCell = null;
        }
    }


    public void setService(Page page, Group group, VBox groupsVBox) {
        this.page = page;
        superServiceUser = page.getSuperServiceUser();
        ChatOpenedController.group = group;
        superServiceUser.getServiceMessage().addObserver(this);
        loadPage();
        populateConversation();
        this.groupsVBox = groupsVBox;
        //Dimensiunea de care am nevoie la initializat cell urile
        //System.out.println(anchorPaneChat.getParent().getParent().getParent().getParent().getParent().getParent().getLayoutBounds().getWidth() - groupsVBox.getPrefWidth());
    }

    public void reset(Group group){
        ChatOpenedController.group = group;
        anchorPaneAnswer.setVisible(false);
        anchorPaneAnswer.setManaged(false);
        labelAnswer.setUserData(null);
        selectedCell = null;
        textAreaMessage.setText("");
        loadPage();
        populateConversation();
    }

    public void loadPage(){
        if(!group.isAConversation()) {
            lblName.setText(group.getName());
            loadProfilePicture(group, true);
            btnExportPdf.setVisible(false);
            return;
        }

        loadTextAndPictureConversation();

        if(Objects.equals(group.getMembers().get(0), superServiceUser.getIdUser())) {
            if (!superServiceUser.areFriends(group.getMembers().get(1))) {
                btnExportPdf.setVisible(false);
                return;
            }
        }
        else
            if(!superServiceUser.areFriends(group.getMembers().get(0))){
                btnExportPdf.setVisible(false);
                return;
            }
        btnExportPdf.setVisible(true);
    }

    private void loadTextAndPictureConversation() {
        List<Long> members = new ArrayList<>(group.getMembers());
        members.remove(superServiceUser.getIdUser());
        User user = null;
        try {
            user = superServiceUser.findOne(members.get(0));
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        lblName.setText(user.getFirstName() + " " + user.getLastName());
        if(user.getProfilePicture() != null)
            setImageConversation(user.getProfilePicture());
        else
            imageGroup.setImage(new Image("file:src/main/resources/profile/default.jpg"));

    }

    private void setImageConversation(String profilePicture) {
        imageGroup.setImage(new Image(new File(profilePicture).toURI().toString()));
        Circle clip = new Circle(imageGroup.getFitWidth()/2, imageGroup.getFitHeight()/2, imageGroup.getFitHeight()/2);
        imageGroup.setClip(clip);
    }


    private void loadProfilePicture(Group group, boolean isGroup) {
        if(group.getPicture()!=null)
            imageGroup.setImage(new Image(new File(group.getPicture()).toURI().toString()));
        else
            imageGroup.setImage(new Image("file:src/main/resources/images/empty.jpeg"));
        Circle clip = new Circle(imageGroup.getFitWidth()/2, imageGroup.getFitHeight()/2, imageGroup.getFitHeight()/2);
        imageGroup.setClip(clip);
    }

    private void populateConversation(){

        scrollPaneVBox.getChildren().clear();
        superServiceUser.setPageSizeGroupMessages(20);
        List<Message> messages;

        int lastPage = superServiceUser.getGroupChat(group.getId()).size() / 20;
        messages = superServiceUser.getMessagesOnPage(lastPage, group.getId());

        if(messages.size() < 20)
            messages.addAll(0, superServiceUser.getPrevMessages(group.getId()));

        if(group.isAConversation())
            messages.forEach(this::addCellConversation);
        else
            messages.forEach(this::addCellGroup);

        scrollPaneVBox.heightProperty().addListener(scrollListener);

        if(!group.isAConversation()){
            List<Long> members = group.getMembers();
            String[] membersNames = {""};
            members.forEach(member -> {
                User user = null;
                try {
                    user = superServiceUser.getServiceUser().findOne(member);
                } catch (ServiceException e) {
                    e.printStackTrace();
                }
                membersNames[0] += user.getFirstName() + " " + user.getLastName() + "\n";
            });

            tooltipMembers = new Tooltip(membersNames[0]);
            Tooltip.install(lblName, tooltipMembers);

        }
        else if (tooltipMembers != null){
            Tooltip.uninstall(lblName, tooltipMembers);
        }
    }

    private void addCellGroup(Message message){
        CellViewMessage cellViewMessage;
        if (Objects.equals(message.getSender(), superServiceUser.getIdUser()))
            cellViewMessage = setSenderConversation(message);
        else
            cellViewMessage = setReceiverGroup(message);

        cellViewMessage.addObserver(this);
        try {
            scrollPaneVBox.getChildren().add(cellViewMessage);
        } catch (Exception ignored) {}
    }

    private CellViewMessage setReceiverGroup(Message message){
        CellViewMessage cellViewMessage;
        if (message.getReplyAt() == null)
            cellViewMessage = new CellViewMessageReceivedGroup(message, superServiceUser);
        else
            cellViewMessage =  new CellViewMessageReceivedReplyAtGroup(message, superServiceUser);
        return cellViewMessage;
    }

    private CellViewMessage setSenderConversation(Message message){
        CellViewMessage cellViewMessage;
        if (message.getReplyAt() == null)
            cellViewMessage = new CellViewMessageSent(message);
        else
            cellViewMessage = new CellViewMessageReplySent(message, superServiceUser);
        return cellViewMessage;

    }

    private CellViewMessage setReceiverConversation(Message message) {
        CellViewMessage cellViewMessage;
        if (message.getReplyAt() == null)
            cellViewMessage = new CellViewMessageReceived(message);
        else
            cellViewMessage = new CellViewMessageReplyReceived(message, superServiceUser);

        return cellViewMessage;
    }

    private void addCellConversation(Message message){
        CellViewMessage cellViewMessage;
        if (Objects.equals(message.getSender(), superServiceUser.getIdUser())) {
            cellViewMessage = setSenderConversation(message);
        } else
            cellViewMessage = setReceiverConversation(message);
        cellViewMessage.addObserver(this);
        try {
            scrollPaneVBox.getChildren().add(cellViewMessage);
        } catch (Exception ignored) {}

    }

    @Override
    public void update(MessageChangeEvent messageChangeEvent) {
        if(messageChangeEvent.getType() == ChangeEventType.ADD)
        {
            Message message = messageChangeEvent.getData();
            if(group.isAConversation())
                addCellConversation(message);
            else
                addCellGroup(message);
        }
        if(messageChangeEvent.getType() == ChangeEventType.REPLY)
        {
            if(selectedCell != null)
                selectedCell.setStyle("-fx-background-color: #d7e0eb");
            Message message = messageChangeEvent.getData();
            User user;
            String name = null;
            try {
                user = superServiceUser.findOne(message.getSender());
                name = user.getFirstName() + " " + user.getLastName();
            } catch (ServiceException e) {
                e.printStackTrace();
            }
            labelAnswer.setText("You are answering to " + name);
            textAreaMessage.requestFocus();
            labelAnswer.setUserData(message);
            selectedCell = messageChangeEvent.getUserData();
            anchorPaneAnswer.setVisible(true);
            anchorPaneAnswer.setManaged(true);
        }
    }

    public static Group getGroup(){return group;}

}
