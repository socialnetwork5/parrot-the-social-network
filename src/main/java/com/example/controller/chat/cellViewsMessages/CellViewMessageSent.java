package com.example.controller.chat.cellViewsMessages;

import com.example.domain.Message;
import javafx.fxml.FXMLLoader;

import java.io.IOException;
import java.lang.reflect.Member;

public class CellViewMessageSent extends CellViewMessage {
    public CellViewMessageSent(Message message){
        super(message);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/chat/cellViewSent.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setBtnReplyHandlers();
        setData();
        setMessageWidth();
    }
}
