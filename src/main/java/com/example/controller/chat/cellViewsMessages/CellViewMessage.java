package com.example.controller.chat.cellViewsMessages;

import com.example.domain.Message;
import com.example.utils.Dates;
import com.example.utils.Pair;
import com.example.utils.events.ChangeEventType;
import com.example.utils.events.MessageChangeEvent;
import com.example.utils.observer.Observer;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class CellViewMessage extends HBox {

    @FXML
    private Button btnReply;
    @FXML
    private HBox hBoxRoot;
    @FXML
    private Text text;
    @FXML
    private TextFlow textFlow;
    @FXML
    private HBox hBoxForToolTip;
    @FXML
    private AnchorPane paneText;

    private List<Observer<MessageChangeEvent>> observers = new ArrayList<>();
    private Message message;

    public CellViewMessage(Message message){
        this.message = message;
    }

    protected void setBtnReplyHandlers(){
        EventHandler<MouseEvent> enterReplyHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                btnReply.setOpacity(1);
            }
        };

        EventHandler<MouseEvent> exitReplyHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                btnReply.setOpacity(0);
            }
        };

        CellViewMessage cellViewMessage = this;
        EventHandler<MouseEvent> clickedReplyHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setStyle("-fx-background-color: #668ab2");
                notifyObservers(new MessageChangeEvent(ChangeEventType.REPLY, message, cellViewMessage));
            }
        };

        btnReply.setOnMouseClicked(clickedReplyHandler);
        btnReply.setOnMouseEntered(enterReplyHandler);
        btnReply.setOnMouseExited(exitReplyHandler);
        btnReply.setOpacity(0);
    }

    protected void setMessageWidth(){
        double width = getMessageWidth();
        paneText.setPrefWidth(width);
    }

    protected double getMessageWidth(){
        double textWidth = text.getLayoutBounds().getWidth();
        return min(paneText.getPrefWidth(), textWidth + 80);
    }

    protected double getPrefWidthPane(){
        return paneText.getPrefWidth();
    }

    protected void setData(){
        text.setText(message.getText());
        setTooltip(message.getTime().format(Dates.DATE_TIME_FORMATTER));
    }

    protected Message getMessage(){
        return message;
    }

    private void setTooltip(String dateTime){
        Tooltip.install(hBoxForToolTip, new Tooltip(dateTime));
    }

    public void addObserver(Observer<MessageChangeEvent> e) {
        observers.add(e);
    }

    public void removeObserver(Observer<MessageChangeEvent> e) {
        observers.remove(e);
    }

    public void notifyObservers(MessageChangeEvent t) {
        observers.forEach(observer -> observer.update(t));
    }

    public String getText() {
        return text.getText();
    }
}
