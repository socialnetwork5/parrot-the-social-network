package com.example.controller.chat.cellViewsMessages;

import com.example.domain.Message;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.SuperServiceUser;
import com.example.utils.Pair;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class CellViewMessageReply extends CellViewMessage{
    @FXML
    private Label lblName;
    @FXML
    private TextFlow textFlowReply;
    @FXML
    private Text textReply;
    private SuperServiceUser superServiceUser;

    public CellViewMessageReply(Message message, SuperServiceUser superServiceUser){
        super(message);
        this.superServiceUser = superServiceUser;
    }

    @Override
    protected void setData() {
        super.setData();
        Message message = super.getMessage();
        Message replyAt = superServiceUser.getMessage(message.getReplyAt());
        User sender;
        String name = null;
        try {
            sender = superServiceUser.findOne(replyAt.getSender());
            name = sender.getFirstName() + " " + sender.getLastName();
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        textReply.setText(replyAt.getText());
        lblName.setText(name);
    }

    protected String getNameSender(){
        User sender = null;
        try {
            sender = superServiceUser.findOne(getMessage().getSender());
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        return sender.getFirstName() + " " + sender.getLastName();

    }

    @Override
    protected double getMessageWidth() {
        double actual = super.getPrefWidthPane();
        double textWidth = super.getMessageWidth();
        double replyWidth = textReply.getLayoutBounds().getWidth() + 80;
        if(textWidth >= actual || replyWidth >= actual)
            return actual;

        return Math.max(replyWidth, textWidth);

    }
}
