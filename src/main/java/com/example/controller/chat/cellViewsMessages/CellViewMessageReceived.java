package com.example.controller.chat.cellViewsMessages;

import com.example.domain.Message;
import com.example.utils.Pair;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;

import java.io.IOException;

public class CellViewMessageReceived extends CellViewMessage {

    @FXML
    private Label lblNameSender;

    public CellViewMessageReceived(Message message){
        super(message);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/chat/cellViewReceived.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setBtnReplyHandlers();
        setVisibleNameSender(false);
        setData();
        setMessageWidth();
    }

    protected void setVisibleNameSender(boolean value){
        lblNameSender.setVisible(value);
        lblNameSender.setManaged(value);
    }

    protected void setNameSender(String nameSender){
        lblNameSender.setText(nameSender);
    }
}
