package com.example.controller.chat.cellViewsMessages;

import com.example.domain.Message;
import com.example.service.SuperServiceUser;
import com.example.utils.Pair;

public class CellViewMessageReceivedReplyAtGroup extends CellViewMessageReplyReceived{
    public CellViewMessageReceivedReplyAtGroup(Message message, SuperServiceUser superServiceUser) {
        super(message, superServiceUser);
        super.setVisibleNameSender(true);
    }

}
