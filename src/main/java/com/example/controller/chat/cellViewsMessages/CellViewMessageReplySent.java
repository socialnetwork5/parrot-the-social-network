package com.example.controller.chat.cellViewsMessages;

import com.example.domain.Message;
import com.example.service.SuperServiceUser;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class CellViewMessageReplySent extends CellViewMessageReply{
    public CellViewMessageReplySent(Message message, SuperServiceUser superServiceUser){
        super(message, superServiceUser);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/chat/cellViewSentReplyAt.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setBtnReplyHandlers();
        setData();
        setMessageWidth();
    }
}
