package com.example.controller.chat.cellViewsMessages;

import com.example.domain.Message;
import com.example.service.SuperServiceUser;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;

import java.io.IOException;

public class CellViewMessageReplyReceived extends CellViewMessageReply{

    @FXML
    private Label lblNameSender;

    public CellViewMessageReplyReceived(Message message, SuperServiceUser superServiceUser){
        super(message, superServiceUser);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/chat/cellViewReceivedReplyAt.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setBtnReplyHandlers();
        setVisibleNameSender(false);
        setData();
        setMessageWidth();
    }

    protected void setVisibleNameSender(boolean value){
        lblNameSender.setVisible(value);
        lblNameSender.setManaged(value);
    }

    @Override
    protected void setData() {
        super.setData();
        lblNameSender.setText(super.getNameSender());
    }
}
