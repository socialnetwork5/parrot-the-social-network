package com.example.controller.chat.cellViewsMessages;

import com.example.domain.Message;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.SuperServiceUser;
import com.example.utils.Pair;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class CellViewMessageReceivedGroup extends CellViewMessageReceived{
    private SuperServiceUser superServiceUser;
    public CellViewMessageReceivedGroup(Message message, SuperServiceUser superServiceUser){
        super(message);
        this.superServiceUser = superServiceUser;
        setVisibleNameSender(true);
        User user;
        String name = null;
        try {
            user = superServiceUser.findOne(message.getSender());
            name = user.getFirstName() + " " + user.getLastName();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        setNameSender(name);
        setMessageWidth();
    }

}
