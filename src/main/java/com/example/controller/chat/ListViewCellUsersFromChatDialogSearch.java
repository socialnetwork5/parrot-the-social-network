package com.example.controller.chat;

import com.example.controller.chat.CellViewGroups;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.SuperServiceUser;
import javafx.scene.control.ListCell;

public class ListViewCellUsersFromChatDialogSearch extends ListCell<User>
{
    private SuperServiceUser superServiceUser;

    public ListViewCellUsersFromChatDialogSearch(SuperServiceUser superServiceUser) {
        this.superServiceUser = superServiceUser;
    }

    @Override
    public void updateItem(User user, boolean empty)
    {

        super.updateItem(user, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        }
        if (user != null) {
            CellViewSearch customCellView = new CellViewSearch();
            try {
                setlblTextAndImage(customCellView, user);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
            setGraphic(customCellView.getHBoxRoot());
        }
    }

    private void setlblTextAndImage(CellViewSearch customCellView, User user) throws ServiceException {
        customCellView.setlblNameText(user.getFirstName() + " " + user.getLastName());
        if(user.getProfilePicture()!=null)
            customCellView.setImagePreview(user.getProfilePicture(), false);
        else
            customCellView.setImageDefault(false);
    }
}