package com.example.controller.chat;

import com.example.controller.chat.CellViewMembers;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.SuperServiceUser;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;

import java.util.List;

public class ListViewCellMembers extends ListCell<User> {
    private List<Long> members;
    private SuperServiceUser superServiceUser;

    public ListViewCellMembers(SuperServiceUser superServiceUser)
    {
        this.superServiceUser = superServiceUser;
    }

    public void setMemmbers(List<Long> members){
        this.members = members;
    }

        @Override
        public void updateItem(User user, boolean empty)
        {

            super.updateItem(user, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            }
            if (user != null) {
                setStyle("-fx-control-inner-background: #d7e0eb" + ";");
                CellViewMembers customCellView = new CellViewMembers();
                try {
                    setlblTextAndImage(customCellView, user);
                    setButton(customCellView, user);
                } catch (ServiceException e) {
                    e.printStackTrace();
                }
                setGraphic(customCellView.getHBoxRoot());
            }
        }

    private void setButton(CellViewMembers customCellView, User user) {
        EventHandler addHandler = (EventHandler) (javafx.event.Event event)->{
            members.add(user.getId());
            setButton(customCellView, user);
        };
        EventHandler deleteHandler = (EventHandler) (javafx.event.Event event)->{
            members.remove(user.getId());
            setButton(customCellView, user);
        };
        if(user.getId().equals(superServiceUser.getIdUser())){
            customCellView.setBtnAddVisibility(false);
            customCellView.setBtnDeleteVisibility(false);
        }
        else {
            if (members.contains(user.getId())) {
                customCellView.setBtnAddVisibility(false);
                customCellView.setBtnDeleteVisibility(true);
                customCellView.setBtnDeleteAction(deleteHandler);
            } else {
                customCellView.setBtnAddVisibility(true);
                customCellView.setBtnDeleteVisibility(false);
                customCellView.setBtnAddAction(addHandler);
            }
        }
    }

    private void setlblTextAndImage(CellViewMembers customCellView, User user) throws ServiceException {
        customCellView.setlblName(user.getFirstName() + " " + user.getLastName());
        if(user.getProfilePicture()!=null)
            customCellView.setImagePreview(user.getProfilePicture());
        else
            customCellView.setImageDefault();
    }
}
