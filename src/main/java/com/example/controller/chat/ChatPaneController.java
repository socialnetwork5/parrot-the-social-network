package com.example.controller.chat;

import com.example.domain.Group;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.service.Page;
import com.example.service.SuperServiceUser;
import com.example.utils.events.GroupChangeEvent;
import com.example.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ChatPaneController implements Observer<GroupChangeEvent> {
    @FXML
    private ImageView imagePreview;
    @FXML
    private Button openCreateGroupDialogButton, uploadPictureButton, createGroupButton;
    @FXML
    private ListView groupsListView, searchUserListView, membersListView;
    @FXML
    private Label testLabel, createGroupErrorLabel;
    @FXML
    private TextField userSearchTextField, groupNameField, addMemberSearchField;
    @FXML
    private BorderPane searchUserPane, createChatPane, chatOpenedPane;
    @FXML
    private VBox groupsVBox;

    private ObservableList<Group> modelGroups = FXCollections.observableArrayList();
    private ObservableList<User> modelUsers = FXCollections.observableArrayList();
    private ObservableList<User> modelMembers = FXCollections.observableArrayList();
    private List<Long> selectedMembers = new ArrayList<>();
    private String selectedImagePath;

    private ChatOpenedController chatOpenedController;
    private SuperServiceUser superServiceUser;
    private Page page;
    private Timer timer = null;

    @FXML
    public void initialize() {
        groupsListView.setItems(modelGroups);
        groupsListView.setCellFactory(new Callback<ListView<String>, ListCell<String>>()
        {
            @Override
            public ListCell call(ListView param)
            {
                ListViewCellGroups cell = new ListViewCellGroups(superServiceUser);
                return cell;
            }

        });
        searchUserListView.setItems(modelUsers);
        searchUserListView.setCellFactory(new Callback<ListView<String>, ListCell<String>>()
        {
            @Override
            public ListCell call(ListView param)
            {
                ListViewCellUsersFromChatDialogSearch cell = new ListViewCellUsersFromChatDialogSearch(superServiceUser);
                return cell;
            }

        });
        configureSearchMemberView(selectedMembers);
        searchUserPane.setVisible(false);
        createChatPane.setVisible(false);
        setBtnOpenCreateGroupDialogAction();
        setBtnUploadPictureAction();
        setBtnCreateGroupAction();
        userSearchTextField.textProperty().addListener(observable -> populateSearchUser());
        addMemberSearchField.textProperty().addListener(observable -> populateSearchMember());

        searchUserListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                User user= (User)searchUserListView.getSelectionModel().getSelectedItem();
                try {
                    if(user != null) {
                        Group group = superServiceUser.searchConversation(user.getId());
                        if(group == null)
                        {
                            List<Long> members = new ArrayList<>();
                            members.add(superServiceUser.getIdUser());
                            members.add(user.getId());
                            if(user.getProfilePicture() == null)
                                user.setProfilePicture("file:src/main/resources/profile/default.jpg");
                            superServiceUser.createGroup(user.getFirstName() + " " + user.getLastName(),
                                    members, user.getProfilePicture());
                        }
                        group = superServiceUser.searchConversation(user.getId());
                        openChat(group);
                    }
                } catch (ValidationException e) {
                    e.printStackTrace();
                }
            }
        });

        groupsListView.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Group group = (Group)groupsListView.getSelectionModel().getSelectedItem();
                if(group != null)
                    openChat(group);
            }
        });



    }

    private void configureSearchMemberView(List<Long> members) {
        membersListView.setItems(modelMembers);
        membersListView.setCellFactory(new Callback<ListView<String>, ListCell<String>>()
        {
            @Override
            public ListCell call(ListView param)
            {
                ListViewCellMembers cell = new ListViewCellMembers(superServiceUser);
                cell.setMemmbers(members);
                return cell;
            }

        });
    }

    private void setBtnOpenCreateGroupDialogAction(){
        EventHandler createGroupHandler = (EventHandler) (javafx.event.Event event)->{
            createChatPane.toFront();
            createChatPane.setVisible(true);
            createGroupErrorLabel.setText("");
            selectedMembers = new ArrayList<>();
            configureSearchMemberView(selectedMembers);
            Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 40);
            imagePreview.setClip(clip);
            createGroupErrorLabel.setText("");
        };
        openCreateGroupDialogButton.setOnAction(createGroupHandler);
    }

    private void setBtnUploadPictureAction(){
        EventHandler uploadPicture = (EventHandler) (javafx.event.Event event)->{
            final FileChooser fileChooser = new FileChooser();
            Stage stage = new Stage();
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                selectedImagePath = file.getPath();
                if(selectedImagePath != null)
                    imagePreview.setImage(new Image(new File(selectedImagePath).toURI().toString()));
                Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 40);
                imagePreview.setClip(clip);
            }
        };
        uploadPictureButton.setOnAction(uploadPicture);
    }

    private void resetCreateGroupDialog(){
        groupNameField.setText("");
        addMemberSearchField.setText("");
        selectedImagePath = null;
        selectedMembers = new ArrayList<>();
        imagePreview.setImage(new Image("file:src/main/resources/images/empty.jpeg"));
    }

    private void setBtnCreateGroupAction(){
        EventHandler createGroupHandler = (EventHandler) (javafx.event.Event event)->{
            if(selectedMembers.size()<2) {
                createGroupErrorLabel.setText("At least 3 members are required in order to create a group!");
                return;
            }
            if(Objects.equals(groupNameField.getText(), "")){
                createGroupErrorLabel.setText("The group must have a name");
                return;
            }

            String name = groupNameField.getText();
            try {
                selectedMembers.add(superServiceUser.getIdUser());
                Group group = superServiceUser.createGroup(name, selectedMembers, selectedImagePath);
                resetCreateGroupDialog();
                openChat(group);
            } catch (ValidationException e) {
                e.printStackTrace();
            }

        };
        createGroupButton.setOnAction(createGroupHandler);
    }

    public void openChat(Group group){
        if(ChatOpenedController.getGroup() == null) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/chat/conversationExperiment.fxml"));
                chatOpenedPane.setCenter(loader.load());
                chatOpenedController = loader.getController();

                chatOpenedController.setService(page, group, groupsVBox);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
            chatOpenedController.reset(group);

        chatOpenedPane.setVisible(true);
        chatOpenedPane.toFront();
        searchUserPane.setVisible(false);


    }

    private void populateSearchUser() {
        String txt = userSearchTextField.getText();
        if(Objects.equals(txt, ""))
            searchUserPane.setVisible(false);
        else{
            searchUserPane.setVisible(true);
            searchUserPane.toFront();
        }
        List<User> users;
        if (txt.equals(""))
            users= StreamSupport.stream(superServiceUser.getUsers().spliterator(), false).collect(Collectors.toList());
        else
            users = superServiceUser.searchByName(txt);

        try {
            User user = superServiceUser.getUser();
            users.remove(user);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        modelUsers.setAll(users);
        if(users.size() <= 3)
            searchUserPane.setPrefHeight(users.size() * searchUserListView.getFixedCellSize());
        else
            searchUserPane.setPrefHeight(3 * searchUserListView.getFixedCellSize());
    }

    private void populateSearchMember() {
        String txt = addMemberSearchField.getText();
        List<User> users;
        if (txt.equals(""))
            users = StreamSupport.stream(superServiceUser.getUsers().spliterator(), false).collect(Collectors.toList());
        else
            users = superServiceUser.searchByName(txt);

        try {
            User user = superServiceUser.getUser();
            users.remove(user);
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        modelMembers.setAll(users);
    }

    public Group initModel(){
        List<Group> groups = superServiceUser.findAllUsersGroups();
        groups = groups.stream().filter(g->!g.isAConversation() || (g.isAConversation() && g.getLastMessage()!=0)).collect(Collectors.toList());
        groups.sort((a, b) -> {
            try {
                LocalDateTime lastActivityA = superServiceUser.getServiceMessage().findOne(a.getLastMessage()).getTime();
                try {
                    LocalDateTime lastActivityB = superServiceUser.getServiceMessage().findOne(b.getLastMessage()).getTime();
                    if(lastActivityA.isBefore(lastActivityB))
                        return 1;
                    else
                        return -1;
                } catch (ServiceException e) {
                    return -1;
                }
            } catch (ServiceException e) {
                return 1;
            }
        });
        modelGroups.setAll(groups);
        if(groups.isEmpty())
            return null;
        else
            return groups.get(0);
    }

    public void setService(Page page){
        this.page = page;
        this.superServiceUser = page.getSuperServiceUser();
        this.superServiceUser.getServiceGroup().addObserver(this);
        Group lastConversation = initModel();
        populateSearchMember();
        //timer = TimerLoad.setInterval(this::initModel, 1500);
    }

    @Override
    public void update(GroupChangeEvent groupChangeEvent) {
        initModel();
    }
}
