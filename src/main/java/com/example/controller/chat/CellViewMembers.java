package com.example.controller.chat;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;

import java.io.File;
import java.io.IOException;


public class CellViewMembers extends HBox {
    @FXML
    HBox hBoxRoot;
    @FXML
    Label lblName;
    @FXML
    Button btnAdd;
    @FXML
    Button btnDelete;
    @FXML
    ImageView imagePreview;

    public CellViewMembers(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/chat/cellViewMembers.fxml"));
        loader.setRoot(this);
        loader.setController(this);

        try {
            loader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setBtnAddAction(EventHandler actionEvent)
    {
        btnAdd.setOnAction(actionEvent);
    }

    public void setBtnDeleteAction(EventHandler actionEvent)
    {
        btnDelete.setOnAction(actionEvent);
    }

    public HBox getHBoxRoot(){
        return hBoxRoot;
    }

    public void setImagePreview(String path) {
        imagePreview.setImage(new Image(new File(path).toURI().toString()));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }

    public void setImageDefault(){
        imagePreview.setImage(new Image("file:src/main/resources/profile/default.jpg"));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }

    public void setlblName(String text){
        lblName.setText(text);
    }

    public void setBtnAddVisibility(boolean value){
        if(value)
            btnAdd.setVisible(true);
        else
            btnAdd.setVisible(false);
    }

    public void setBtnDeleteVisibility(boolean value){
        if(value)
            btnDelete.setVisible(true);
        else
            btnDelete.setVisible(false);
    }

}
