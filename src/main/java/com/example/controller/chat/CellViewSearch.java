package com.example.controller.chat;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;

import java.io.File;
import java.io.IOException;

/**
 * FXML Controller class
 *
 * @author blj0011
 */
public class CellViewSearch extends HBox
{
    @FXML
    private HBox hboxRoot;
    @FXML
    private Label lblName;
    @FXML
    private ImageView imagePreview;

    public CellViewSearch()  {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/chat/cellViewSearch.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    public HBox getHBoxRoot()
    {
        return hboxRoot;
    }

    public void setlblNameText(String text)
    {
        lblName.setText(text);
    }

    public void setImagePreview(String path, boolean isGroup) {
        if(path!=null)
            imagePreview.setImage(new Image(new File(path).toURI().toString()));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }

    public void setImageDefault(boolean isGroup){
        if(isGroup)
            imagePreview.setImage(new Image("file:src/main/resources/images/empty.jpeg"));
        else
            imagePreview.setImage(new Image("file:src/main/resources/profile/default.jpg"));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }
}
