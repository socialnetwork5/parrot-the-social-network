package com.example.controller.events;

import com.example.domain.Event;
import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.service.SuperServiceUser;
import com.example.utils.Dates;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ListViewCellEvents extends ListCell<Event>
{
    private SuperServiceUser superServiceUser;

    public ListViewCellEvents(SuperServiceUser superServiceUser) {
        this.superServiceUser = superServiceUser;
    }

    @Override
    public void updateItem(Event event, boolean empty)
    {

        super.updateItem(event, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        }
        if (event != null) {
            CellViewEvents customCellView = new CellViewEvents();

            customCellView.setTitle(event.getName());
            customCellView.setDescription(event.getDescription());
            customCellView.setLocation(event.getLocation());
            customCellView.setDate(event.getTime().toLocalDate().format(Dates.DATE_FORMATTER));
            customCellView.setTime(event.getTime().toLocalTime().toString());
            setPicture(customCellView, event);
            setEnrollButton(customCellView, event);
            setGraphic(customCellView.getBorderPaneRoot());
            setParticipants(customCellView, event);
            setCancelButton(customCellView, event);
            setNotificationsButton(customCellView, event);
        }
    }

    public void setParticipants(CellViewEvents customCellView, Event event){
        Long idUser = superServiceUser.getIdUser();
        List<Long> idPaticipants = superServiceUser.getEventManager().getEnrolledUsers(event.getId());
        idPaticipants.remove(idUser);
        List<User> participants = new ArrayList<>();
        idPaticipants.stream().forEach(i -> {
            try {
                User user = superServiceUser.findOne(i);
                participants.add(user);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
        });
        customCellView.setParticipants(participants);
    }

    public void setPicture(CellViewEvents customCellView, Event event){
        if(event.getPicture() != null)
            customCellView.setPicture(new File(event.getPicture()).toURI().toString());
        else
            customCellView.setPicture("file:src/main/resources/images/empty.jpeg");
    }

    public void setCancelButton(CellViewEvents customCellView, Event event){
        Long idUser = superServiceUser.getIdUser();
        if(idUser.equals(event.getOrganizer())){
            EventHandler cancelHandler = (EventHandler) (javafx.event.Event eventHandler)->{
                try {
                    superServiceUser.getEventManager().deleteEvent(event.getId());
                } catch (ServiceException e) {
                    e.printStackTrace();
                }
            };
            customCellView.setCancelEventButton(true, cancelHandler);
        }
        else
            customCellView.setCancelEventButton(false, null);
    }

    public void setEnrollButton(CellViewEvents customCellView, Event event){
        EventHandler enabledHandler = (EventHandler) (javafx.event.Event eventHandler)->{
            superServiceUser.unEnrollUser(event.getId());
            setEnrollButton(customCellView, event);
        };
        EventHandler disabledHandler = (EventHandler) (javafx.event.Event eventHandler)->{
            superServiceUser.enrollUser(event.getId());
            setEnrollButton(customCellView, event);
        };
        if(superServiceUser.getEventManager().IsUserEnrolled(superServiceUser.getIdUser(), event.getId())) {
            customCellView.setButtonsColors(true);
            customCellView.setEnrollButton(true, enabledHandler);
        }
        else {
            customCellView.setButtonsColors(false);
            customCellView.setEnrollButton(false, disabledHandler);
        }
    }

    public void setNotificationsButton(CellViewEvents customCellView, Event event){

        EventHandler enabledHandler = (EventHandler) (javafx.event.Event eventHandler)->{
            superServiceUser.turnNotificationsOff(event.getId());
            setNotificationsButton(customCellView, event);
        };
        EventHandler disabledHandler = (EventHandler) (javafx.event.Event eventHandler)->{
            superServiceUser.turnNotificationsOn(event.getId());
            setNotificationsButton(customCellView, event);
        };
        if(superServiceUser.getEventManager().IsUserEnrolled(superServiceUser.getIdUser(), event.getId())) {
            if (superServiceUser.getEventManager().areNotificationsOn(superServiceUser.getIdUser(), event.getId())) {
                customCellView.setNotificationsButtonsColors(true);
                customCellView.setNotificationsButtons(true, enabledHandler);
            } else {
                customCellView.setNotificationsButtonsColors(false);
                customCellView.setNotificationsButtons(false, disabledHandler);
            }
        }
        else{customCellView.setNotificationButtonOffNotVisible();}
    }

}