package com.example.controller.events;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.example.domain.User;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * FXML Controller class
 *
 * @author blj0011
 */
public class CellViewEvents extends BorderPane
{
    @FXML
    BorderPane borderPaneRoot;

    @FXML
    Label eventTime, eventDate, eventLocation, eventTitle, areGoingLabel;

    @FXML
    TextFlow eventDescription;

    @FXML
    ImageView eventPicture, image1, image2, image3;

    @FXML
    Button enrollButtonEnabled, enrollButtonDisabled, cancelEventButton, turnOffNotificationsButton, turnOnNotificationsButton;

    public CellViewEvents()  {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/events/cellViewEvents.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    public void setDescription(String description){
        Text text = new Text(description);
        eventDescription.getChildren().add(text);
    }

    public void setTitle(String title){
        eventTitle.setText(title);
    }

    public void setDate(String title){
        eventDate.setText(title);
    }

    public void setTime(String title){
        eventTime.setText(title);
    }

    public void setLocation(String title){
        eventLocation.setText(title);
    }

    public void setNotificationButtonOffNotVisible(){
        turnOffNotificationsButton.setVisible(false);
    }

    public void setParticipants(List<User> participants){
        if(participants.isEmpty()) {
            areGoingLabel.setVisible(false);
            image1.setVisible(false);
            image2.setVisible(false);
            image3.setVisible(false);
        }
        else{
            areGoingLabel.setVisible(true);
            image1.setVisible(true);
            image1.setImage(new Image(new File(participants.get(0).getProfilePicture()).toURI().toString()));
            Circle clip1 = new Circle(image1.getFitWidth()/2, image1.getFitHeight()/2, image1.getFitWidth()/2);
            image1.setClip(clip1);
            image2.setVisible(false);
            image3.setVisible(false);
            if(participants.size()>1) {
                image2.setVisible(true);
                image2.setImage(new Image(new File(participants.get(1).getProfilePicture()).toURI().toString()));
                Circle clip2 = new Circle(image2.getFitWidth()/2, image2.getFitHeight()/2, image1.getFitWidth()/2);
                image2.setClip(clip2);
                image3.setVisible(false);
            }
            if(participants.size()>2) {
                image3.setVisible(true);
                image3.setImage(new Image(new File(participants.get(2).getProfilePicture()).toURI().toString()));
                Circle clip = new Circle(image1.getFitWidth()/2, image1.getFitHeight()/2, image1.getFitWidth()/2);
                image3.setClip(clip);
            }
        }
    }

    public void setPicture(String path){
        eventPicture.setImage(new Image(path));
    }

    public void setEnrollButton(boolean var, EventHandler actionEvent){
        if(var == true) {
            enrollButtonEnabled.setOnAction(actionEvent);
        }
        else {
            enrollButtonDisabled.setOnAction(actionEvent);
            turnOnNotificationsButton.setVisible(false);
            turnOffNotificationsButton.setVisible(false);
        }
    }

    public void setNotificationsButtonsColors(boolean var){
        if(var){
            turnOffNotificationsButton.setVisible(false);
            turnOnNotificationsButton.setVisible(true);
        }
        else{
            turnOffNotificationsButton.setVisible(true);
            turnOnNotificationsButton.setVisible(false);
        }
    }

    public void setNotificationsButtons(boolean var, EventHandler actionEvent){
        if(var == true) {
            turnOnNotificationsButton.setOnAction(actionEvent);
        }
        else {
            turnOffNotificationsButton.setOnAction(actionEvent);
        }
    }

    public void setButtonsColors(boolean var){
        if(var == true) {
            enrollButtonEnabled.setVisible(true);
            enrollButtonDisabled.setVisible(false);
        }
        else {
            enrollButtonEnabled.setVisible(false);
            enrollButtonDisabled.setVisible(true);
            turnOffNotificationsButton.setVisible(false);
            turnOnNotificationsButton.setVisible(false);
        }
    }

    public void setCancelEventButton(boolean var, EventHandler actionEvent){
        if(var){
            cancelEventButton.setVisible(true);
            cancelEventButton.setOnAction(actionEvent);
        }
        else
            cancelEventButton.setVisible(false);
    }

    public BorderPane getBorderPaneRoot()
    {
        return borderPaneRoot;
    }
}
