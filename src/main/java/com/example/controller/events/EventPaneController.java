package com.example.controller.events;

import com.example.domain.Event;
import com.example.exceptions.ValidationException;
import com.example.service.EventManager;
import com.example.service.Page;
import com.example.service.SuperServiceUser;
import com.example.utils.events.EventChangedEvent;
import com.example.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class EventPaneController implements Observer<EventChangedEvent> {
    @FXML
    private ListView listViewEvents;
    @FXML
    private Button createNewEvent,  btnLoadMore;
    @FXML
    private BorderPane fog, createNewEventDialog, mainPane;
    @FXML
    private ImageView eventPicture;
    @FXML
    private DatePicker eventDatePicker;
    @FXML
    private TextField eventLocationField, eventTitleField;
    @FXML
    private TextArea eventDescriptionField;
    @FXML
    private Label errorLabel;
    @FXML
    private Button saveButton, cancelButton;
    @FXML
    private Spinner eventHour, eventMinute;
    private String selectedImagePath;

    private Page page;
    private SuperServiceUser superServiceUser;
    private EventManager eventManager;
    private ObservableList<Event> model = FXCollections.observableArrayList();

    public void initialize(){

        listViewEvents.setItems(model);
        listViewEvents.setCellFactory(new Callback<ListView<String>, ListCell<String>>()
        {
            @Override
            public ListCell call(ListView param)
            {
                ListViewCellEvents cell = new ListViewCellEvents(superServiceUser);
                return cell;
            }
        });

        EventHandler<MouseEvent> handler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                loadNextPage();
            }
        };

        btnLoadMore.setOnMouseClicked(handler);

        fog.setVisible(false);
        createNewEventDialog.setVisible(false);
        eventDatePicker.setEditable(false);
        eventHour.setEditable(false);
        eventMinute.setEditable(false);
        populateCurrentTime();

        setBtnCancelAction();
        setBtnSaveAction();
        setBtnNewEventAction();
        setBtnUploadPictureAction();
    }

    private void setBtnUploadPictureAction(){
        EventHandler uploadPicture = (EventHandler) (javafx.event.Event event)->{
            final FileChooser fileChooser = new FileChooser();
            Stage stage = new Stage();
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                selectedImagePath = file.getPath();
                if(selectedImagePath != null)
                    eventPicture.setImage(new Image(new File(selectedImagePath).toURI().toString()));
            }
        };
        eventPicture.setOnMouseClicked(uploadPicture);
    }

    private void setBtnSaveAction(){
        EventHandler saveAction = (EventHandler) (javafx.event.Event event)->{

            if(Objects.equals(eventTitleField.getText(), "") || Objects.equals(eventDescriptionField.getText(), "")
            || Objects.equals(eventLocationField.getText(), "") || eventDatePicker.getValue() == null)
            {
                errorLabel.setText("All fields are mandatory");
                errorLabel.setVisible(true);
                return;
            }

            String eventTitle = eventTitleField.getText();
            String eventDescription = eventDescriptionField.getText();
            String eventLocation = eventLocationField.getText();
            LocalDate eventDate = eventDatePicker.getValue();
            LocalTime eventTime = LocalTime.of((Integer)eventHour.getValue(), (Integer)eventMinute.getValue());
            LocalDateTime eventDateTime =  LocalDateTime.of(eventDate, eventTime);
            try {
                superServiceUser.createEvent(eventTitle, eventDescription, eventLocation, eventDateTime, selectedImagePath);
                fog.setVisible(false);
                createNewEventDialog.setVisible(false);
                mainPane.toFront();
            } catch (ValidationException e) {
                errorLabel.setText(e.getMessage());
            }
            eventDatePicker.setValue(null);
            eventDescriptionField.setText("");
            eventTitleField.setText("");
            eventLocationField.setText("");
            selectedImagePath = null;
            eventPicture.setImage(new Image("file:src/main/resources/images/empty.jpeg"));
        };
        saveButton.setOnAction(saveAction);


    }

    private void setBtnCancelAction(){
        EventHandler cancelAction = (EventHandler) (javafx.event.Event event)->{
            fog.setVisible(false);
            createNewEventDialog.setVisible(false);
            mainPane.toFront();
        };
        cancelButton.setOnAction(cancelAction);
    }

    private void setBtnNewEventAction(){
        EventHandler newEventAction = (EventHandler) (javafx.event.Event event)->{
            fog.setVisible(true);
            fog.toFront();
            createNewEventDialog.setVisible(true);
            createNewEventDialog.toFront();
            populateCurrentTime();
            errorLabel.setText("");
        };
        createNewEvent.setOnAction(newEventAction);
    }
    
    public void setService(Page page){
        this.page = page;
        this.superServiceUser = page.getSuperServiceUser();
        this.eventManager = superServiceUser.getEventManager();
        eventManager.addObserver(this);
        loadFirstPage();
    }


    private void loadFirstPage(){
        EventManager eventManager = superServiceUser.getEventManager();
        eventManager.setPageSizeEvents(2);
        List<Event> events = eventManager.getEventsOnPage(0);
        model.setAll(events);
    }

    private void loadNextPage(){
        List<Event> events = eventManager.getNextPage();
        model.addAll(events);
    }

    @Override
    public void update(EventChangedEvent eventChangedEvent) {
        if(eventChangedEvent.getType() == null){
            List<Event> events = new ArrayList<>(model.stream().collect(Collectors.toList()));
            listViewEvents.getItems().clear();
            model.setAll(events);
            return;
        }
        loadFirstPage();
        listViewEvents.scrollTo(0);
    }

    public void populateCurrentTime(){
        eventHour.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 23, LocalTime.now().getHour()));
        eventMinute.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 59, LocalTime.now().getMinute()));
        restrictDatePicker(eventDatePicker, LocalDate.now());
    }

    public void restrictDatePicker(DatePicker datePicker, LocalDate minDate) {
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(minDate)) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePicker.setDayCellFactory(dayCellFactory);
    }

}
