package com.example.controller.notification;


import com.example.controller.mainComtrollers.UserMainController;
import com.example.domain.Event;
import com.example.service.Page;
import com.example.service.SuperServiceUser;
import com.example.utils.events.EventChangedEvent;
import com.example.utils.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import java.util.List;

public class NotificationPaneController implements Observer<EventChangedEvent> {
    public ListView notificationListView;
    public BorderPane notificationPane;
    public Label notificationNoLabel;

    private Page page;
    private SuperServiceUser serviceUser;

    private ObservableList<Event> model = FXCollections.observableArrayList();
    private UserMainController userMainController;

    public void initialize(){
        notificationListView.setItems(model);
        notificationListView.setCellFactory(new Callback<ListView<String>, ListCell<String>>()
        {
            @Override
            public ListCell call(ListView param)
            {
                ListViewCellNotifications cell = new ListViewCellNotifications(serviceUser);
                return cell;
            }

        });
    }

    public void setService(Page page) {
        this.page = page;
        this.serviceUser = page.getSuperServiceUser();
        this.serviceUser.getEventManager().addObserver(this);
        initModel();
    }

    private void initModel() {
        List<Event> upcoming = serviceUser.getNotificationsForUpcomingEvents();
        model.setAll(upcoming);
        if(upcoming.size() != 0) {
            if (upcoming.size() <= 3) {
                userMainController.setNotificationPaneHeight(50 + upcoming.size() * notificationListView.getFixedCellSize());
                notificationPane.setMaxHeight(50 + upcoming.size() * notificationListView.getFixedCellSize());
            } else {
                userMainController.setNotificationPaneHeight(50 + 3 * notificationListView.getFixedCellSize());
                notificationPane.setMaxHeight(50 + 3 * notificationListView.getFixedCellSize());
            }
            notificationNoLabel.setText("You have " + upcoming.size() + " notifications");
            userMainController.setRedDot(true);
        }
        else {
            notificationNoLabel.setText("You have no notifications");
            userMainController.setNotificationPaneHeight((double)30);
            notificationPane.setMaxHeight(30);
            userMainController.setRedDot(false);
        }
    }

    public void injectController(UserMainController userMainController) {
        this.userMainController = userMainController;
    }

    @Override
    public void update(EventChangedEvent eventChangedEvent) {
        initModel();
    }
}
