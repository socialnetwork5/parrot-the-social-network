package com.example.controller.notification;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;

import java.io.File;
import java.io.IOException;

public class CellViewNotifications extends HBox{
    @FXML
    Label lblTitle;
    @FXML
    Label lblDetails;
    @FXML
    ImageView imagePreview;
    @FXML
    HBox hBoxRoot;

    public CellViewNotifications()  {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/notifications/cellViewNotifications.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public HBox getHBoxRoot()
    {
        return hBoxRoot;
    }

    public void setLblTitle(String title){
        lblTitle.setText(title);
    }

    public void setLblDetails(String details){
        lblDetails.setText(details);
    }

    public void setImagePreview(String path) {
        imagePreview.setImage(new Image(path));
        Circle clip = new Circle(imagePreview.getFitWidth()/2, imagePreview.getFitHeight()/2, 25);
        imagePreview.setClip(clip);
    }
}
