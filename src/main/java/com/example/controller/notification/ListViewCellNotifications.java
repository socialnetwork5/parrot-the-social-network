package com.example.controller.notification;

import com.example.controller.friendRequests.CellViewFriendRequest;
import com.example.domain.Event;
import com.example.service.SuperServiceUser;

import javafx.scene.control.ListCell;

import java.io.File;

public class ListViewCellNotifications extends ListCell<Event>
{
    private SuperServiceUser superServiceUser;

    public ListViewCellNotifications(SuperServiceUser superServiceUser) {
        this.superServiceUser = superServiceUser;
    }

    @Override
    public void updateItem(Event event, boolean empty)
    {

        super.updateItem(event, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        }
        if (event != null) {
            setStyle("-fx-control-inner-background: #266867" + ";");
            CellViewNotifications customCellView = new CellViewNotifications();
            customCellView.setLblTitle(event.getName());
            customCellView.setLblDetails("Upcoming event on " + event.getTime().toLocalDate());
            setImage(customCellView, event);
            setGraphic(customCellView.getHBoxRoot());
        }
    }

    private void setImage(CellViewNotifications customCellView, Event event) {
        if(event.getPicture() != null)
            customCellView.setImagePreview(new File(event.getPicture()).toURI().toString());
        else
            customCellView.setImagePreview("file:src/main/resources/images/empty.jpeg");
    }
}