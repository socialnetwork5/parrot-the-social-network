package com.example.controller.mainComtrollers;

import com.example.controller.chat.ChatOpenedController;
import com.example.controller.chat.ChatPaneController;
import com.example.controller.events.EventPaneController;
import com.example.controller.friendships.FriendsController;
import com.example.controller.notification.NotificationPaneController;
import com.example.controller.profile.ProfilePaneController;
import com.example.controller.raports.DatePickersStageController;
import com.example.controller.users.UsersController;
import com.example.domain.User;
import com.example.service.Page;
import com.example.service.ServiceLogIn;
import com.example.service.ServiceSignUp;
import com.example.service.SuperServiceUser;
import com.example.socialnetworkdb2.LogInInterface;
import com.example.utils.Dates;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.security.auth.login.CredentialException;
import java.io.File;
import java.io.IOException;

public class UserMainController  {

    public Label userEmailLabel;
    public Label userEntireNameLabel;

    public Label emailLabel;
    public Label birthdateLabel;
    public Label sexLabel;

    @FXML
    public BorderPane notificationPane;
    @FXML
    public ImageView notificationButton;

    @FXML
    public BorderPane settingsPane;
    public BorderPane fogPane;
    public Button exportButton;

    @FXML
    private FriendsController friendsController = null;
    @FXML
    private ChatPaneController chatController = null;
    @FXML
    private UsersController usersController = null;
    @FXML
    private ProfilePaneController profilesController = null;
    @FXML
    private SettingsController settingsController = null;
    @FXML
    private EventPaneController eventController = null;
    @FXML
    private NotificationPaneController notificationController = null;
    @FXML
    private Label userNameLabel;
    @FXML
    private BorderPane profilePane, friendsPane, usersPane, chatPane, eventsPane;
    @FXML
    public BorderPane userProfilePane;
    @FXML
    public ImageView imageProfile;
    @FXML
    public Button btnLogOut;

    private SuperServiceUser serviceUser;
    private Page page;

    @FXML
    public void initialize(){
        btnLogOut.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                FXMLLoader fxmlLoader = new FXMLLoader(LogInInterface.class.getResource("/com/example/socialnetworkdb2/mainWindows/logInView.fxml"));
                ChatOpenedController.setGroup(null);
                Scene scene = null;
                try {
                    scene = new Scene(fxmlLoader.load());
                    LogInController logInController = fxmlLoader.getController();
                    logInController.setServices(ServiceLogIn.getInstance(), ServiceSignUp.getInstance(), serviceUser);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Stage stage = (Stage) btnLogOut.getScene().getWindow();
                stage.setScene(scene);
                stage.sizeToScene();
                stage.setMinWidth(scene.getWidth());
                stage.setMinHeight(scene.getHeight());
                stage.sizeToScene();
                stage.setResizable(false);
            }
        });

        try{
            FXMLLoader loaderNotifications = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/notifications/notificationView.fxml"));
            notificationPane.setCenter(loaderNotifications.load());
            notificationController = loaderNotifications.getController();
            notificationController.injectController(this);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        EventHandler<MouseEvent> handlerExport = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                FXMLLoader fxmlLoader = new FXMLLoader(LogInInterface.class.getResource("/com/example/socialnetworkdb2/raports/datePickersStage.fxml"));
                AnchorPane root = null;
                try {
                    root = fxmlLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                DatePickersStageController datePickersStageController = fxmlLoader.getController();
                datePickersStageController.setServices(page, null, 1);
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setTitle("Parrot");
                stage.setScene(scene);
                stage.show();
            }
        };
        exportButton.setOnMouseClicked(handlerExport);
    }

    @FXML
    public void createPage(SuperServiceUser serviceUser, Long id){
        this.serviceUser = serviceUser;
        try {
            this.serviceUser.setIdUser(id);
        } catch (CredentialException e) {
            e.printStackTrace();
        }
        page = new Page(serviceUser);
        notificationController.setService(page);
    }

    @FXML
    public void populateWithData() {
        userNameLabel.setText(page.getFirstName() + " " + page.getLastName());
        userEntireNameLabel.setText(page.getFirstName() + " " + page.getLastName());
        userEmailLabel.setText(page.getEmail());

        emailLabel.setText(page.getEmail());
        birthdateLabel.setText(page.getBirthDate().format(Dates.DATE_FORMATTER));
        sexLabel.setText(page.getSex().toString());
        loadProfilePicture(page.getProfilePicture());
        profilePane.toFront();
        usersPane.setVisible(false);
        friendsPane.setVisible(false);
        userProfilePane.setVisible(false);
        chatPane.setVisible(false);
        eventsPane.setVisible(false);
        notificationPane.setVisible(false);
        settingsPane.setVisible(false);
        fogPane.setVisible(false);

        notificationPane.getScene().setOnMousePressed(event -> {
            if(!notificationPane.equals(event.getSource())){
                notificationPane.setVisible(false);
            }
        });
    }

    private void loadProfilePicture(String profilePicture) {
        if(profilePicture != null)
            imageProfile.setImage(new Image(new File(profilePicture).toURI().toString()));
        else
            imageProfile.setImage(new Image("file:src/main/resources/profile/default.jpg"));
        Circle clip = new Circle(imageProfile.getFitWidth()/2, imageProfile.getFitHeight()/2, 100);
        imageProfile.setClip(clip);
    }

    @FXML
    public void onProfileClick(){
        profilePane.setVisible(true);
        profilePane.toFront();
        friendsPane.setVisible(false);
        userProfilePane.setVisible(false);
        usersPane.setVisible(false);
        chatPane.setVisible(false);
        eventsPane.setVisible(false);
        notificationPane.setVisible(false);
        settingsPane.setVisible(false);
        fogPane.setVisible(false);
    }

    @FXML
    public void onFriendsClick(){
        if(friendsController == null){
            try {
                FXMLLoader loaderFriends = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/friendships/friendsPane.fxml"));
                friendsPane.setCenter(loaderFriends.load());
                friendsController = loaderFriends.getController();
                friendsController.setService(page);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        friendsPane.setVisible(true);
        friendsPane.toFront();
        friendsController.panesToBack();
        eventsPane.setVisible(false);
        notificationPane.setVisible(false);
        settingsPane.setVisible(false);
        fogPane.setVisible(false);
    }

    @FXML
    public void onUsersClick(){
        if(usersController == null){
            try {
                FXMLLoader loaderUsers = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/users/usersPane.fxml"));
                usersPane.setCenter(loaderUsers.load());
                usersController = loaderUsers.getController();
                usersController.injectController(this);
                usersController.setService(page);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        usersPane.setVisible(true);
        usersPane.toFront();
        eventsPane.setVisible(false);
        notificationPane.setVisible(false);
        settingsPane.setVisible(false);
        fogPane.setVisible(false);
    }

    @FXML
    public void onUserProfileClick(){
        userProfilePane.setVisible(true);
        userProfilePane.toFront();
        notificationPane.setVisible(false);
        settingsPane.setVisible(false);
        fogPane.setVisible(false);
    }

    @FXML
    public void showProfile(User user) {
        if(profilesController == null) {
            try {
                FXMLLoader loaderProfiles = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/profile/profilePane.fxml"));
                userProfilePane.setCenter(loaderProfiles.load());
                profilesController = loaderProfiles.getController();
                profilesController.injectController(this);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        profilesController.populateWithData(user);
        userProfilePane.setVisible(true);
        userProfilePane.toFront();
        eventsPane.setVisible(false);
        notificationPane.setVisible(false);
        settingsPane.setVisible(false);
        fogPane.setVisible(false);
    }

    @FXML
    public void onChatClick() {
        if(chatController == null){
            try {
                FXMLLoader loaderGroups = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/chat/chatPane.fxml"));
                chatPane.setCenter(loaderGroups.load());
                chatController = loaderGroups.getController();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            chatController.setService(page);
        }
        chatPane.setVisible(true);
        chatPane.toFront();
        eventsPane.setVisible(false);
        notificationPane.setVisible(false);
        settingsPane.setVisible(false);
        fogPane.setVisible(false);
    }

    @FXML
    public void onEventClick() {
        if(eventController == null){
            try{
                FXMLLoader loaderEvents = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/events/eventPane.fxml"));
                eventsPane.setCenter(loaderEvents.load());
                eventController = loaderEvents.getController();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            eventController.setService(page);
        }
        eventsPane.setVisible(true);
        eventsPane.toFront();
        settingsPane.setVisible(false);
        fogPane.setVisible(false);
    }

    @FXML
    public void onSettingsClick() {
        if(settingsController == null){
            try{
                FXMLLoader loaderSettings = new FXMLLoader(getClass().getResource("/com/example/socialnetworkdb2/mainWindows/settingsPane.fxml"));
                AnchorPane setPane = loaderSettings.load();
                settingsPane.setCenter(setPane);
                setPane.setMaxHeight(400);
                setPane.setMaxWidth(600);
                settingsController = loaderSettings.getController();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
            settingsController.setService(page);
            settingsController.injectController(this);
        }
        settingsController.refreshData();
        settingsPane.setVisible(true);
        fogPane.setVisible(true);
        settingsPane.toFront();
    }

    @FXML
    public void onNotificationClick() {
        if(notificationPane.isVisible()){
            notificationPane.setVisible(false);
        }
        else {
            notificationPane.setVisible(true);
            notificationPane.toFront();
        }
        settingsPane.setVisible(false);
        fogPane.setVisible(false);
    }

    @FXML
    public void setRedDot(boolean var){
        if(var)
            notificationButton.setImage(new Image("file:src/main/resources/images/bellOn.png"));
        else
            notificationButton.setImage(new Image("file:src/main/resources/images/bellOff.png"));
    }

    public void setNotificationPaneHeight(Double height){
        notificationPane.setMaxHeight(height);
    }
}
