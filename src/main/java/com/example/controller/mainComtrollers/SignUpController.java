package com.example.controller.mainComtrollers;

import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.service.ServiceSignUp;
import com.example.socialnetworkdb2.LogInInterface;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.IOException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class SignUpController implements Initializable {
    public TextField usernameTextFieldsp;
    public PasswordField passwordTextFieldsp;
    public Button registerButton;
    public Label signUpMessageLabel;
    public TextField firstNameTextFieldsp;
    public TextField emailTextFieldsp;
    public TextField lastNameTextFieldsp;
    public PasswordField retypeTextFieldsp;
    public ComboBox SexComboIdsp;
    public DatePicker BirthdatePickersp;
    private ServiceSignUp serviceSignUp;

    LogInController logInController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        SexComboIdsp.getItems().add("Male");
        SexComboIdsp.getItems().add("Female");
        BirthdatePickersp.setEditable(false);
    }

    public void setService(ServiceSignUp serviceSignUp){
        this.serviceSignUp = serviceSignUp;
    }

    @FXML
    public void onSignupButton(ActionEvent actionEvent) {
        signUpMessageLabel.setText("");
        String username, email, pass1, pass2, firstName, lastName, sex;
        LocalDate birthdate;
        username = usernameTextFieldsp.getText();
        email = emailTextFieldsp.getText();
        pass1 = passwordTextFieldsp.getText();
        pass2 = retypeTextFieldsp.getText();
        firstName = firstNameTextFieldsp.getText();
        lastName = lastNameTextFieldsp.getText();

        if(SexComboIdsp.getValue() == null || BirthdatePickersp.getValue() == null || username.length() == 0 || email.length() == 0 || firstName.length() == 0 || lastName.length() == 0 || pass1.length() == 0 || pass2.length() == 0)
            signUpMessageLabel.setText("All fields are mandatory");
        else {
            sex = SexComboIdsp.getValue().toString();
            birthdate = BirthdatePickersp.getValue();
            if (!pass1.equals(pass2)) {
                signUpMessageLabel.setText("Passwords does not match!");
            } else {
                try {
                    serviceSignUp.signUp(username, pass1, firstName, lastName, sex, email, birthdate);
                    logInController.autoComplete(pass1, username);
                    ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
                } catch (ValidationException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | ServiceException e) {
                    signUpMessageLabel.setText(e.getMessage());
                }
                }
            }
    }

    public void injectController(LogInController logInController) {
        this.logInController = logInController;
    }
}
