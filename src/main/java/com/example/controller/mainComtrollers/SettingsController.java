package com.example.controller.mainComtrollers;

import com.example.domain.User;
import com.example.exceptions.ServiceException;
import com.example.exceptions.ValidationException;
import com.example.service.Page;
import com.example.utils.Sex;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.File;
import java.security.InvalidKeyException;
import java.time.LocalDate;

public class SettingsController {

    public ImageView profilePicturePreview;
    public DatePicker birthdateField;
    public ComboBox sexField;
    public TextField lastNameField;
    public TextField firstNameField;
    public TextField emailField;
    public Button updateChangesButton;
    public Button discardChangesButton;
    public Label errorLabel;
    private Page page;
    private UserMainController userMainController;
    private String selectedImagePath;

    public void initialize(){
        birthdateField.setEditable(false);
        sexField.getItems().add("MALE");
        sexField.getItems().add("FEMALE");
    }

    public void setService(Page page){
        this.page = page;
    }

    public void refreshData() {
        errorLabel.setText("");
        selectedImagePath = "";
        loadProfilePicture(page.getProfilePicture());
        firstNameField.setText(page.getFirstName());
        lastNameField.setText(page.getLastName());
        emailField.setText(page.getEmail());
        birthdateField.setValue(page.getBirthDate());
        sexField.setValue(page.getSex().toString());
    }

    private void loadProfilePicture(String path) {
        if(path!=null)
            profilePicturePreview.setImage(new Image(new File(path).toURI().toString()));
        else
            profilePicturePreview.setImage(new Image("file:src/main/resources/profile/default.jpg"));
        Circle clip = new Circle(profilePicturePreview.getFitWidth()/2, profilePicturePreview.getFitHeight()/2, 97.5);
        profilePicturePreview.setClip(clip);
    }

    public void injectController(UserMainController userMainController) {
        this.userMainController = userMainController;
        setButtons();
    }

    private void setButtons() {
        setUploadPictureAction();
        discardChangesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                userMainController.onProfileClick();
            }
        });
        updateChangesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                if(updateChanges() == 0)
                    userMainController.populateWithData();
            }
        });
    }

    private int updateChanges() {
        String email, firstName, lastName, sex;
        LocalDate birthdate;
        email = emailField.getText();
        firstName = firstNameField.getText();
        lastName = lastNameField.getText();
        if(sexField.getValue() == null || birthdateField.getValue() == null || email.length() == 0 || firstName.length() == 0 || lastName.length() == 0) {
            errorLabel.setText("All fields are mandatory");
            return 1;
        }
        else {
            sex = sexField.getValue().toString();
            birthdate = birthdateField.getValue();
            try {
                page.getSuperServiceUser().updateInfos(firstName, lastName, sex, email, birthdate);
                if(selectedImagePath.length()!=0) {
                    page.getSuperServiceUser().setProfilePicture(selectedImagePath);
                    page.setProfilePicture(selectedImagePath);
                }
                page.setFirstName(firstName);
                page.setLastName(lastName);
                page.setEmail(email);
                page.setSex(Sex.valueOf(sex));
                page.setBirthDate(birthdate);
                return 0;
            } catch (ValidationException | ServiceException e) {
                errorLabel.setText(e.getMessage());
                return 1;
            }
        }
    }

    private void setUploadPictureAction(){
        EventHandler uploadPicture = (EventHandler) (javafx.event.Event event)->{
            final FileChooser fileChooser = new FileChooser();
            Stage stage = new Stage();
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                selectedImagePath = file.getPath();
                if(selectedImagePath != null)
                    profilePicturePreview.setImage(new Image(new File(selectedImagePath).toURI().toString()));
            }
        };
        profilePicturePreview.setOnMouseClicked(uploadPicture);
    }
}
