package com.example.controller.mainComtrollers;

import com.example.service.*;
import com.example.socialnetworkdb2.LogInInterface;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.Window;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.security.auth.login.CredentialException;
import java.io.IOException;
import java.security.InvalidKeyException;

public class LogInController {
    public Button loginButton;
    public Button registerButton;
    public TextField usernameTextField;
    public PasswordField passwordTextField;
    public Label loginMessageLabel;

    private ServiceLogIn serviceLogIn;
    private ServiceSignUp serviceSignUp;
    private SuperServiceUser serviceUser;

    public void setServices(ServiceLogIn serviceLogIn, ServiceSignUp serviceSignUp, SuperServiceUser serviceUser){
        this.serviceLogIn = serviceLogIn;
        this.serviceSignUp = serviceSignUp;
        this.serviceUser = serviceUser;
    }

    @FXML
    public void onLoginButton(ActionEvent actionEvent) {
        if(usernameTextField.getText().isBlank())
            loginMessageLabel.setText("Username cannot be null!");
        if(passwordTextField.getText().isBlank())
            loginMessageLabel.setText("Password cannot be null!");
        String username = usernameTextField.getText();
        String password = passwordTextField.getText();
        try {
            Long user_id = serviceLogIn.authenticate(username, password);
            usernameTextField.setText("");
            passwordTextField.setText("");
            connectUser(user_id);

           // ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
        } catch (CredentialException e) {
            loginMessageLabel.setText(e.getMessage());
        } catch (IllegalBlockSizeException | InvalidKeyException | BadPaddingException e) {
            e.printStackTrace();
        }
    }

    private void connectUser(Long user_id) {
        FXMLLoader fxmlLoader = new FXMLLoader(LogInInterface.class.getResource("mainWindows/userMainView.fxml"));
        Scene scene = null;
        Stage stage = (Stage) loginButton.getScene().getWindow();
        try {
            scene = new Scene(fxmlLoader.load());

            UserMainController userMainController = fxmlLoader.getController();
            userMainController.createPage(serviceUser, user_id);
            userMainController.populateWithData();
            stage.setScene(scene);
            stage.sizeToScene();
            stage.setMinWidth(scene.getWidth());
            stage.setMinHeight(scene.getHeight());
            stage.setResizable(true);


        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setTitle("Parrot");
    }

    @FXML
    public void onSignupButton(ActionEvent actionEvent) {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(LogInInterface.class.getResource("mainWindows/signUpView.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load());
            SignUpController signUpController  = fxmlLoader.getController();
            signUpController.setService(serviceSignUp);
            signUpController.injectController(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setTitle("Parrot");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    public void autoComplete(String password, String username){
        usernameTextField.setText(username);
        passwordTextField.setText(password);
    }
}
