package com.example.controller.profile;

import com.example.controller.mainComtrollers.UserMainController;
import com.example.domain.User;
import com.example.utils.Dates;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;

import java.io.File;

public class ProfilePaneController {
    @FXML
    private Label emailLabel, birthdateLabel, sexLabel, userEntireNameLabel, userEmailLabel;
    @FXML
    private ImageView imageProfile;

    private UserMainController userMainController;

    @FXML
    public void populateWithData(User user) {
        if(user == null)
            return;

        userEntireNameLabel.setText(user.getFirstName() + " " + user.getLastName());
        userEmailLabel.setText(user.getEmail());

        emailLabel.setText(user.getEmail());
        birthdateLabel.setText(user.getBirthdate().format(Dates.DATE_FORMATTER));
        sexLabel.setText(user.getSex().toString());
        loadProfilePicture(user);
    }

    private void loadProfilePicture(User user) {
        if(user.getProfilePicture()!=null)
            imageProfile.setImage(new Image(new File(user.getProfilePicture()).toURI().toString()));
        else
            imageProfile.setImage(new Image("file:src/main/resources/profile/default.jpg"));
        Circle clip = new Circle(imageProfile.getFitWidth()/2, imageProfile.getFitHeight()/2, 100);
        imageProfile.setClip(clip);
    }

    public void injectController(UserMainController userMainController) {
        this.userMainController = userMainController;
    }
}
